##FFS2Serv

Basic Server for FFS2Play multiplayer technology.
No yet optimized

This server is designed to work as E107 V2.1.7 Extension

The license is as published by the Free Software Foundation and appearing in the file LICENSE.GPL3 included in the packaging of this software. Please review the following information to ensure the GNU General Public License requirements will be met: https://www.gnu.org/licenses/gpl-3.0.html.

###INSTALLATION :
1. Just clone this repo directly on E107 CMS root directory
1. Fill you FFS2Play server SSL certificate on the e107_plugins/mark_42_multi/sql_insert_data.php
1. move action.php in root path of your web site 
1. Go on E107 Administration aera to install plugin

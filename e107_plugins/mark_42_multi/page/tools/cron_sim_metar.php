<?php
require_once("./../../../../class2.php");
require_once(HEADERF);
/**
 * e107 website system
 *
 * Copyright (C) 2008-2016 e107 Inc (e107.org)
 * Released under the terms and conditions of the
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
 *
 */

		//départ du chrono
		$start=time();


		$sql = e107::getDb();
		$mes = e107::getMessage();

		error_reporting(E_ERROR | E_WARNING | E_PARSE);

		$sql->select("tks_airports", "`icao`,`latitude` as `Latitude`, `longitude` as `Longitude`","1");
		//var_dump($sql);
		//$data_m_a=array();
		//var_dump($row= $sql->fetch());
		$all_airports=array();
		while($row= $sql->fetch())
		{
			$all_airports[$row['icao']]['Longitude']=$row['Longitude'];
			$all_airports[$row['icao']]['Latitude']=$row['Latitude'];
		}
//var_dump($all_airports);
		

		//tentative de chargement du protocole metar
		if (! @include_once(e_PLUGIN."mark_42_multi/class/Metar.php"))
		{
			throw new Exception ("Votre protocol Metar est introuvable");
		}

		function files_temp ($zoulou)
		{
			$file_time_last_edit=filemtime(e_PLUGIN."mark_42_multi/airac_data/".str_pad($zoulou."Z.TXT", 7, "0", STR_PAD_LEFT));
			
			if((time()-60*10)>$file_time_last_edit)
			{
				echo "fichier obselette<br>";
				echo "récupération du fichier http://tgftp.nws.noaa.gov/data/observations/metar/cycles/".str_pad($zoulou."Z.TXT", 7, "0", STR_PAD_LEFT)."<br>";
				$file_metar=file_get_contents("http://tgftp.nws.noaa.gov/data/observations/metar/cycles/".str_pad($zoulou."Z.TXT", 7, "0", STR_PAD_LEFT));
				file_put_contents(e_PLUGIN."mark_42_multi/airac_data/".str_pad($zoulou."Z.TXT", 7, "0", STR_PAD_LEFT), $file_metar,LOCK_EX);
				return $file_metar;
			}
			else
			{
				echo "fichier toujours valide: ".e_PLUGIN."mark_42_multi/airac_data/".str_pad($zoulou."Z.TXT", 7, "0", STR_PAD_LEFT)."<br>";
				return $file_metar=file_get_contents(e_PLUGIN."mark_42_multi/airac_data/".str_pad($zoulou."Z.TXT", 7, "0", STR_PAD_LEFT));
				//return FALSE;
			}
		}

		function files_extract ($file_data)
		{
			//var_dump($file_data);
			//recherche des séparateur (retour à la ligne) et découpe du fichier dans un tableau
			$all_data_metar=explode("\r\n\r\n", $file_data);

			//si la découpe ne donne que 2 résultats, c'est que la découpe \r\n\r\n n'est pas la bonne
			if(count($all_data_metar)<2)
			{
				//recherche des séparateur (retour à la ligne, seconde méthode) et découpe du fichier dans un tableau
				$all_data_metar=explode("\n\n", $file_data);
				//si la découpe ne donne que 2 résultats, c'est que la découpe \n\n n'est pas la bonne non plus
				if(count($all_data_metar)<2)
				{
					return FALSE;
				}
				else
				{
					return $all_data_metar;
				}
			}
			else
			{
				return $all_data_metar;
			}
		}

		function clean_data($data,$sql)
		{
			//on prépare le tableau
			$pre_scan=array();
			//on initialise un index
			$step=0;

			//on parcour les lignes de données
			foreach ($data as $key => $value)
			{
				//if($step>=10){break;}
				$step++;

				//on découpe les données, [0]date, [1]donneés
				$temp_data_metar=explode("\n",$value);

				//initialisation de l'objet de décrypage métar
				$metar = new Metar($temp_data_metar[1]);

				//recherche de la création d'un futur doublon
				if(array_key_exists($metar->airport, $pre_scan))
				{
					//on parcour les données du premier métar fournis
					foreach ($pre_scan[$metar->airport] as $key2 => $value2)
					{
						//si la date de la ligne en cours de lecture est supérieur à la date de celui du métar en mémoire
						if(strtotime($value2['date'])<strtotime(str_replace('/', '-', $temp_data_metar[0])))
						{
							//on remplace les donnéeset si les données du métar sont correcte (recherche de l'aéroport dans les données)
							if($metar->airport!=NULL)
							{


								//on insére ou remplace les données métar
								$pre_scan[$metar->airport][$value2['key']]=array('icao'=>$metar->airport,'key'=>$value2['key'],'date'=> str_replace('/', '-', $temp_data_metar[0]),'metar'=>$metar->raw,'Latitude'=>$airport->latitude,'Longitude'=>$airport->longitude);
							}
						}
					}
				}
				//si c'est la premiére fois que l'on traite ce métar
				else
				{
					//si les données du métar sont correcte (recherche de l'aéroport dans les données)
					if($metar->airport!=NULL)
					{

						$sql->select("tks_airports", "*","`icao` = '" . $metar->airport . "'");
						$airport= $sql->fetch();


						//on insére ou remplace les données métar
						$pre_scan[$metar->airport][$step]=array('icao'=>$metar->airport,'key'=>$step,'date'=> str_replace('/', '-', $temp_data_metar[0]),'metar'=>$metar->raw,'Latitude'=>$airport->latitude,'Longitude'=>$airport->longitude);
					}
				}
			}

			if (count($pre_scan)<2)
			{
				return FALSE;
			}
			else
			{
				return $pre_scan;
			}

		}

		function metar_do_db($mod='new',$sql,$metar,$date,$all_airports)
		{

			//données metar à insérer ou à mettre à jours
			$data=array(
				'station_id'=> strval($metar->airport),
				'Latitude'=> $all_airports[$metar->airport]['Latitude'],
				'Longitude'=> $all_airports[$metar->airport]['Longitude'],
				'raw_text'=> strval($metar->raw),
				'observation_time'=> strval(str_replace('/', '-', $date)),
				'temp_c'=> intval($metar->temperature->temperature),
				'dewpoint_c'=> intval($metar->temperature->dewpoint),
				'wind_dir_degrees'=> intval($metar->wind->direction),
				'wind_speed_kt'=> intval($metar->wind->speed),
				'visibility_statute_mi'=> intval($metar->visibility->visibility),
				'sky_condition' => json_encode($metar->cloud_layers),
				'sea_level_pressure_mb'=> intval($metar->altimeter->altimeter),
				);

			//si on met à jours
			if($mod=='update')
			{
				//on rajoute la clé where
				$data['WHERE']="station_id = '".$data['station_id']."'";
				//on fait la mise à jours du métar
				if($sql->update('tks_metar', $data))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
			//si on fait un ajout
			if($mod=='new')
			{
				if($sql->insert('tks_metar', $data))
				{
					return TRUE;
				}
				else
				{
					return FALSE;
				}
			}
		}


		unset($file_metar);
		unset($data_all_metars);
		unset($all_clean_metar);
		$time_limit = time()-(5*3600);

		echo "<pre style='text-align: left;'>";

		//vérification du fichier NOAA en mémoire
		$file_metar=files_temp(gmdate("H"));
		//si le fichiers de NOAA est plus récent que le notre
		if($file_metar != FALSE)
		{
			//var_dump($file_metar);
			//extraction des données du fichier NOAA
			$data_all_metars=files_extract ($file_metar);
			//si des données sont extraite du fichier NOAA
			if($data_all_metars != FALSE)
			{
				echo "<br>nombre de lignes disponibles dans le nouveau fichier: " .count($data_all_metars). "<br>";
				//interprétation des données du fichier NOAA
				$all_clean_metar=clean_data($data_all_metars,$sql);
				//var_dump($all_clean_metar);
				//si des données sont exploitable
				if($all_clean_metar!= FALSE)
					{	echo "nombre de métars disponibles dans le nouveau fichier: " .count($all_clean_metar). "<br>";
						//on scan les données dans la base de données
						if($db_metar = $sql->retrieve('tks_metar', 'observation_time, station_id', '', true))
						{
							echo "nombre de métars en mémoire db: " .count($db_metar). "<br>";
								//on parcours ces données
							foreach($db_metar as $old_metar)
							{
								//var_dump($all_clean_metar);
								//si dans le fichier NOAA cette ficher exsite
								if(array_key_exists($old_metar['station_id'], $all_clean_metar))
								{
										//on recherche la clé de la fiche fichier NOAA (faut pas cherher à comprendre)
									foreach (array_keys($all_clean_metar[$old_metar['station_id']]) as $value)
									{
											//si la date de la fiche NOAA est plus récente que celle de la BD
										if(strtotime($old_metar['observation_time']) < strtotime($all_clean_metar[$old_metar['station_id']][$value]['date']))
										{

											$metar_to_update = new Metar($all_clean_metar[$old_metar['station_id']][$value]['metar']);
											$metar_to_update_date=$all_clean_metar[$old_metar['station_id']][$value]['date'];
											$action_metar=metar_do_db('update',$sql,$metar_to_update,$metar_to_update_date,$all_airports);

											if($action_metar==TRUE)
											{
												$count_update++;
												unset($all_clean_metar[$old_metar['station_id']]);
												unset($metar_to_update);
												unset($metar_to_update_date);
												unset($metar_to_create);
											}
											else
											{
												var_dump($sql);
													//break 2;
											}
										}
										else
										{
											unset($all_clean_metar[$old_metar['station_id']]);
										}
									}

								}
								else
								{
									if(strtotime($old_metar['observation_time'])< $time_limit)
									{
										$sql->delete("tks_metar", "station_id='".$old_metar['station_id']."'");
										$count_delete++;
									}

								}
							}
							//echo "nombre de métars en mémoire db supprimés: " .count($count_delete). "<br>";
							if(count($all_clean_metar)>5)
							{
								echo "<br>injections des données complémentaires<br>";

								foreach ($all_clean_metar as $key => $value)
								{
										//if($step>=10){break;}
									$step++;
									if(count($value)==1)
									{
										foreach ($value as $key2 => $value2)
										{

											$metar_to_create = new Metar($value2['metar']);
											$action_metar=metar_do_db('new',$sql,$metar_to_create,$value2['date'],$all_airports);

											if($action_metar==TRUE)
											{
												$count_insert++;
												unset($all_clean_metar[$value2['icao']]);
											}
											else
											{
												var_dump($sql);
													//break 2;
											}
										}
										unset($action_metar);
										unset($metar_to_create);

									}
								}
							}
						}
						else
						{
							echo "<br>erreur base de données ou base de données vides <br>";

							if(count($all_clean_metar)>10)
							{
								$sql->gen("TRUNCATE TABLE `e107_tks_metar`");
								echo "<br>inections des nouvelles données<br>";

								foreach ($all_clean_metar as $key => $value)
								{
										//if($step>=10){break;}
									$step++;
									if(count($value)==1)
									{
										foreach ($value as $key2 => $value2)
										{

											$metar_to_create = new Metar($value2['metar']);
												//$metar_to_create_date=$all_clean_metar[$old_metar['station_id']][$value]['date'];
											$action_metar=metar_do_db('new',$sql,$metar_to_create,$value2['date'],$all_airports);

											if($action_metar==TRUE)
											{
												$count_insert++;
												unset($all_clean_metar[$value2['icao']]);
											}
											else
											{
												var_dump($sql);
													//break 2;
											}
										}
										unset($metar);
										unset($metar_to_create);
										unset($data_metar_to_db);

									}
								}
							}
						}

			}
			else
			{
				echo "<br>Aucune données exploitables<br>";
				var_dump($all_clean_metar);
			}
		}
		else
		{
			echo "<br>Aucune données trouvées<br>";
			var_dump($data_all_metars);
		}
	}
	else
	{
		echo "<br>Aucune fichier à charger<br>";
		var_dump($file_metar);
	}

	$end=time();
	$timer=$end-$start;

	
	
	echo "total inséré: ".$count_insert."<br>";
	echo "total supprimé: ".$count_delete."<br>";
	echo "total mis a jours: ".$count_update."<br>";
	echo "temps total: ".$timer."<br>";
	echo "restant: ".count($all_clean_metar)."<br>";
	echo "mémoire utilisé: ".(memory_get_peak_usage (true)/1000000)." Mega octets <br>"; 
	echo '</pre>';


require_once(FOOTERF);
exit;
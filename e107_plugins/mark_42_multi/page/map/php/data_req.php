<?php 
error_reporting(E_ALL);

//tentative de chargement des classes de E107
if (! @include_once("./../../../../../class2.php"))
	throw new Exception ("Votre E107 est introuvable");
	//tentative de chargement du gestionnaire de carte
if (! @include_once("./".e_PLUGIN."mark_42_multi/class/map_data.php"))
	throw new Exception ("Le gestionnaire de carte est introuvable");


//initialisation de la bd E107
$sql = e107::getDb();
date_default_timezone_set("Europe/Brussels" );

function flying_pilote($sql, $filter='all')
{
	$now = $date = date ("Y-m-d H:i:s");
	$date = new DateTime($now);
	$date->sub (new DateInterval('PT30M'));
	$time = $date->format ("Y-m-d H:i:s");

	//listage des serveurs
	$sql->select("tks_serveur", "`serveur_id`,`serveur_name`");
	$data_serveur=array();
	while($row = $sql->fetch())
	{
		$data_serveur[$row['serveur_id']]=$row['serveur_name'];
	}


	//listages des joueurs
	$sql->select("tks_acarsdata", "*","`lastupdate` between  '". $time ."' and '" . $now . "'");
	$data_pilote=array();
	while($row = $sql->fetch())
	{
		$data_pilote[$row['map_id']]=$row;
	}

	foreach ($data_pilote as $key => $value)
	{

		$pilote = $sql->retrieve("tks_mp", "*","pilotname='".$value['pilotname']."'");

		$data_pilote[$key]['server']=$pilote['server'];
		$data_pilote[$key]['server_name']=$data_serveur[$pilote['server']];
	}
	
	
	if($filter=="all")
	{
		header('content-type: application/json; charset=utf-8');
		
		if (count($data_pilote)>0 and $data_pilote!=null){echo json_encode($data_pilote);}
	}
	else
	{
		header('content-type: application/json; charset=utf-8');
		foreach ($data_pilote as $key => $value)
		{
			if ($data_pilote[$key]['server_name']!=$filter)
			{
				unset($data_pilote[$key]);
			}
			
		}
		if (count($data_pilote)>0 and $data_pilote!=null)
			{echo json_encode($data_pilote);}
	}

	
}

function map_aircraft($sql)
{

	$now = $date = date ("Y-m-d H:i:s");
	$date = new DateTime($now);
	$date->sub (new DateInterval('PT30M'));
	$time = $date->format ("Y-m-d H:i:s");

	$where = '`lastupdate` between  "' . $time . '" and "' . $now . '"';

	$data = '*';

	$sql->select("tks_acarsdata", "*","`lastupdate` between  '". $time ."' and '" . $now . "'");

	$data_m_a=array();
	while($row = $sql->fetch())
	{
		$data_m_a[]=$row;
	}
	if ($sql!=null){echo json_encode($data_m_a);}
}

function tab_aircraft($sql)
{
	function truncate($string, $length, $dots = "...") {
		return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
	}
	//réglage de l'interval de temps
	$now = $date = date ("Y-m-d H:i:s");
	$date = new DateTime($now);
	$date->sub (new DateInterval('PT5M'));
	$time = $date->format ("Y-m-d H:i:s");

	//listage des serveurs;
	$sql->select("tks_serveur", "*","1 ORDER BY `serveur_name` asc");

	$serveur_data=array();
	while($row= $sql->fetch())
	{
		$serveur_data[$row['serveur_id']]=$row;
	}

	
	//listages des joueurs
	$sql->select("tks_acarsdata", "*","`lastupdate` between  '". $time ."' and '" . $now . "' ORDER BY `pilotname` asc");
	
	$acars_data=array();
	while($row= $sql->fetch())
	{
		$acars_data[]=$row;
	}
	
	$tab_data=array();

	$tab_start="<table style='width:100%;border:1px solid;'><thead><tr><th>Nom du pilote</th><th>Appareil utilisé</th><th>Prés de</th><th>Vitesse</th><th>Cap</th><th>Altitude</th></tr></thead><tbody>";
	$tab_end="</tbody></table><br>";
	$tab_line="";
	$tab_all_end="Si le nom d'un pilote est suivi d'une *, c'est que celui utilise le mod global";



	foreach ($acars_data as $key => $value)
	{
		$sql->gen("SELECT `icao`,`Longitude`,`Latitude`,`name`,( 3959 * acos ( cos ( radians('".$value['Latitude']."') ) * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians('".$value['Longitude']."') ) + sin ( radians('".$value['Latitude']."') ) * sin( radians( Latitude ) ) ) ) AS distance FROM `e107_tks_airports` HAVING distance < 60 ORDER BY distance LIMIT 0 , 1");

		$aiport=$sql->fetch();
		
		$pilote = $sql->retrieve("tks_mp", "*","pilotname='".$value['pilotname']."'");
		$serveur = $sql->retrieve("tks_serveur", "*","serveur_id='".$pilote['server']."'");

		if($serveur['private']==0){$private_serv="oui";}
		else{$private_serv="non";}

		if($pilote['tx_mod']==1)
		{
			$pilotname=$pilote['pilotname']."*";
		}
		else
		{
			$pilotname=$pilote['pilotname'];	
		}

		$tab_data[]=array("pilote"=>$pilotname,"aircraft"=>$value['aircraft'],"aiport"=>$aiport['icao'] .'&nbsp;&nbsp;&nbsp;'.$aiport['name'],"serveur_name"=>$serveur['serveur_name'],"serveur_number"=>$pilote['server'],"serveur_private"=>$private_serv,"ias"=>$value['ias'],"alt"=>$value['alt'],"heading"=>$value['heading']);
	}

	foreach ($tab_data as $key => $value)
	{
		$tab_aircraft[]=$value['aircraft'];
		$tab_pilote[]=$value['pilote'];
		$tab_serveur_name[]=$value['serveur_name'];
		$tab_aiport[]=$value['aiport'];
	}

	foreach ($tab_data as $key => $value)
	{
		$serveur_data[$value['serveur_number']]['players'][]=$value;
	}

	echo "<H4>".count($acars_data)." pilote(s) en vol(s)</H4> ";

	foreach ($serveur_data as $key => $value)
	{
		if(count($value['players'])>0)
		{
			echo "<H4 style='display:inline;'>".$value['serveur_name']."</H4> ";
			echo ", ". count($value['players'])." pilote(s) en vol(s)";
			echo $tab_start;

			foreach ($value['players'] as $key2 => $value2)
			{
				echo "<tr class='like_display' style='border-bottom: solid 1px;border-bottom-color: orange'>
				<td>".$value2['pilote']."</td>
				<td>".truncate($value2['aircraft'],30)."</td>
				<td>".truncate($value2['aiport'],50)."</td>
				<td>".$value2['ias']." ias</td>
				<td>".$value2['heading']." °</td>
				<td>".$value2['alt']." Pieds</td>
				<tr>";
			}

			echo $tab_end;
		}
		
	}
	echo $tab_all_end;
}

function data_black_box($sql,$fn=null)
{

	if($fn!=null)
	{
		$where = '`flightnum` ="'. $fn.'"';

		$data = '*';

		$result = $db_m_5->select ('`' . PF_MARK_5_ENR . 'blackbox`', $data, $where, 'ORDER BY `id` DESC');

			//var_dump($result);

		$data_record=json_decode($result->record);

		foreach ($data_record as $key => $value) {
			if($value->data_type=='cycle')
			{
						//$real_route[]=array('lat' => $value->Lat,'lng' => $value->Lng);
				$real_route[]='{lat: '.$value->Lat.', lng: '.$value->Lng.'}';
			}
		}
		echo str_replace('"', '', json_encode($real_route));
	}
}

function data_schedule($sql,$fn=null)
{
	if($fn!=null)
	{
			//$flightnum=explode('-', $fn, 2);
			//$flightnum[1]="-".$flightnum[1];

		$where = '`flightnum` ="'. $fn.'"';

		$data = '*';

		$result = $db_m_5->select ('`' . PF_MARK_5_ENR . 'resas`', $data, $where);

			//var_dump($result->depicao);

			//$resa=$db_TKS->query_list("SELECT * FROM `tks_schedules` where `flightnum` = '$flightnum[1]' and `code` = '$flightnum[0]'");
		$dep= new Airport($result->depicao);
		$arr= new Airport($result->arricao);
		$points=array(
			'dep' 	=> array('lat'=>$dep->lat,'lng'=>$dep->lng,'name'=>$dep->name,),
			'arr'	=> array('lat'=>$arr->lat,'lng'=>$arr->lng,'name'=>$arr->name,),
		);
		echo json_encode($points);
	}

}

function data_flight($sql,$map_id)
{
	$sql->select("tks_acarsdata", "*","`map_id` = '" . $map_id . "'");
	$acardata= $sql->fetch();
	$data_on_map='
	<div class="aircraftInfoGridContainer" style="//height: 387px; display: block;">
	<table class="aircraftInfoGrid">
	<tbody>
	<tr class="first">
	<td class="iconContainer">
	<span class="icon aircraft"></span>
	</td>
	<td colspan="2">
	Aircraft<br><span class="right strong" id="aircraftIcaoVal">( '.$acardata['aircraft'].' )</span><br>
	<span id="aircraftVal" class="strong"></span>
	</td>
	</tr>
	<tr>
	<td class="iconContainer"></td>
	<td colspan="2">
	Registration<br><span class="right strong">( '.$acardata['pilotname'].' )</span><br>
	<span id="registrationVal" class="strong"><a class="regLink">'.$reg_aircraft.'</a></span>
	</td>
	</tr>
	<tr>
	<td class="iconContainer">
	<span class="icon cloud"></span>
	</td>
	<td>
	Altitude<br>
	<span id="altitudeVal" class="strong">'.$acardata['alt'].'</span>
	</td>
	<td>
	Vertical Speed<br>
	<span id="vspdVal" class="strong">INF</span>
	</td>
	</tr>
	<tr>
	<td class="iconContainer"></td>
	<td>
	Speed<br>
	<span id="speedVal" class="strong">'.$acardata['ias'].' kt</span>
	</td>
	<td>
	Track<br>
	<span id="trackVal" class="strong">'.$acardata['heading'].'°</span>
	</td>
	</tr>
	<tr>
	<td class="iconContainer">
	<span class="icon satellite"></span>
	</td>
	<td>
	Latitude<br>
	<span id="latVal" class="strong">'.$acardata['Latitude'].'</span>
	</td>
	<td>
	Longitude<br>
	<span id="lonVal" class="strong">'.$acardata['Longitude'].'</span>
	</td>
	</tr>
	<tr>
	<td class="iconContainer"></td>
	<td>
	Radar<br>
	<span id="radarVal" class="strong">FFS2P_01</span>
	</td>
	<td>
	Squawk<br>
	<span id="squawkVal" class="strong">'.$acardata['squawk'].'</span>
	</td>
	</tr>
	<tr>
	<td class="iconContainer"></td>
	<td colspan="2">
	Phase<br>
	<span id="latVal" class="strong">'.$acardata['phasedetail'].'</span><br><br>
	<span id="latVal" class="strong">'.$acardata['arrapt'].'</span>
	</td>


	</tr>


	</tbody>
	</table>
	</div>

	';
		//}

		//var_dump($resa);

	/*if($resa!==FALSE)
	{
		$percent=$resa->distance/100;
		$percent=100-($acardata->distremain/$percent);
		
		$aircraft=new Aircraft($resa->aircraft_id,$resa->code);

			//var_dump($aircraft);

		$data_on_map_2='
		<div class="aircraftImage"><a href="https://external.flightradar24.com/redir.php?url=http%3A%2F%2Fwww.jetphotos.net%2Fphoto%2F8153149" target="_blank" title="Voir en plus grand sur Jetphotos.net"><img src="'.$aircraft->image_thumb.'"></a></div>
		<div class="routeInformationBox">
			<div class="titleBox">'.$resa->flightnum.'</div>
			<div class="airportInfo" style="display: -webkit-box;">
				<span class="arrow" data-tooltip-maxwidth="200" data-tooltip-value="" data-iata=""></span>
				<div class="airport left from  airportLink" ><h4>'.$resa->depicao.'</h4>'.$dep->name.'</div>
				<div class="airport right to  airportLink" ><h4>'.$resa->arricao.'</h4>'.$arr->name.'</div>
				<br class="clear">
			</div>
			<div class="progressBar " style="display: block;">
				<div class="progress" style="width: '.$percent.'%;"></div>
			</div>
			<div class="statusInfo">
				<div class="airport left from">
					<div id="stdWrapper"><span class="small blue " >STD </span><span id="stdVal">'.$resa->deptime.'</span><br></div>
					<div id="atdWrapper"><span class="small blue " >ATD </span><span id="atdVal" class="hasTooltip" data-tooltip-value="00:54 ago">'.$acardata->deptime.'</span> <span class="small">CET</span></div>
				</div>
				<div class="airport right to">
					<div id="staWrapper"><span class="small blue " >STA </span><span id="staVal">'.$resa->arrtime.'</span><br></div>
					<div id="etaWrapper"><span class="small blue " >ETA </span><span id="etaVal" class="hasTooltip" data-tooltip-value="in 01:08">'.$acardata->arrtime.'</span> <span class="small">CET</span></div>
				</div>
				<br class="clear">
			</div>
		</div>';
	}
		/*echo '	  <ul class="aircraftActions">';
		echo '		<li class="first">';
		echo '		  <a class="showRouteLink" title="Show Route" data-callsign="RYR3JY">';
		echo '			<span class="icon route"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a id="follow-aircraft" title="Follow Aircraft">';
		echo '			<span class="icon center"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a class="pilotViewLink" title="Cockpit View" data-callsign="RYR3JY">';
		echo '			<span class="icon threeD"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li class="last">';
		echo '		  <a class="shareFlightLink" title="Share" data-callsign="RYR3JY" data-aircraft="884603e">';
		echo '			<span class="icon share"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '	  </ul>';
		}

		//var_dump($acardata);
		//echo 'test';

		/*$flightnum=explode('-', $fn, 2);
		$flightnum[1]="-".$flightnum[1];
		$acardata=new Data_map_2($fn);

		//$acardata=$db_TKS->query_list("SELECT * FROM `tks_acarsdata` where `flightnum` = '$fn'");

		//var_dump($acardata);

		$resa=$db_TKS->query_list("SELECT * FROM `tks_schedules` where `flightnum` = '$flightnum[1]' and `code` = '$flightnum[0]'");
		
		$type=$resa[0]->escad;

		$percent=$resa[0]->distance/100;
		$percent=100-($acardata->distremain/$percent);

		$dep= new Airport_2($resa[0]->depicao);
		$arr= new Airport_2($resa[0]->arricao);
		//var_dump($resa[0]->notes);
		
		$temp=explode('*', $resa[0]->notes);
		$temp=explode(':', $temp[0]);
		$id_achat=$temp[1];

		$aircraft_reg=new P_aircraft($id_achat,$type);

		$id_aircraft=$aircraft_reg->id_aircraft;
		
		$aircraft=new Aircraft($id_aircraft,$type);

		$reg_aircraft=strtoupper ($aircraft_reg->type)."-".$aircraft_reg->id_pilot."-".$aircraft_reg->id_achat;*/
		//if ($resa!=null){echo clean_JSON($resa);}
		//else{echo "pas d'info pour ce vol";}
		/*echo '<div class="aircraftSummary" style="display: block;">';
		if($acardata->flightnum!=0){


		echo '	  <div class="aircraftImage"><a href="https://external.flightradar24.com/redir.php?url=http%3A%2F%2Fwww.jetphotos.net%2Fphoto%2F8153149" target="_blank" title="Voir en plus grand sur Jetphotos.net"><img src="'.$aircraft->image_type_1.'"></a></div>';
		echo '	  <div class="routeInformationBox">';
		echo '		<div class="titleBox">'.$resa[0]->code.''.$resa[0]->flightnum.'</div>';
		echo '		<div class="airportInfo" style="display: -webkit-box;">';
		echo '		  <span class="arrow" data-tooltip-maxwidth="200" data-tooltip-value="" data-iata=""></span>';
		echo '		  <div class="airport left from  airportLink" ><h4>'.$resa[0]->depicao.'</h4>'.$dep->name.'</div>';
		echo '		  <div class="airport right to  airportLink" ><h4>'.$resa[0]->arricao.'</h4>'.$arr->name.'</div>';
		echo '		  <br class="clear">';
		echo '		</div>';
		echo '		<div class="progressBar " style="display: block;">';
		echo '		  <div class="progress" style="width: '.$percent.'%;"></div>';
		echo '		</div>';
		echo '		<div class="statusInfo">';
		echo '		  <div class="airport left from">';
		echo '			<div id="stdWrapper"><span class="small blue " >STD </span><span id="stdVal">'.$resa[0]->deptime.'</span><br></div>';
		echo '			<div id="atdWrapper"><span class="small blue " >ATD </span><span id="atdVal" class="hasTooltip" data-tooltip-value="00:54 ago">'.$acardata->deptime.'</span> <span class="small">CET</span></div>';
		echo '		  </div>';
		echo '		  <div class="airport right to">';
		echo '			<div id="staWrapper"><span class="small blue " >STA </span><span id="staVal">'.$resa[0]->arrtime.'</span><br></div>';
		echo '			<div id="etaWrapper"><span class="small blue " >ETA </span><span id="etaVal" class="hasTooltip" data-tooltip-value="in 01:08">'.$acardata->arrtime.'</span> <span class="small">CET</span></div>';
		echo '		  </div>';
		echo '		  <br class="clear">';
		echo '		</div>';
		echo '	  </div>';
		}
		/*echo '	  <ul class="aircraftActions">';
		echo '		<li class="first">';
		echo '		  <a class="showRouteLink" title="Show Route" data-callsign="RYR3JY">';
		echo '			<span class="icon route"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a id="follow-aircraft" title="Follow Aircraft">';
		echo '			<span class="icon center"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a class="pilotViewLink" title="Cockpit View" data-callsign="RYR3JY">';
		echo '			<span class="icon threeD"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li class="last">';
		echo '		  <a class="shareFlightLink" title="Share" data-callsign="RYR3JY" data-aircraft="884603e">';
		echo '			<span class="icon share"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '	  </ul>';*/
		echo '	</div>';

		echo $data_on_map_2;
		echo $data_on_map;
	}
	function map_airport($sql,$center_lat,$center_lng,$range)
	{

		$sql->gen("SELECT `ICAO`,`ID` as `map_id`,`Longitude`,`Latitude`,`Name`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( Latitude ) ) ) ) AS distance FROM `e107_tks_airports` HAVING distance < ".$range." ORDER BY distance");

		while($row = $sql->fetch())
		{
			$result[]=$row;
		}

		if ($sql!=null){echo json_encode($result);}
		else{echo json_encode(null);}
	}

	function map_runway($sql,$center_lat,$center_lng,$range)
	{
		$sql->gen("SELECT `TrueHeading`,`Length`, `ID` as `map_id`, `Latitude`,`Longitude`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( Latitude ) ) ) ) AS distance FROM `e107_tks_runways` HAVING distance < ".$range." ORDER BY distance");

		while($row = $sql->fetch())
		{
			$result[]=$row;
		}

		if ($result!=null){echo json_encode($result);}
		else{echo json_encode(null);}
	}


	function info_aiport($sql,$ID)
	{

		$sql->gen("SELECT
			`e107_tks_airports`.`ID` as `map_id`,
			`e107_tks_airports`.`ICAO`,
			`e107_tks_airports`.`Name`,
			`e107_tks_airports`.`Latitude`,
			`e107_tks_airports`.`Longitude`,
			`e107_tks_runways`.`ID` as `Runways_ID`,
			`e107_tks_runways`.`Airport_ID`,
			`e107_tks_runways`.`Ident` as `Runways_Ident`,
			`e107_tks_runways`.`Length`,
			`e107_tks_runways`.`Width`,
			`e107_tks_runways`.`Surface`,
			`e107_tks_runways`.`Latitude` as `Runways_Latitude`,
			`e107_tks_runways`.`Longitude` as `Runways_Longitude`,
			`e107_tks_runways`.`Elevation`,
			`e107_tks_runways`.`TrueHeading`,
			`e107_tks_ILSes`.`ID` as `ILS_ID`,
			`e107_tks_ILSes`.`RunwayID`,
			`e107_tks_ILSes`.`Freq`,
			`e107_tks_ILSes`.`GsAngle`,
			`e107_tks_ILSes`.`Latitude` as `ILS_Latitude`,
			`e107_tks_ILSes`.`Longitude` as `ILS_Longitude`,
			`e107_tks_ILSes`.`Category`,
			`e107_tks_ILSes`.`Ident` as `ILS_Ident`,
			`e107_tks_ILSes`.`LocCourse`,
			`e107_tks_ILSes`.`Elevation` as `ILS_Elevation`



			from `e107_tks_airports`

			LEFT JOIN `e107_tks_runways` ON `e107_tks_airports`.`ID` = `e107_tks_runways`.`Airport_ID`
			LEFT JOIN `e107_tks_ILSes` ON `e107_tks_runways`.`ID` = `e107_tks_ILSes`.`RunwayID`

			where `e107_tks_airports`.`ICAO` ='".$ID."' ORDER BY `Runways_Ident` ASC");

		while($row = $sql->fetch())
		{
			$result[]=$row;
		}

		if ($result!=null){echo json_encode($result);}
		else{echo json_encode(null);}
	}


	function metar_one($sql,$center_lat,$center_lng,$range)
	{

		$sql->gen("SELECT  `raw_text`,`observation_time`,`temp_c`,`wind_dir_degrees`,`wind_speed_kt`,`dewpoint_c`,`visibility_statute_mi`,`altim_in_hg`,`sky_condition`,`sea_level_pressure_mb`,`wx_string`,`flight_category`,`metar_type`,`elevation_m`,`station_id` as `map_id`, `station_id`,`Latitude`,`Longitude`, `Latitude` as `latitude`, `Longitude` as `longitude`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( latitude ) ) ) ) AS distance FROM `e107_tks_metar` HAVING distance < 150 ORDER BY distance  limit 1");

		while($row = $sql->fetch())
		{
			$result[]=$row;

			$info_metar='<u><B>Station de '.$row['station_id'].'</B></u>&nbsp;&nbsp;&nbsp;&nbsp;<i>'.$row['raw_text'].'</i>&nbsp;&nbsp;&nbsp;&nbsp;<B>Mise à jourle</B>: '.$row['observation_time'].'&nbsp;&nbsp;&nbsp;&nbsp;<B>Tempéraure</B>: '.$row['temp_c'].'°c &nbsp;&nbsp;&nbsp;&nbsp;<B>Point de rosée</B>:'.$row['dewpoint_c'].'°c &nbsp;&nbsp;&nbsp;&nbsp;<B>Direction du vent</B>: '.$row['wind_dir_degrees'].'° &nbsp;&nbsp;&nbsp;&nbsp;<B>Vitesse du vent</B>: '.$row['wind_speed_kt'].' Noeuds &nbsp;&nbsp;&nbsp;&nbsp;<B>Visibilité</B>: '.$row['visibility_statute_mi'].' Miles Nautique &nbsp;&nbsp;&nbsp;&nbsp;<B>Pression au niveau de la mer</B>:'.$row['sea_level_pressure_mb'].' Millibar &nbsp;&nbsp;&nbsp;&nbsp;' ;
			$sky_condition=json_decode($row['sky_condition']);
			if ($sky_condition!= null)
			{
				foreach ($sky_condition as $key => $value)
				{
					$altitude=$value->altitude*100;
					if ($value->coverage=="OVC"){$info_metar=$info_metar."COUVERT à ".$altitude." &nbsp;&nbsp;&nbsp;&nbsp;";}
					if ($value->coverage=="SCT"){$info_metar=$info_metar."EPART à ".$altitude." &nbsp;&nbsp;&nbsp;&nbsp;";}
					if ($value->coverage=="BKN"){$info_metar=$info_metar."FRAGMENTE à ".$altitude." &nbsp;&nbsp;&nbsp;&nbsp;";}
					if ($value->coverage=="FEW"){$info_metar=$info_metar."PEU NOMBREUX à ".$altitude." &nbsp;&nbsp;&nbsp;&nbsp;";}
					if ($value->coverage=="NSC"){$info_metar=$info_metar."AUCUN NUAGE SOUS 5000 pieds &nbsp;&nbsp;&nbsp;&nbsp;";}
					if ($value->coverage=="NCD"){$info_metar=$info_metar."PAS DE NUAGE DETECTE &nbsp;&nbsp;&nbsp;&nbsp;";}
					if ($value->coverage=="SKC"){$info_metar=$info_metar."CIEL CLAIR";}
				}
			}

		}
		if ($result!=null){echo $info_metar;}
		else{echo '<u><B>Aucune station dans un Rayons de 150 Nautiques du points que vous observez...</u></B>';}

	}




	function metar($sql,$center_lat,$center_lng,$range)
	{

		$sql->gen("SELECT  `raw_text`,`observation_time`,`temp_c`,`wind_dir_degrees`,`wind_speed_kt`,`dewpoint_c`,`visibility_statute_mi`,`altim_in_hg`,`sky_condition`,`sea_level_pressure_mb`,`wx_string`,`flight_category`,`metar_type`,`elevation_m`,`station_id` as `map_id`, `station_id`,`Latitude`,`Longitude`, `Latitude` as `latitude`, `Longitude` as `longitude`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( latitude ) ) ) ) AS distance FROM `e107_tks_metar` HAVING distance < ".$range." ORDER BY distance");

		while($row = $sql->fetch())
		{
			$result[]=$row;
		}

		if ($result!=null){echo json_encode($result);}
		else{echo json_encode(null);}
	}

	function map_vor($sql,$center_lat,$center_lng,$range)
	{
		$sql->gen("SELECT `Ident`,`Name`,`Elevation`,`Freq`,`Channel`,`Usage`, `Usage`,`Type`, `ID` as `map_id`, `Latitude`, `Longitude`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( Latitude ) ) ) ) AS distance FROM `e107_tks_Navaids` HAVING distance < ".$range." and (`Type`='1' or `Type`='2' or `Type`='3' or `Type`='4') ORDER BY distance");

		while($row = $sql->fetch())
		{
			if($row['Freq']!="0.000")
			{
				$result[]=$row;
			}

		}

		if ($result!=null){echo json_encode($result);}
		else{echo json_encode(null);}

		//var_dump($row);

	}
	function map_ndb($sql,$center_lat,$center_lng,$range)
	{
		$sql->gen("SELECT `Ident`,`Name`,`Elevation`,`Freq`,`Channel`,`Usage`, `Usage`,`Type`, `ID` as `map_id`, `Latitude`, `Longitude`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( Latitude ) ) ) ) AS distance FROM `e107_tks_Navaids` HAVING distance < ".$range." and (`Type`='5' or `Type`='7') ORDER BY distance");

		while($row = $sql->fetch())
		{
			if($row['Freq']!="0.000")
			{
				$result[]=$row;
			}

		}

		if ($result!=null){echo json_encode($result);}
		else{echo json_encode(null);}
	
	}

	function map_ils($sql,$center_lat,$center_lng,$range)
	{
		$sql->gen("SELECT `Ident`,`GsAngle`,`Elevation`,`Freq`,`LocCourse`, `RunwayID` as `map_id`, `Latitude`, `Longitude`,( 3959 * acos ( cos ( radians('".$center_lat."') ) * cos( radians( Latitude ) ) * cos( radians( Longitude ) - radians('".$center_lng."') ) + sin ( radians('".$center_lat."') ) * sin( radians( Latitude ) ) ) ) AS distance FROM `e107_tks_ILSes` HAVING distance < ".$range." ORDER BY distance");

		while($row = $sql->fetch())
		{
			if($row['ILSFrequency']!="0.000")
			{
				$result[]=$row;
			}

		}

		if ($result!=null){echo json_encode($result);}
		else{echo json_encode(null);}
	}
/*function data_black_box($db_m_5,$fn=null)
{

	if($fn!=null)
	{
		$where = '`flightnum` ="'. $fn.'"';

		$data = '*';

		$result = $db_m_5->select ('`' . PF_MARK_5_ENR . 'blackbox`', $data, $where, 'ORDER BY `id` DESC');

			//var_dump($result);

		$data_record=json_decode($result->record);

		foreach ($data_record as $key => $value) {
			if($value->data_type=='cycle')
			{
						//$real_route[]=array('lat' => $value->Lat,'lng' => $value->Lng);
				$real_route[]='{lat: '.$value->Lat.', lng: '.$value->Lng.'}';
			}
		}
		echo str_replace('"', '', json_encode($real_route));
	}
}
*/
if (!empty($_GET))
{
		//recherche des avions
	if (isset($_GET['mod']) && $_GET['mod'] =='map_aircraft')
	{
		map_aircraft($sql,date('Y-m-d H:i:s',strtotime("-1 hours")));
	}

		//recherche des avions pour une extration json
	elseif (isset($_GET['mod']) && $_GET['mod'] =='flying_pilote' && isset($_GET['filter']) && !empty($_GET['filter']))
	{
			//echo "hello world";
		flying_pilote($sql,$_GET['filter']);
			//map_aircraft($sql,date('Y-m-d H:i:s',strtotime("-1 hours")));
	}

		//recherche des avions pour un tableau
	elseif (isset($_GET['mod']) && $_GET['mod'] =='tab_aircraft')
	{
		tab_aircraft($sql,date('Y-m-d H:i:s',strtotime("-1 hours")));
	}

	elseif (isset($_GET['mod']) && $_GET['mod'] =='data_flight' && isset($_GET['fn']) && !empty($_GET['fn']))
	{
		data_flight($sql,$_GET['fn']);
				//echo 'test';
	}
	elseif (isset($_GET['mod']) && $_GET['mod'] =='data_flight' && isset($_GET['map_id']))
	{
		data_flight($sql,$_GET['map_id']);
	}
		//recherche des aéroports
	elseif (isset($_GET['mod']) && $_GET['mod'] =='map_airport' && !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		map_airport($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']);
	}

	//recherche des vor
	elseif (isset($_GET['mod']) && $_GET['mod'] =='map_vor'&& !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		map_vor($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']);
	}

	//recherche des ndb
	elseif (isset($_GET['mod']) && $_GET['mod'] =='map_ndb' && !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		map_ndb($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']);
	}
		//recherche des pistes
	elseif (isset($_GET['mod']) && $_GET['mod'] =='map_runway' && !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		map_runway($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']); 
	}

		//recherche des pistes ILS
	elseif (isset($_GET['mod']) && $_GET['mod'] =='map_ils' && !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		map_ils($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']); 
	}


	elseif (isset($_GET['mod']) && $_GET['mod'] =='data_schedule' && isset($_GET['fn']) && !empty($_GET['fn']))
	{
		data_schedule($db_m_5,$_GET['fn']);

	}

	elseif (isset($_GET['mod']) && $_GET['mod'] =='data_black_box' && isset($_GET['fn']) && !empty($_GET['fn']))
	{
		data_black_box($db_m_5,$_GET['fn']);
	}

		//recherche des infos complémentaire des aérports
	elseif (isset($_GET['mod']) && $_GET['mod'] =='info_aiport' && !empty($_GET['ID']))
	{
		info_aiport($sql,$_GET['ID']); 
	}

		//recherche des station météo Metar
	elseif (isset($_GET['mod']) && $_GET['mod'] =='metar' && !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		metar($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']); 
	}

		//recherche des station météo Metar
	elseif (isset($_GET['mod']) && $_GET['mod'] =='metar_one' && !empty($_GET['center_lng']) && !empty($_GET['center_lat']) && !empty($_GET['range']))
	{
		metar_one($sql,$_GET['center_lat'],$_GET['center_lng'],$_GET['range']); 
	}

}
?>


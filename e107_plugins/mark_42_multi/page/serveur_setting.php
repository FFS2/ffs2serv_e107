<script type="text/javascript">
	document.title = "Gestion des serveurs";
</script>

<?php 
require_once("class2.php");
require_once(HEADERF);
$sql = e107::getDb();
date_default_timezone_set("Europe/Brussels" );

$all_member =$sql->retrieve("user", "`user_id`,`user_loginname`","ORDER BY `user_loginname` ASC", true);

if(isset($_POST))
{
	if($_POST["action"]=="priv_state")
	{
		if(is_numeric($_POST ["private"])&&is_numeric($_POST ["admin"])&&is_numeric($_POST ["serveur_id"]))
		{
			$update = array(
				"private" =>$_POST ["private"],
				"WHERE" => "`serveur_id` = '".$_POST ["serveur_id"]."'"
				);
			//si le changement de visibilité du serveur est effectif
			if($sql->update('tks_serveur', $update))
			{
				//si l'on rend le serveur privé on le notifie de la sorte
				if($_POST ["private"]==1)
				{
					?>
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						<p>Vous venez de privatiser un serveur</p>
					</div>
					<?php
				}
					//sinon l'on rend le serveur public et on le notifie de la sorte
				else
				{
					?>
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
						<p>Vous venez de rendre un serveur public</p>
					</div>
					<?php
				}
			}
		}
	}

	if($_POST["action"]=="sup_member")
	{
		if(is_numeric($_POST ["user_id"])&&is_numeric($_POST ["serveur_id"]))
		{
			if($sql->delete("tks_serveur_member", "user_id='".$_POST ["user_id"]."' and serveur_id='".$_POST ["serveur_id"]."'"))
			{
				$update = array(
					"server" =>'1',
					"WHERE" => "`id` = '".$_POST ["slot_id"]."' "
					);

				if($sql->update('tks_mp', $update))
				{
					?>
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						<p>Vous venez de supprimer un membre de votre serveur et il a été déplacé vers le serveur public 1</p>
					</div>
					<?php
				}
			}
		}
	}
	if($_POST["action"]=="add_member")
	{
		if(is_numeric($_POST ["user_id"])&&is_numeric($_POST ["serveur_id"]))
		{
			$insert = array(
				'user_id' 			=> $_POST ["user_id"],
				'serveur_id'		=> $_POST ["serveur_id"],
				'administrateur'	=> '0'
				);
			if($sql->insert('tks_serveur_member', $insert))
			{

				?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<p>Vous venez de rajouter un membre sur votre serveur</p>
				</div>
				<?php
			}
		}
	}
	if($_POST["action"]=="add_admin")
	{
		if(is_numeric($_POST ["user_id"])&&is_numeric($_POST ["serveur_id"]))
		{
			$update = array(
				"administrateur" =>'1',
				"WHERE" => "`serveur_id` = '".$_POST ["serveur_id"]."' and `user_id` = '".$_POST ["user_id"]."'"
				);
			//si l'ajout d'administrateur est effectif
			if($sql->update('tks_serveur_member', $update))
			{

				?>
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<p>Vous venez d'élever un membre au grade d'administrateur </p>
				</div>
				<?php
			}
		}
	}
	if($_POST["action"]=="guest_pilote")
	{
		if(is_numeric($_POST ["slot_id"]) && is_numeric($_POST ["serveur_id"]))
		{
			$update = array(
				"server" =>$_POST ["serveur_id"],
				"WHERE" => "`id` = '".$_POST ["slot_id"]."' "
				);

			if($sql->update('tks_mp', $update))
			{

				?>
				<div class="alert alert-success alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<p>Vous venez d'inviter un pilote</p>
				</div>
				<?php
			}
		}
	}
	if($_POST["action"]=="undo_guest")
	{
		if(is_numeric($_POST ["slot_id"]))
		{
			$update = array(
				"server" =>"1",
				"WHERE" => "`id` = '".$_POST ["slot_id"]."' "
				);

			if($sql->update('tks_mp', $update))
			{

				?>
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<p>Vous venez d'expulser un invité de votre serveur.</p>
				</div>
				<?php
			}
		}
	}

	
	if($_POST["action"]=="sup_admin")
	{
		if(is_numeric($_POST ["user_id"])&&is_numeric($_POST ["serveur_id"]))
		{
			$update = array(
				"administrateur" =>'0',
				"WHERE" => "`serveur_id` = '".$_POST ["serveur_id"]."' and `user_id` = '".$_POST ["user_id"]."'"
				);
			//si l'ajout d'administrateur est effectif
			if($sql->update('tks_serveur_member', $update))
			{

				?>
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<p>Vous venez de rétrograder un membre au grade de simple pilote</p>
				</div>
				<?php
			}
		}
	}

}

		//on recherche la liste de tous les serveurs ou l'utilisateur en cours est adminstrateur
$server_admin =$sql->retrieve("tks_serveur_member", "*","user_id='". USERID ."' and administrateur='1'", true);

		//on recherche la liste de tous les serveurs ou l'utilisateur en cours est fondateur
$server_foundation =$sql->retrieve("tks_serveur", "*","administrateur='". USERID ."'", true);
foreach ($server_foundation	as $key => $value)
{
	$present_serveur=false;
	foreach ($server_admin	 as $key2 => $value2)
	{
		if ($value["serveur_id"]==$value2["serveur_id"]) $present_serveur=true;
	}
			//si l'utilisateur est un fondateur, on le rajoute à la liste des administrateurs
	if($present_serveur==false) $server_admin[]["serveur_id"]=$value["serveur_id"];
}

foreach ($server_admin	 as $key => $value)
{
			//on recherche les infos sur le serveur en cours
	$server_current =$sql->retrieve("tks_serveur","`serveur_name`,`serveur_id`,`private`","serveur_id='".$value['serveur_id']."'");

	?>
	<h4> Serveur: <?=$server_current["serveur_name"] ?></H4>
		<?php 	
		echo $frm = e107::getForm($server_current["serveur_name"].'_state_priv_form');
		echo $frm->open($server_current["serveur_name"].'_state_priv_form', 'post', './');
		echo $frm->hidden("action",'priv_state');
		echo $frm->hidden("admin",USERID);
		echo $frm->hidden("serveur_id",$server_current["serveur_id"]);

		if ($server_current["private"]==1)
		{
			echo $frm->hidden("private",'0');
			echo $frm->button("submit","Rendre le serveur public");
			echo $frm->close();
		}
		else
		{
			echo $frm->hidden("private",'1');
			echo $frm->button("submit","Privatisé le serveur",'','',array('class' => 'btn btn-danger'));
			echo $frm->close();
		}
				//on recherche la liste de tous les membres de ce serveur
		$server_member =$sql->retrieve("tks_serveur_member", "*","serveur_id='". $server_current["serveur_id"] ."'", true);

		?>
		<div class="container">
			<div class="row">
				<div class="col-lg-4" >
					<h4> Liste des pilotes autorisés à se connecter</H4>
						<div style="border: 1px solid;height: 250px; overflow-y: scroll;">	
							<table width="100%">
								<thead>
									<tr>
										<th width="90%"></th>
										<th width="5%"></th>
										<th width="5%"></th>
									</tr>
								</thead>
								<tbody>
									<?php 	
									foreach ($server_member	 as $key => $value2)
									{
										//recherche d'un membre coorespondant et extraction de son pseudo
										$s_member =$sql->retrieve("user", "user_loginname","user_id='". $value2['user_id']."'");

										/*
										*ajout d'un administrateur ou suppression d'un membre du serveur
										*/

										echo "<tr>";
										echo $frm = e107::getForm($value2['user_id'].'member_or_admin_form');
										echo $frm->open($value2['user_id'].'member_or_admin_form', 'post', './');
										//echo $frm->hidden("action",'add_admin');
										echo $frm->hidden("user_id",$value2['user_id']);
										echo $frm->hidden("serveur_id",$server_current["serveur_id"]);
										echo "<td>";
										echo "&nbsp;&nbsp;".$s_member."&nbsp;&nbsp;";
										echo "</td>";
										echo "<td>";
										if($value2['administrateur']==0)
										{
											echo '<button type="submit" name="action" value="add_admin" class=" btn btn-default" style="background: darkgrey;"><span class="glyphicon glyphicon-star-empty" aria-hidden="true" style="color: yellow;"></span></button>';
										}
										echo "</td>";
										echo "<td>";
										echo '<button type="submit" name="action" value="sup_member" class=" btn btn-default" style="background: darkgrey;"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span></button>';
										echo "&nbsp;&nbsp;";
										echo "</td>";
										echo $frm->close();

										echo "</tr>";
									}
									?>
								</tbody>
							</table>
						</div>
						<h4>Ajouter un membre:</h4>
						<?php 	
						echo $frm = e107::getForm($server_current["serveur_id"].'_add_member_form');
						echo $frm->open($server_current["serveur_id"].'_add_member_form', 'post', './');
						echo $frm->hidden("action",'add_member');
						echo $frm->hidden("serveur_id",$server_current["serveur_id"]);
						echo '<select name="user_id" style="background-color: black;font-size: 1.15em;">';
						echo '<option value="0">Aucun</option>';
						foreach ($all_member as $key2 => $value2)
						{
							echo '<option value="'.$value2['user_id'].'">'.$value2['user_loginname'].'</option>';
						}
						echo '</select>';
						echo '&nbsp;&nbsp;<button type="submit" class=" btn btn-default" style="background: darkgrey;">Rajouter ce membre</button>';
						echo $frm->close();
						?>



					</div>
					<div class="col-lg-4">
						<h4> Liste des administrateurs de ce serveur</H4>
							<div style="border: 1px solid;height: 250px; overflow-y: scroll;">	
								<table width="100%">
									<thead>
										<tr>
											<th width="95%"></th>
											<th width="5%"></th>
										</tr>
									</thead>
									<tbody>
										<?php 	



										foreach ($server_member	 as $key => $value2)
										{
											if($value2['administrateur']==1)
											{
											//recherche d'un membre coorespondant et extraction de son pseudo
												$s_member =$sql->retrieve("user", "user_loginname","user_id='". $value2['user_id']."'");

											/*
											*ajout d'un administrateur ou suppression d'un membre du serveur
											*/

											echo "<tr>";
											echo $frm = e107::getForm($value2['user_id'].'member_or_admin_form');
											echo $frm->open($value2['user_id'].'member_or_admin_form', 'post', './');
											//echo $frm->hidden("action",'add_admin');
											echo $frm->hidden("user_id",$value2['user_id']);
											echo $frm->hidden("serveur_id",$server_current["serveur_id"]);
											echo "<td>";
											echo "&nbsp;&nbsp;".$s_member."&nbsp;&nbsp;";
											echo "</td>";
											echo "<td>";
											echo '<button type="submit" name="action" value="sup_admin" class=" btn btn-default" style="background: darkgrey;"><span class="glyphicon glyphicon-remove" aria-hidden="true" style="color: red;"></span></button>';
											echo "&nbsp;&nbsp;";
											echo "</td>";
											echo $frm->close();
											echo "</tr>";
										}
									}
									?>
								</tbody>
							</table>
							
						</div>
					</div>
					<div class="col-lg-4">
						<h4> Liste des pilotes en vols</H4>
							<div style="border: 1px solid;height: 250px; overflow-y: scroll;">

								<table width="100%">
									<thead>
										<tr>
											<th width="90%"></th>
											<th width="5%"></th>
											<th width="5%"></th>
										</tr>
									</thead>
									<tbody>
										<?php
										//création de date limite de validité de la connexion
										$now = $date = date("Y-m-d H:i:s");
										$date = new DateTime($now);
										$date->sub(new DateInterval('PT35S'));
										$time = $date->format("Y-m-d H:i:s");

										$server_pilot_flight =$sql->retrieve("tks_mp", "*","(`lastupdate` between  '". $time ."' and '" . $now . "') and server= '" . $server_current["serveur_id"] . "' ORDER BY `pilotname` ASC", true);
										foreach ($server_pilot_flight as $key => $value2)
										{

											//recherche d'un membre coorespondant et extraction de son pseudo
											//$server_pilot_flight =$sql->retrieve("user", "user_loginname","user_id='". $value2['user_id']."'");

											/*
											*ajout d'un administrateur ou suppression d'un membre du serveur
											*/
											echo "<tr>";
											echo $frm = e107::getForm($value2['user_id'].'kick_or_ban_form');
											echo $frm->open($value2['user_id'].'kick_or_ban_form', 'post', './');
											//echo $frm->hidden("action",'add_admin');
											echo $frm->hidden("slot_id",$value2['id']);
											//echo $frm->hidden("serveur_id",$server_current["serveur_id"]);
											echo "<td>";
											echo "&nbsp;&nbsp;".$value2['pilotname']."&nbsp;&nbsp;";
											echo "</td>";
											echo "<td>";

											echo "</td>";
											echo "<td>";
											echo '<button type="submit" name="action" value="undo_guest" class=" btn btn-default" style="background: darkgrey;"><span class="glyphicon glyphicon-warning-sign" aria-hidden="true" style="color: yellow;"></span></button>';

											echo "</td>";

											echo $frm->close();

											echo "</tr>";
										}
										?>
									</tbody>
								</table>


							</div>
							<h4>Inviter un pilote externe:</h4>
							<?php 	


							//interrogation de la base de donnée avec la limite de temps
							$sql = e107::getDb();
							$all_active_pilote=$sql->retrieve("tks_mp", "*","(`lastupdate` between  '". $time ."' and '" . $now . "') ORDER BY `pilotname` ASC", true);

							echo $frm = e107::getForm($server_current["serveur_id"].'_guest_pilote_form');
							echo $frm->open($server_current["serveur_id"].'_guest_pilote_form', 'post', './');
							echo $frm->hidden("action",'guest_pilote');
							echo $frm->hidden("serveur_id",$server_current["serveur_id"]);
							echo '<select name="slot_id" style="background-color: black;font-size: 1.15em;">';
							echo '<option value="0">Aucun</option>';
							foreach ($all_active_pilote as $key2 => $value2)
							{
								echo '<option value="'.$value2['id'].'">'.$value2['pilotname'].'</option>';
							}
							echo '</select>';
							echo '&nbsp;&nbsp;<button type="submit" class=" btn btn-default" style="background: darkgrey;">Inviter ce pilote</button>';
							echo $frm->close();
							?>
						</div>
					</div>
				</div>
				<?php
			}
			echo $frm = e107::getForm('myform');
			echo $frm->open('myform', 'get', 'myscript.php', array('autocomplete' => 'on', 'class' => 'formclass',));

			echo $frm->close();

			require_once(FOOTERF);
			exit;
			?>



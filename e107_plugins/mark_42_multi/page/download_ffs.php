<?php 

function Get_FFST_Files ($folder, $ext = array ('txt'), $recursif = true)
{

	$files = array ();
	$dir = opendir ($folder);
	while ($file = readdir ($dir))
	{
		if ($file == '.' || $file == '..')
		{
			continue;
		}
		if (is_dir ($folder . '/' . $file))
		{
			if ($recursif == true)
			{
				$files = array_merge ($files, Get_FFST_Files ($folder . '/' . $file, $ext));
			}
		}
		else
		{
			foreach ($ext as $v)
			{
				if (strtolower ($v) == strtolower (substr ($file, - strlen ($v))))
				{
					$files[] = $folder . '/' . $file;
					break;
				}
			}
		}
	}
	closedir ($dir);
	return $files;
}
function clean_FFST_files($url,$to_del)
{
	$files=Get_FFST_Files ($url, array ("zip"), TRUE);
	foreach($files as $file)
	{
		if(strpos ($file, $to_del)>0)
		{
			$file = str_replace ($url, "", $file);
			$file = str_replace ($to_del, "", $file);
			$file = str_replace (".zip", "", $file);
			return $file;
		}
	}
}

$FFST_CURRENT = clean_FFST_files ('/home/ffs2/www/download/', '/FFSTracker_');
$FFST_P3D_CURRENT = clean_FFST_files ('/home/ffs2/www/download/', '/FFSTrackerP3D_');
$FFS2Play_CURRENT = clean_FFST_files ('/home/ffs2/www/download/', '/FFS2Play_');

?>
<meta http-equiv="refresh" content="0; URL=http://download.ffsimulateur2.fr/FFS2Play_<?=$FFS2Play_CURRENT ?>.zip">


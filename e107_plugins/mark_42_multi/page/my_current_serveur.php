<script type="text/javascript">
	document.title = "Le serveur en cours";
</script>

<?php 



//$ipv6 = "2a01:e34:ed5e:1350:eca8:60d7:8edb:b35c";

//echo $ipv4 = hexdec(substr($ipv6, 0, 2)). "." . hexdec(substr($ipv6, 2, 2)). "." . hexdec(substr($ipv6, 5, 2)). "." . hexdec(substr($ipv6, 7, 2));

require_once("class2.php");
require_once(HEADERF);
$sql = e107::getDb();

date_default_timezone_set("Europe/Brussels" );

//création de date limite de validité de la connexion
$now = $date = date("Y-m-d H:i:s");
$date = new DateTime($now);
$date->sub(new DateInterval('PT35S'));
$time = $date->format("Y-m-d H:i:s");

/*	recherche du slot de la personne connecté sur le site ayant un slot correspondant grace à son IP*/
$slot=$sql->retrieve("tks_mp", "*","ip='". $_SERVER["REMOTE_ADDR"] ."' and (`lastupdate` between  '". $time ."' and '" . $now . "') ");

//si l'identifiant du serveur demandé est bien numérique
if (is_numeric($_GET['serveur']))
{
	//si le joueur a de slot actif depuis  moins de 35 secondes
	if(count($slot)!=0)
	{
		//on recherche la liste de tous les serveurs existant
		$this_server=$sql->retrieve("tks_serveur","`serveur_name`,`serveur_id`,`private`","serveur_id='".$_GET['serveur']."'");


		//recherche d'un membre coorespondant et extraction de son email
		$s_member =$sql->retrieve("user", "user_email,user_id","user_loginname='". $slot['pilotname']."'");

		//on recherche la liste de tous les serveurs privés dont ce membre à accés
		$server_acces =$sql->retrieve("tks_serveur_member", "*","user_id='". $s_member["user_id"] ."' and serveur_id='".$_GET['serveur']."'");

		if(count($this_server)!=0)
		{
			//si c'est un serveur libre ou que membre a accés a ce serveur ou que c'est déjà son serveur actif
			if (($this_server['private']==0 and ($slot["server"]!=$this_server['serveur_id']))or($this_server['private']==1 and count($server_acces)!=0) or ($slot["server"]==$this_server['serveur_id']))
			{
				$update = array(
					"server" =>$_GET['serveur'],
					"WHERE" => "`ip` like '".$_SERVER["REMOTE_ADDR"]."'"
				);
				//si le changement de serveur est effectif
				if($sql->update('tks_mp', $update))
				{
					?>
					<div  data-example-id="dismissible-alert-js">
						<div class="alert alert-warning alert-dismissible fade in" role="alert">
							<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
							<strong>Soyez patient!</strong>
							<br>
							Le changement de serveur sera effectif dans moins de 30 secondes.
						</div>

					</div>
					<?php
				}
				//sinon le changment n'a pu être fait, pour raison technique ou pour serveur déjà sélectionné
				else
				{
					?>
					<div class="alert alert-danger alert-dismissible fade in" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
						<h4>Accés à ce serveur impossible</h4>
						<p>
							Vous venez de demander l'accés à un serveur mais un soucis technique empêche de vous le donner.<br><br>
							Si vous êtiez déjà sur ce serveur, merci de ne pas tenir compte de cette alerte.<br><br>
							Sinon veuillez contacter l'un des administrateurs, systéme.
						</p>

					</div>
					<?php
				}

				
			}
		//sinon l'accés au serveur est refusé
			else
			{
				?>
				<div class="alert alert-danger alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4>Accés à ce serveur refusé</h4>
					<p>
						Vous venez de demander l'accés à un serveur privé.<br><br>
						L'administrateur de ce serveur ne vous a pas donner l'accés.<br>
						Veuillez le contacter pour vous faire intégrer à ce serveur ou vous faire inviter ponctuellement.
					</p>

				</div>
				<?php
			}
		}
		else
		{
			?>
			<div class="alert alert-danger alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4>Serveur introuvable</h4>
				<p>
					Le serveur que vous demandez n'existe plus, ou n'est plus à cette endroit.<br>
					Si vous avez utiliser un raccourcis, pensez à le supprimer et / ou le mettre à jour.
				</p>

			</div>
			<?php
		}


		


		
	}
	else
	{
		?>
		<div class="alert alert-danger alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<h4>Avion non détecté</h4>
			<p>
				Le site n'arrive pas à détecter le fonctionnement de votre logiciel.<br>
				Veuillez vérfier que vos FFS2play et bien connecté ainsi que votre simulateur.<br>

				<?php 
				echo  $_SERVER["REMOTE_ADDR"]."<br>";
				?>
			</p>

		</div>
		<?php
	}
}
elseif($_GET['serveur']=='ON_GLOBAL' or $_GET['serveur']=='OFF_GLOBAL')
{
	//si le joueur a de slot actif depuis  moins de 35 secondes
	if(count($slot)!=0)
	{
		if($_GET['serveur']=='ON_GLOBAL')
			{
				$tx_mod=1;
			}
		elseif($_GET['serveur']=='OFF_GLOBAL')
		{
			$tx_mod=0;
		}

		$update = array(
			"tx_mod" =>$tx_mod,
			"WHERE" => "`ip` like '".$_SERVER["REMOTE_ADDR"]."'"
		);
				//si le changement de serveur est effectif
		if($sql->update('tks_mp', $update))
		{
			?>
			<div  data-example-id="dismissible-alert-js">
				<div class="alert alert-warning alert-dismissible fade in" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					<strong>Soyez patient!</strong>
					<br>
					Le changement de mod sera effectif dans moins de 30 secondes.
				</div>

			</div>
			<?php
		}
		else
		{
			?>
			<div class="alert alert-danger alert-dismissible fade in" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4>Changement du mod impossible</h4>
				<p>
					Suite à un disfonctionnement inconnu, le changement de mod n'a pas été pris en compte.<br>
					Veuillez contacter un adminstrateur pour identifier le problème.
				</p>

			</div>
			<?php
		}
	}
}

/*	recherche du slot de la personne connecté sur le site ayant un slot correspondant grace à son IP*/
$slot=$sql->retrieve("tks_mp", "*","ip='". $_SERVER["REMOTE_ADDR"] ."' and (`lastupdate` between  '". $time ."' and '" . $now . "') ");
//var_dump($slot);

//si le joueur n'a pas de slot actif depuis 35 secondes
if(count($slot)==0)
{
	?>
	<div class="alert alert-danger alert-dismissible fade in" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		<h4>Avion non détecté</h4>
		<p>
			Le site n'arrive pas à détecter le fonctionnement de votre logiciel.<br>
			Veuillez vérfier que vos FFS2play et bien connecté ainsi que votre simulateur.
			<?php 
			echo  $_SERVER["REMOTE_ADDR"]."<br>";
			?>
		</p>

	</div>
	<?php
}
//sinon
else
{
	
	//recherche d'un membre coorespondant et extraction de son email
	$s_member =$sql->retrieve("user", "user_email,user_id","user_loginname='". $slot['pilotname']."'");

	//si un memebre est détecté
	if ($s_member != NULL)
	{
		echo "<h4> Le serveur vous a reconnu en tant que :".$slot['pilotname']."&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-ok' aria-hidden='true' style='color: green;'></span></H4>";
	}
	//sinon
	else
	{
		echo "<h4> Le serveur vous a reconnu en tant que :".$slot['pilotname']."&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-minus-sign' aria-hidden='true' style='color: red;'></span></H4>";
		echo "<h4> Attention vous n'êtes pas identifié avec un compte de notre site, il se peut que celà vous empéche d'acceder à tous vos serveurs.</H4>";
	}
	echo "<div style='width: 200px;margin-left: auto;margin-right: auto;'>";
	//on recherche la liste de tous les serveurs existant
	$all_server=$sql->retrieve("tks_serveur","`serveur_name`,`serveur_id`,`private`","ORDER BY `serveur_name` ASC",true);

	foreach ($all_server as $key => $value)
	{
		//on recherche la liste de tous les serveurs privés dont ce membre à accés
		$server_acces =$sql->retrieve("tks_serveur_member", "*","user_id='". $s_member["user_id"] ."' and serveur_id='".$value["serveur_id"]."'");

		//si il est déjà ce sur ce serveur
		if ($slot["server"]==$value['serveur_id'])
		{
			echo '<button type="button" class="btn btn-success" style="width: 200px;">'.$value['serveur_name'].'</button>';

		}
		//sinon si c'est un serveur libre et que le membre n'y est pas
		elseif ($value['private']==0 and ($slot["server"]!=$value['serveur_id']))
		{
			echo '<a href="./'.$value['serveur_id'].'"><button type="button" class="btn btn-default" style="width: 200px;">'.$value['serveur_name'].'</button></a>';

		}
		//sinon si c'est un serveur privé, que le membre n'est pas dessus, et qu'il n'y a pas accés
		elseif ($value['private']==1 and ($slot["server"]!=$value['serveur_id']) and count($server_acces)==0)
		{
			echo '<button type="button" class="btn btn-danger" style="width: 200px;">'.$value['serveur_name'].'</button>';

		}
		//sinon si c'est un serveur privé, que le membre n'est pas dessus mais qu'il y a accés
		elseif($value['private']==1 and ($slot["server"]!=$value['serveur_id']) and count($server_acces)!=0)
		{
			echo '<a href="./'.$value['serveur_id'].'"><button type="button" class="btn btn-default" style="width: 200px;">'.$value['serveur_name'].'</button></a>';
		}
		//sinon dans un cas non prévu
		else
		{
			echo '<button type="button" class="btn btn-warning" style="width: 200px;">'.$value['serveur_name'].'</button>';
		}
		echo "<br>";
	}
	echo "<br><br>";
	if ($slot["tx_mod"] == 0)
	{
		//var_dump($slot);
		echo '<a href="./ON_GLOBAL"><button type="button" class="btn btn-success" style="width: 200px;">Activer le mod Global</button></a>';
	}
	elseif ($slot["tx_mod"] == 1)
	{
		echo '<a href="./OFF_GLOBAL"><button type="button" class="btn btn-warning" style="width: 200px;">Désactiver le mod Global</button></a>';
	}
	echo "<div>";

	

}

require_once(FOOTERF);
exit;
?>



 <script type="text/javascript">
	document.title = "FFS2Play/Carte avancée";
</script>

  <!--css pour la fenêtre mobile des appareils -->
  <link rel="stylesheet" type="text/css" href="./../e107_plugins/mark_42_multi/page/m_map/css/leftColOverlay.css">

  <?php
 /*
 * e107 website system
 *
 * Copyright (C) 2008-2013 e107 Inc (e107.org)
 * Released under the terms and conditions of the
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
 *
 */

 if (!defined('e107_INIT')) 
 {
		//tentative de chargement des classes de E107
 	if (! @include_once("./../../class2.php"))
 		throw new Exception ("Votre E107 est introuvable");
	//tentative de chargement du gestionnaire de carte
 	if (! @include_once("./".e_PLUGIN."mark_42_multi/class/map_data.php"))
 		throw new Exception ("Le gestionnaire de carte est introuvable");


 };
 require_once("class2.php");
 require_once(HEADERF);

 define('api_google_map_key','your_api_key');

 ?>


 <!--script de communication entre le client et la base de données -->
 <script src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/js/app.js"></script>

 <!--script de gestion des fenêtre mobile -->
 <script src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/js/windows.js"></script>

 <!--script des variables éléments vor ndb ils aiports...  -->
 <script src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/js/element_map.js"></script>

 <!--script des compas...  -->
 <script src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/js/compass.js"></script>



 <body style="padding: 5px;">

 	<!-- Modal loading -->
 	<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 		<div class="modal-dialog" role="document">
 			<div class="modal-content">
 				<img src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/images/loading.gif"> Chargement des données en cours
 			</div>
 		</div>
 	</div>

 	<!-- Modal aircraft solo -->
 	<div class="modal fade" id="aircraft_distrib" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 		<div class="modal-dialog" role="document">
 			<div class="modal-content">
 				<div class="modal-header">
 					<h5 class="modal-title" >Répartition des joeurs / fenêtre</h5>
 					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 						<span aria-hidden="true">&times;</span>
 					</button>
 				</div>
 				<div class="modal-body">
 					<div id="aircraft_distrib_list"></div>
 				</div>
 				<div class="modal-footer">
 					<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
 				</div>
 			</div>
 		</div>
 	</div>


 	<!-- Modal metar.... solo -->
 	<div class="modal fade" id="info_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 		<div class="modal-dialog" role="document">
 			<div class="modal-content">
 				<div class="modal-header">
 					<h5 class="modal-title" id="aircraft_info_title">Information sur l'objet</h5>
 					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
 						<span aria-hidden="true">&times;</span>
 					</button>
 				</div>
 				<div class="modal-body">
 					<div id="leftColOverlay">

 						<div id="info_data_map"><div id="aircraft_info_text"></div></div>
 					</div>

 				</div>
 				<div class="modal-footer">
 					<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
 				</div>
 			</div>
 		</div>
 	</div>	


 	<div id="airport_map">
 		<div id="airport_map_header"></div>
 		<div id="airport_info_map">
 		</div>
 	</div>

 	<?php 
 	for ($i=0; $i <10 ; $i++)
 	{ 
 		echo '<div id="aircraft_map_'.$i.'">
 		<div id="aircraft_map_'.$i.'_header"></div>
 		
 		<div id="aircraft_info_map_'.$i.'"><img src="./../'.e_PLUGIN .'mark_42_multi/page/m_map/images/loading.gif"> Recherche des données en cours</div>
 		</div>';
 	}

 	echo "<style>";
 	for ($i=0; $i <10 ; $i++)
 	{
 		echo "#aircraft_map_".$i." {position: absolute;display: none;z-index: 9;top: 0px;right: 0px;background: rgba(0,0, 0, 0.7);border: 3px solid #84d484;max-width: 20%;//width: 350px//height:500px;max-height: 50%;resize: both;overflow: auto;}

			#aircraft_map_".$i."_header { padding: 5px; cursor: move; z-index: 10; background-color: #84d484;; color: black;}";
 	}
 	echo "</style>";
 	?>
 	<div id="aircraft_map">
 		<div id="aircraft_map_header"></div>
 		<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#aircraft_map').toggle(500);")>
 			<span aria-hidden="true">&times;</span>
 		</button>
 		<div id="aircraft_info_map"></div>
 	</div>	

 	<div id="ffs_map" style="//float:right;"></div>
 	<div id="wrapper" style="position: absolute; bottom:0px;">
 		<div id="top_map" style="font-size: 14px;background-color: black;">
 			<div>
 				<span id="x1" class="position"></span>	
 				<span id="y1" class="position"></span>
 				<span id="x2" class="position"></span>
 				<span id="y2" class="position"></span>
 				<span id="zoom" class="position"></span>
 				<span id="x_center" class="position"></span>
 				<span id="y_center" class="position"></span>
 				<span>
 					<select id="styles_selector" style="width: inherit; background-color: black;"">
 						<option>Sélectionnez un style de carte</option>
 						<option value="vfr_rouds_style">VRF Routes</option>
 						<option value="dark_style">Style obscure</option>
 						<option value="water_only_style">Pieds dans l'eau?</option>
 						<option value="tactical_style">Frontières</option>
 						<option value="black_styles">Fond noir</option>
 						<option value="unsaturated_browns_style">Unsaturated browns</option>
 					</select>
 				</span>
 			</div>
 		</div>
 	</div>
 	<div id="wrapper_2" style="position: absolute; bottom:0px; right: 0px;background-color: black;">
 		<span style="display: inline-flex;" class="position">
 			<input id="address" type="textbox" value="LFPO" style="background-color:black">
 		</span>
 		<span style="display: inline-flex;" class="position">
 			<input id="submit" type="button" value="Rechercher" style="background-color:black" autocomplete="off">
 		</span>
 	</div>

 	<div  style="position: absolute;top: 0px;left: 0px;width: 100%;//height: 50px;background-color: cadetblue;
 	">
 	<marquee><span id="metar_top"></span></marquee>
 </div>



 <div id="left_map" style="position: absolute;top: 60px;left: 0px;">
 	<div id="left_map_header">Menu</div>

 	<span> 
 		<button type="button" class="btn btn-primary btn-sm" id='aircraft_bp' style="width: 100px;" onclick="if(aircraft_bp === true){aircraft_bp = false;aircrafts.delete_info();}else{aircraft_bp = true;}">Appareils</button>
 	</span>
 	<span>
 		<select id="range_selector" style="background-color: black;">
 			<option value="25">25 Nautiques</option> 
 			<option value="50">50 Nautiques</option> 
 			<option value="100">100 Nautiques</option> 
 			<option value="150">150 Nautiques</option> 
 			<option value="200">200 Nautiques</option> 
 			<option value="250">250 Nautiques</option> 
 			<option value="500">500 Nautiques</option> 

 		</select>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='airport_bp' style="width: 100px;" onclick="if(airport_bp === true){airport_bp = false;airports.delete_info();}else{airport_bp = true;}">Aéroports</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='runway_bp' style="width: 100px;" onclick="if(runway_bp === true){runway_bp = false;runway.delete_info();}else{runway_bp = true;}">Pistes</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='ils_bp' style="width: 100px;" onclick="if(ils_bp === true){ils_bp = false;ils.delete_info();}else{ils_bp = true;}">ILS</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='vor_bp' style="width: 100px;" onclick="if(vor_bp === true){vor_bp = false;}else{vor_bp = true;vor.delete_info();}">VOR</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='ndb_bp' style="width: 100px;" onclick="if(ndb_bp === true){ndb_bp = false;}else{ndb_bp = true;ndb.delete_info();}">NDB</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='metar_bp' style="width: 100px;" onclick="if(metar_bp === true){metar_bp = false;metar.delete_info();}else{metar_bp = true;}">METAR</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-info btn-sm" id='AID' style="width: 100px;" onclick="$('#aircraft_distrib').modal('show');">AID</button>
 	</span>
 	<span>
 		<button type="button" class="btn btn-danger btn-sm" id='focus_bp' style="width: 100px;" onclick="
 		if(focus_bp === true)
 		{
 			focus_bp = false;
 			if(focus_selected!='')
 			{
 				focus_selected=''
 			}
 		}
 		else{focus_bp = true;map.setZoom(10)}">focus</button>
 	</span>
 	<span>
 		<select id="focus_selector" style="background-color: black;">
 		</select>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='compas_bp' style="width: 100px;" onclick="if(compas_bp === true){compas_bp = false;compas_destroy();}else{compas_bp = true;compas_make()}">Compas</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='compas_bp_2' style="width: 100px;" onclick="if(compas_bp_2 === true){compas_bp_2 = false;compas_destroy();}else{compas_bp_2 = true;free_compas_make()}">Compas mobile</button>
 	</span>
 	<span> 
 		<button type="button" class="btn btn-danger btn-sm" id='reset_bp' style="width: 100px;" onclick="
 		metar.delete_info();ndb.delete_info();vor.delete_info();ils.delete_info();runway.delete_info();airports.delete_info();aircrafts.delete_info();metar_bp =ndb_bp =vor_bp =ils_bp =runway_bp =airport_bp =aircraft_bp = false;">Reset</button>
 	</span>

 </div>

 <!--script des styles...  -->
 <script src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/js/style.js"></script>



 <!--script de création des gestionnaires de fenêtres mobiles -->
 <script type="text/javascript">
 	dragElement(document.getElementById(("airport_map")));
 	dragElement(document.getElementById(("aircraft_map")));
 	dragElement(document.getElementById(("left_map")));

 	for (var i = 0; i<10; i++)
 	{
 		dragElement(document.getElementById(("aircraft_map_"+i)));
 	}
 </script>


 <script type="text/javascript">
 	$( document ).ready(function(){$('#loading').modal('show')});

 	function alert_data(data,op)
 	{
 		alertObject(data);
 	}

 	function alertObject(obj){console.log(obj);}

 	function convertFromBaseToBase(str, fromBase, toBase)
 	{

 		var num = parseInt(str, fromBase);
 		if(str!=""){
 			var result=num.toString(toBase);

 			if(result.toString()=="NaN"){
 				result="Invalid Input";
 			}
 			return result/10000;
 		}
 		else
 		{
 			return;	
 		}
 	}

 	var metar = new element(settings_metar);
 	var vor = new element(settings_vor);
 	var ndb = new element(settings_ndb);
 	var ils = new element(settings_ils);
 	var runway = new element(settings_runways);
 	var airports = new element(settings_airports);
 	var aircrafts = new element(settings_aircrafts);
 	var last_metar="";

 	var airport_bp=runway_bp=vor_bp=ils_bp=ndb_bp=focus_bp=metar_bp=compas_bp=compas_bp_2=false;
 	var aircraft_bp = true;
 	var focus_select = false;
 	var range_selected=25;
 	var focus_selected='';
 	var option_focus='<option selected>focus sur</option>';

 	var	routeMarkers,flightPath,flightPath2,depMarker,arrMarker,map;

 	var color_file=0;

 	var count_metar_top=0;
 	var count_AID=0;



 	var Options;
 	var last_string="";
 	var refreshTime_aev=2000;

 	var run_once = false;
 	var label_control;
 	var first=0;

 	var AID=" ";
 	var Last_AID=" ";
 	var AID_array=["none","none","none","none","none","none","none","none","none","none"];

 	function element(settings)
 	{
 		this.attributs={name:settings.name,labels:settings.labels,points:settings.points,point:settings.point,draw:settings.draw,label:settings.label,drawing:settings.drawing,update:settings.update,icon:settings.icon,info_window:settings.info_window,label_fontSize: settings.label_fontSize,label_strokeColor: settings.label_strokeColor,label_fontColor: settings.label_fontColor,label_strokeWeight: settings.label_strokeWeight,label_align: settings.label_align,draw_strokeColor: settings.draw_strokeColor,draw_strokeOpacity: settings.draw_strokeOpacity,draw_strokeWeight: settings.draw_strokeWeight,};

 		this.search_info=function (Methode,Url,String,attributs=this.attributs)
 		{
 			var xhr = getHttpRequest();
 			xhr.onreadystatechange = function ()
 			{
 				if (xhr.readyState === 4 && xhr.status == 200)
 				{
 					result=xhr.responseText;
 					if(result!="" || result!="null")
 					{
 						//alertObject(result);
						//alert(JSON.parse(result));
						//alertObject(JSON.parse(result));
						attributs.last_req=String;
						distrib_data(JSON.parse(result),attributs);
					}
				}
			};
			xhr.open(Methode, Url + '?' + String, true);
			xhr.send();
		};

		this.delete_info=function (attributs=this.attributs)
		{
			if(attributs.draw!= null && attributs.drawing===true)
			{
				if(attributs.draw.length >= 0)
				{
					for(var index in attributs.draw)
					{ 
						attributs.draw[index].setMap(null);
						delete attributs.draw[index];
					}
				}
				attributs.draw.length = 0;
			}
			if(attributs.labels!= null && attributs.label===true)
			{
				if(attributs.labels.length >= 0)
				{
					for(var index in attributs.labels)
					{ 
						attributs.labels[index].setMap(null);
						delete attributs.labels[index];
					}
				}
				attributs.labels.length = 0;
			}

			if(attributs.points!= null && attributs.point===true)
			{
				if(attributs.points.length >= 0)
				{
					for(var index in attributs.points)
					{ 
						attributs.points[index].setMap(null);
						delete attributs.points[index];
					}
				}
				attributs.points.length = 0;
			}
		};
	};

	function distrib_data(data_map,attributs)
	{
			//console.log(data_map);
			if (data_map!= null)
			{
				for(var index in attributs.points)
				{ 
					var present=false;
					for (var i = 0; i < data_map.length; i++) 
					{
						if(index==data_map[i].map_id)
						{
							present=true;
							break;	
						}
					}

					if(present==false)
					{
						attributs.points[index].setMap(null);
						
						if(attributs.label)
						{
							attributs.labels[index].setMap(null);
							delete attributs.labels[index];
						}

						delete attributs.points[index];
						
					}
				}
				for (var i = 0; i < data_map.length; i++) 
				{
					if (data_map[i]!= null)
					{
						if(attributs.point)
						{

							if(attributs.points[data_map[i].map_id] == undefined)
							{
								points_create(data_map[i],attributs);
							}
							else if (attributs.update)
							{
								if(attributs.name==="aircrafts")
								{
									var my_lat=parseFloat(data_map[i].Latitude).toFixed(4);
									var my_lng=parseFloat(data_map[i].Longitude).toFixed(4);
									var pos = new google.maps.LatLng(my_lat, my_lng);
									
									if (attributs.points[data_map[i].map_id].getPosition().lat()!=my_lat || attributs.points[data_map[i].map_id].getPosition().lng()!=my_lng)
									{
										
										attributs.points[data_map[i].map_id].setPosition(pos);

										attributs.points[data_map[i].map_id].setMap(null);
										delete attributs.points[data_map[i].map_id];

										points_create(data_map[i],attributs);
									}
									
									if(data_map[i].ias > 250)
									{
										color_file =1;
									}

									if(focus_bp==true && focus_selected==data_map[i].pilotname)
									{
										map.setCenter(pos);
										if(compas_bp==true)
										{
											Compas.setPosition(pos);
										}
									}
								}
							}

							
						}
						if(attributs.info_window)
						{
							windows_create(data_map[i],attributs);
						}

						if(attributs.label)
						{
							if(attributs.labels[data_map[i].map_id]== undefined)
							{
								labels_create(data_map[i],attributs);
							}
							else if (attributs.update)
							{
								if(attributs.name==="aircrafts")
								{
									attributs.labels[data_map[i].map_id].set('position', new google.maps.LatLng(data_map[i].Latitude, data_map[i].Longitude));
									attributs.labels[data_map[i].map_id].set('text', data_map[i].pilotname+' '+data_map[i].heading+'°   '+data_map[i].alt+' Ft   '+data_map[i].ias+' Kts');
								}

							}
						}
						if(attributs.drawing)
						{
							if(attributs.draw[data_map[i].map_id]== undefined)
							{
								draw_create(data_map[i],attributs);
							}
						}
					}
				}
			}
		};

		function close_loader()
		{
			$('#loading').modal('hide');
		}

		function points_create(data_map,attributs)
		{
			var my_lat=parseFloat(data_map.Latitude).toFixed(4);
			var my_lng=parseFloat(data_map.Longitude).toFixed(4);

			var pos = new google.maps.LatLng(my_lat, my_lng);

			var point_name='none'
			if(attributs.name==="aircrafts")
			{
				var color_file=0;

				if(data_map.onground==1)
				{
					color_file = 2;
				}
				else
				{
					if (data_map.squawk==7500 || data_map.squawk==7600 || data_map.squawk==7700 )
					{
						color_file = 1;
					}
					else
					{
						if (parseInt(data_map.ias)>250 && parseInt(data_map.alt)<10000)
						{
							color_file = 1;
						}
						else
						{
							color_file = 0;
						}
					}
				}
				attributs.icon=icon_Map_AI(data_map.heading,10,color_file);
				point_name=data_map.pilotname;
			}

			if(attributs.name==="vor")
			{
				if(data_map.Type==1)
				{
					attributs.icon='./../e107_plugins/mark_42_multi/page/map/images/vor.png';
				}
				if(data_map.Type==2)
				{
					attributs.icon='./../e107_plugins/mark_42_multi/page/map/images/vortac.png';
				}
				if(data_map.Type==3)
				{
					attributs.icon='./../e107_plugins/mark_42_multi/page/map/images/tacan.png';
				}
				if(data_map.Type==4)
				{
					attributs.icon='./../e107_plugins/mark_42_multi/page/map/images/vordme.png';
				}

			}

			attributs.points[data_map.map_id] = new google.maps.Marker({
				position: pos,
				map: map,
				icon: attributs.icon,
				optimized: false,
				flightdetails: data_map,
				name:point_name,

			});
		}
		function draw_create(data_map,attributs)
		{
			if(attributs.name==="ils")
			{
				var p1 = new google.maps.LatLng(data_map.Latitude, data_map.Longitude);
				var distance_1=-18000;
				var distance_2=-17000;
				var angle_1=parseInt(data_map.LocCourse)+2;
				var angle_2=parseInt(data_map.LocCourse)-2;

				var p2 = new google.maps.geometry.spherical.computeOffset(p1, distance_1, angle_1);
				var p3 = new google.maps.geometry.spherical.computeOffset(p1, distance_2, data_map.LocCourse);
				var p4 = new google.maps.geometry.spherical.computeOffset(p1, distance_1, angle_2);

				var drawn_Coords = [p1, p2,p3,p4];
			}
			if(attributs.name==="runways")
			{
				var startLL = new google.maps.LatLng(data_map.Latitude,data_map.Longitude);
				var distance=0.3048*data_map.Length;
				var endLL = new google.maps.geometry.spherical.computeOffset(startLL, distance, data_map.TrueHeading);

				var drawn_Coords = [startLL, endLL];
			}

			attributs.draw[data_map.map_id] = new google.maps.Polygon({
				paths: drawn_Coords,
				strokeColor: attributs.draw_strokeColor,
				strokeOpacity: attributs.draw_strokeOpacity,
				strokeWeight: attributs.draw_strokeWeight,
			});
			attributs.draw[data_map.map_id].setMap(map);
		}
		function labels_create(data_map,attributs)
		{
			var pos=p1 = new google.maps.LatLng(data_map.Latitude, data_map.Longitude);
			if(attributs.name==="metar")
			{
				var text= data_map.station_id;
			}
			if (attributs.name==="vor")
			{
				var text= data_map.Ident+' '+convertFromBaseToBase(data_map.Freq, 10, 16)+'   '+data_map.Elevation+' Ft';
			}
			if (attributs.name==="ndb")
			{
				var text= data_map.Ident+' '+convertFromBaseToBase(data_map.Freq, 10, 16);
			}
			if(attributs.name==="ils")
			{
				var text=data_map.Ident+'    '+data_map.LocCourse+'°    '+convertFromBaseToBase(data_map.Freq, 10, 16)+'    Glide  '+data_map.GsAngle+'°';
				attributs.label_align = (data_map.LocCourse<180) ? 'right' : 'left';

				var distance_1=-18000;
				var angle_2=parseInt(data_map.LocCourse)-2;
				pos=new google.maps.geometry.spherical.computeOffset(p1, distance_1, angle_2);

			}
			if(attributs.name==="aircrafts")
			{
				var text=data_map.pilotname+' '+data_map.heading+'°   '+data_map.alt+' Ft   '+data_map.ias+' Kts';
				pos = new google.maps.LatLng(data_map.Latitude, data_map.Longitude);
			}
			if(attributs.name==="airports")
			{
				var text=data_map.ICAO+'    '+data_map.Name;
			}

			attributs.labels[data_map.map_id] = new MapLabel({
				text: text,
				position: pos,
				map: map,
				fontSize: attributs.label_fontSize,
				strokeColor: attributs.label_strokeColor,
				fontColor: attributs.label_fontColor,
				align: attributs.label_align,
				strokeWeight:attributs.label_strokeWeight,
			});
		}

		function windows_airport(data_map,attributs)
		{
			var info_text='Aucune info disponible.';
			var String = 'ID='+data_map.ICAO+'&mod=info_aiport';
			//alert(String);
			var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
			var Methode="GET";

			var xhr = getHttpRequest();
			xhr.onreadystatechange = function ()
			{
				if (xhr.readyState === 4 && xhr.status == 200)
				{
					result=xhr.responseText;
					if(result!="" || result!="null")
					{ 
						data = JSON.parse(result);
						info_text="<div style='padding: 45px;color: #84d484;text-shadow: -1px 0 #025802, 0 1px #025802, 1px 0 #025802, 0 -1px #025802;'>";
						for (var i = 0; i < data.length; i++) 
						{
							info_text=info_text+"<hr><U><B>Piste "+data[i].Runways_Ident+"</B></U><br>";

							if(data[i].Freq===null)
							{
								info_text=info_text+"Radio guidage: Aucun<br><br>";
							}
							else
							{
								info_text=info_text+"<i>Radio guidage:</i><br>";
								info_text=info_text+"Code d'identification: "+data[i].ILS_Ident+"<br>";
								info_text=info_text+"Fréquence: "+convertFromBaseToBase(data[i].Freq, 10, 16) +"<br>";
								info_text=info_text+"Catégorie n°: "+data[i].Category+"<br>";
								info_text=info_text+"Angle de descente: "+data[i].GsAngle+"°<br><br>";
							}

							info_text=info_text+"<B>Axe de piste</B>: "+Math.round(data[i].TrueHeading)+"°<br><B>Surface: </B>"+data[i].Surface+"<br><B>Longueur: </B>"+Math.round(data[i].Length/3,28084)+" Mètres<br><B>Largeur: </B>"+Math.round(data[i].Width/3,28084)+" Mètres<br><B>Elevation:</B>"+data[i].Elevation+" Pieds<br><br>";
						}

						document.getElementById('airport_map_header').innerHTML="<span style='font-size: 1.5em;font-weight: 800;'>"+data_map.ICAO+' '+data_map.Name+":</span>";
						document.getElementById('airport_info_map').innerHTML=info_text;
						$('#airport_map').show(500);

					}
				}
			};
			xhr.open(Methode, Url + '?' + String, true);
			xhr.send();
		}
		function windows_aircraft(data_map,attributs)
		{
			var info_text='Aucune info disponible.';
			var String = 'mod=data_flight&map_id='+data_map.map_id;
			var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
			var Methode="GET";

			var xhr = getHttpRequest();
			xhr.onreadystatechange = function ()
			{
				if (xhr.readyState === 4 && xhr.status == 200)
				{
					result=xhr.responseText;
					if(result!="" || result!="null")
					{
						document.getElementById('aircraft_info_map').innerHTML="<div id='leftColOverlay'><div id='info_data_map'><div id='aircraft_info_text'>"+result+"</div></div></div>";
						document.getElementById('aircraft_map_header').innerHTML=data_map.pilotname;
						$('#aircraft_map').show(500);
					}
				}
			};
			xhr.open(Methode, Url + '?' + String, true);
			xhr.send();
		}

		function windows_metar(data_map,attributs)
		{
			var info_metar='<div style="color: #333;//text-shadow: -1px 0 #025802, 0 1px #025802, 1px 0 #025802, 0 -1px #025802;"><span style="font-size: 1.5em;font-weight: 800;">Station de '+data_map.station_id+'</span><br><br><B>Mise à jourle</B>: '+data_map.observation_time+'<br><br><B>Tempéraure</B>: '+data_map.temp_c+'°c<br><B>Point de rosée</B>: '+data_map.dewpoint_c+'°c<br><B>Direction du vent</B>: '+data_map.wind_dir_degrees+'°<br><B>Vitesse du vent</B>: '+data_map.wind_speed_kt+' Noeuds<br><B>Visibilité</B>: '+data_map.visibility_statute_mi+' Miles Nautique<br><B>Pression au niveau de la mer</B>: '+data_map.sea_level_pressure_mb+' Millibar';

			var sky_condition=JSON.parse(data_map.sky_condition);


			var level='';
			if (sky_condition!= null)
			{
				for (var i = 0; i < sky_condition.length; i++) 
				{
					if(sky_condition[i].coverage=='OVC'){level=level+'<br>COUVERT à '+sky_condition[i].altitude*100+' pieds';}
					if(sky_condition[i].coverage=='SCT'){level=level+'<br>EPART à '+sky_condition[i].altitude*100+' pieds';}
					if(sky_condition[i].coverage=='BKN'){level=level+'<br>FRAGMENTE à '+sky_condition[i].altitude*100+' pieds';}
					if(sky_condition[i].coverage=='FEW'){level=level+'<br>PEU NOMBREUX à '+sky_condition[i].altitude*100+' pieds';}
					if(sky_condition[i].coverage=='NSC'){level=level+'<br>AUCUN NUAGE SOUS 5000 pieds';}
					if(sky_condition[i].coverage=='NCD'){level=level+'<br>PAS DE NUAGE DETECTE';}
					if(sky_condition[i].coverage=='SKC'){level=level+'<br>CIEL CLAIR';}
				}
			}
			info_metar=info_metar+'<br><br><U><B>Planchés nuageux</U></B>'+level;

			document.getElementById('info_data_map').innerHTML=info_metar+"</div>";
			$('#info_data').modal('show');
		}

		function windows_create(data_map,attributs)
		{
			google.maps.event.clearInstanceListeners(attributs.points[data_map.map_id]);
			if(attributs.name==='metar')
			{
				google.maps.event.addListener(attributs.points[data_map.map_id], 'mousedown', function(event) {windows_metar(data_map,attributs)});
			}


			if(attributs.name==='airports')
			{
				google.maps.event.addListener(attributs.points[data_map.map_id], 'mousedown', function(event) {windows_airport(data_map,attributs)});

			}

			if(attributs.name==='aircrafts')
			{

				for(var index in AID_array)
				{ 
					if(AID_array[index]!=="none" && AID_array[index]==data_map.map_id)
					{
						var target_windows=index;
						make_windows(AID_array[index],'aircraft_map_'+target_windows+'_header','aircraft_info_map_'+target_windows,'aircraft_map_'+target_windows,"S"+target_windows);
					}
				}

				if(focus_selected==data_map.pilotname && $("#aircraft_map").is(':visible'))
				{
					make_windows(data_map.map_id,'aircraft_map_header','aircraft_info_map','aircraft_map',"focus");
				}

				function make_windows(map_id,header,content,mother,index)
				{
					var String = 'mod=data_flight&map_id='+data_map.map_id;
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";

					var xhr = getHttpRequest();
					xhr.onreadystatechange = function ()
					{
						if (xhr.readyState === 4 && xhr.status == 200)
						{

							result=xhr.responseText;
							if(result!="" || result!="null")
							{
								document.getElementById(content).innerHTML="<div id='leftColOverlay'><div id='info_data_map'><div id='aircraft_info_text'>"+result+"</div></div></div>";

								var close_bp="<button type='button' style ='position:absolute; right:20px;' class='close' data-dismiss='modal' aria-label='Close' onclick=\"$(\'#"+mother+"\').toggle(500);\")>X</button>";

								if(data_map.onground==1)
								{
									document.getElementById(header).style.backgroundColor = "cadetblue";
									document.getElementById(mother).style.borderColor  = "cadetblue";
									document.getElementById(header).innerHTML=index+". "+data_map.pilotname+" (au sol)"+close_bp;
								}
								else
								{
									if (data_map.squawk==7500 || data_map.squawk==7600 || data_map.squawk==7700 )
									{
										document.getElementById(header).style.backgroundColor = "red";
										document.getElementById(mother).style.borderColor  = "red";

										if(data_map.squawk==7500)
										{
											document.getElementById(header).innerHTML=index+". "+data_map.pilotname+" (intrusion)"+close_bp;
										}
										if(data_map.squawk==7600)
										{
											document.getElementById(header).innerHTML=index+". "+data_map.pilotname+" (Radio INOP)"+close_bp;
										}
										if(data_map.squawk==7700)
										{
											document.getElementById(header).innerHTML=index+". "+data_map.pilotname+" (May Day)"+close_bp;
										}
									}
									else
									{
										if (parseInt(data_map.ias)>250 && parseInt(data_map.alt)<10000)
										{
											document.getElementById(header).style.backgroundColor = "red";
											document.getElementById(mother).style.borderColor  = "red";
											document.getElementById(header).innerHTML=index+". "+data_map.pilotname+" (speed)"+close_bp;
										}
										else
										{
											document.getElementById(header).style.backgroundColor = "#84d484";
											document.getElementById(mother).style.borderColor  = "#84d484";
											document.getElementById(header).innerHTML=index+". "+data_map.pilotname+" (en vol)"+close_bp;
										}
									}
								}
							}
						}
					}
					xhr.open(Methode, Url + '?' + String, true);
					xhr.send();
				}

				
				google.maps.event.addListener(attributs.points[data_map.map_id], 'mousedown', function(event) {windows_aircraft(data_map,attributs)});
			}
		}

		function icon_Map_AI (hdg,type,spec)
		{
			if (hdg >= 0 || hdg <= 360)
			{
				var colone_image=(Math.round((hdg)/15))+12;
				if (colone_image>23){colone_image=colone_image-24;}
			};

			if (spec==0) {url_icon='./../<?=e_PLUGIN."mark_42_multi"?>/page/map/images/yellow_large.png';}
			if (spec==1) {url_icon='./../<?=e_PLUGIN."mark_42_multi"?>/page/map/images/red_large.png';}
			if (spec==2) {url_icon='./../<?=e_PLUGIN."mark_42_multi"?>/page/map/images/blue_large.png';}
			var image = {
				url: url_icon,
				// This marker is 20 pixels wide by 32 pixels high.
				size: new google.maps.Size(48, 48),
				// The origin for this image is (0, 0).
				origin: new google.maps.Point(48*colone_image, 48*type),
				// The anchor for this image is the base of the flagpole at (0, 32).
				anchor: new google.maps.Point(24, 24),
				//scaledSize: new google.maps.Size(60, 60)
			};
			return image;
		}

		document.getElementById("focus_selector").addEventListener("change", function()
		{
			focus_selected =document.getElementById("focus_selector").value;
		});

		document.getElementById("range_selector").addEventListener("change", function()
		{
			range_selected =document.getElementById("range_selector").value;
		});

		function removeLine()
		{
			Clear_route_schedules();
			Clear_route_black_box();
			clearRouteMarkers();
		};

		function error_log(jqXHR, textStatus, errorThrown)
		{ 
			console.log('jqXHR:');
			console.log(jqXHR);
			console.log('textStatus:');
			console.log(textStatus);
			console.log('errorThrown:');
			console.log(errorThrown);
		};

		function clearRouteMarkers()
		{
			if(routeMarkers!= null)
			{
				if(routeMarkers.length > 0)
				{
					for(var i = 0; i < routeMarkers.length; i++)
					{
						routeMarkers[i].setMap(null);
					}
				}
				routeMarkers.length = 0;
			}
		};

		function Clear_route_schedules()
		{
			if(flightPath != null)
			{
				flightPath.setMap(null);
				flightPath = null;
			}
			if(depMarker != null)
			{
				depMarker.setMap(null);
				depMarker = null;
			}

			if(arrMarker != null)
			{
				arrMarker.setMap(null);
				arrMarker = null;
			}
		};

		function Clear_route_black_box()
		{
			if(flightPath2 != null)
			{
				flightPath2.setMap(null);
				flightPath2 = null;
			}
		};

		function Selector_gen()
		{

			var option_text='<option style "background: black;">Centrer sur</option>';
			for(var index in aircrafts.attributs.points)
			{ 
				var attr = aircrafts.attributs.points[index];
				var marker = attr;
				var is_selected=''
				if(aircrafts.attributs.points[index].name==focus_selected)
				{
					is_selected=' selected ';
				}
				option_text=option_text+'<option value="'+aircrafts.attributs.points[index].name+'"'+is_selected+'>'+aircrafts.attributs.points[index].name+'</option>';
			}
			document.getElementById('focus_selector').innerHTML=option_text;

			AID="";



			for (var i = 0; i <=9; i++)
			{
				AID=AID+'Surveillance n°'+i+'<br><select name="AID_'+i+'" id="AID_'+i+'" class="AID" style="color: black;">';
				AID=AID+'<option value="none">choisir un pilote à afficher</option>';

				

				//console.log("test "+i);

				for(var index in aircrafts.attributs.points)
				{ 
					var is_selected='';
					//console.log("joueur"+aircrafts.attributs.points[index].name+" n:"+index);
					for (var index2 in AID_array)
					{
						
						if(AID_array[index2]==index && i==index2)
						{
							is_selected=' selected ';
							//console.log("joueur"+aircrafts.attributs.points[index].name+" est sélectionné");
						}
						//else{is_selected='';}
					}
					AID=AID+'<option value="'+index+'"'+is_selected+'>'+aircrafts.attributs.points[index].name+'</option>';
				}
				AID=AID+'</select><br><button type="button" class="btn btn-info btn-sm" style="//width: 100px;" onclick="$(\'#aircraft_map_'+i+'\').show(500);">ouvrir la fenêtre</button><hr>';


			}

			for (var i = 0; i < 10; i++)
			{

			}



			if(Last_AID!=AID || first==2)
			{
				document.getElementById('aircraft_distrib_list').innerHTML=AID;
				Last_AID=AID;
			}



			if(first<2)
			{
				first++;
			}
			else if (first==2)
			{
				$('#loading').modal('hide');
			}
		}

		function liveRefresh()
		{

			if (first>=2)
			{
				if(count_metar_top==0)
				{

					if( center.lat() != undefined && center.lng() != undefined)
					{
						var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=metar_one';
						var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
						var Methode="GET";

						var xhr = getHttpRequest();
						xhr.onreadystatechange = function ()
						{
							if (xhr.readyState === 4 && xhr.status == 200)
							{
								result=xhr.responseText;
								if(result!="" || result!="null")
								{
									if(last_metar!==result)
									{
										last_metar=result;

										document.getElementById('metar_top').innerHTML=last_metar;

										last_metar;
									}
								}
							}
						};
						xhr.open(Methode, Url + '?' + String, true);
						xhr.send();

						count_metar_top++;
					}

				}
				else
				{
					count_metar_top++;
					if(count_metar_top>=10)
					{
						count_metar_top=0;
					}
				}
			}


			if(aircraft_bp==true)
			{
				var String = 'mod=map_aircraft';
				var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
				var Methode="GET";
				aircrafts.search_info('GET',Url,String);
				$("#aircraft_bp").removeClass('btn-danger');
				$("#aircraft_bp").addClass('btn-primary');
			}
			else
			{
				aircrafts.delete_info();
				$("#aircraft_bp").removeClass('btn-primary');
				$("#aircraft_bp").addClass('btn-danger');
			}

			if(airport_bp==true)
			{
				if( center.lat() != undefined && center.lng() != undefined)
				{
					var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=map_airport';
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";


					if (airports.attributs.last_req!=String)
					{
						airports.search_info('GET',Url,String);
					}
				}

				$("#airport_bp").removeClass('btn-danger');
				$("#airport_bp").addClass('btn-primary');
			}
			else
			{
				airports.delete_info();
				airports.attributs.last_req="";
				$("#airport_bp").removeClass('btn-primary');
				$("#airport_bp").addClass('btn-danger');
			}

			if(runway_bp==true)
			{
				if( center.lat() != undefined && center.lng() != undefined)
				{
					var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=map_runway';
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";

					if (runway.attributs.last_req!=String)
					{
						runway.search_info('GET',Url,String);
					}
				}
				$("#runway_bp").removeClass('btn-danger');
				$("#runway_bp").addClass('btn-primary');
			}
			else
			{
				runway.delete_info();
				runway.attributs.last_req="";
				$("#runway_bp").addClass('btn-danger');
				$("#runway_bp").removeClass('btn-primary');

			}
			if(ils_bp==true)
			{
				if( center.lat() != undefined && center.lng() != undefined)
				{
					var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=map_ils';
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";

					if (ils.attributs.last_req!=String)
					{
						ils.search_info('GET',Url,String);
					}
				}

				$("#ils_bp").removeClass('btn-danger');
				$("#ils_bp").addClass('btn-primary');
			}
			else
			{
				ils.delete_info();
				ils.attributs.last_req="";
				$("#ils_bp").addClass('btn-danger');
				$("#ils_bp").removeClass('btn-primary');
			}

			if(vor_bp==true)
			{
				if( center.lat() != undefined && center.lng() != undefined)
				{
					var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=map_vor';
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";

					if (vor.attributs.last_req!=String)
					{
						vor.search_info('GET',Url,String);
					}
				}
				$("#vor_bp").removeClass('btn-danger');
				$("#vor_bp").addClass('btn-primary');
			}
			else
			{
				vor.delete_info();
				$("#vor_bp").addClass('btn-danger');
				$("#vor_bp").removeClass('btn-primary');
			}
			if(ndb_bp==true)
			{
				if( center.lat() != undefined && center.lng() != undefined)
				{
					var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=map_ndb';
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";

					if (ndb.attributs.last_req!=String)
					{
						ndb.search_info('GET',Url,String);
					}
				}

				$("#ndb_bp").removeClass('btn-danger');
				$("#ndb_bp").addClass('btn-primary');
			}
			else
			{
				ndb.delete_info();
				$("#ndb_bp").addClass('btn-danger');
				$("#ndb_bp").removeClass('btn-primary');
			}

			if(metar_bp==true)
			{
				if( center.lat() != undefined && center.lng() != undefined)
				{
					var String = 'center_lat='+center.lat()+'&center_lng='+center.lng()+'&range='+range_selected+'&mod=metar';
					var Url="./../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php";
					var Methode="GET";

					if (metar.attributs.last_req!=String)
					{
						metar.search_info('GET',Url,String);
					}
				}
				$("#metar_bp").removeClass('btn-danger');
				$("#metar_bp").addClass('btn-primary');
			}
			else
			{
				metar.delete_info();
				metar.attributs.last_req="";
				$("#metar_bp").addClass('btn-danger');
				$("#metar_bp").removeClass('btn-primary');
			}

			if(focus_bp==true)
			{
				$("#focus_bp").addClass('btn-warning');
				$("#focus_bp").removeClass('btn-danger');
			}
			else
			{
				$("#focus_bp").addClass('btn-danger');
				$("#focus_bp").removeClass('btn-warning');
			}

			if(compas_bp==true)
			{
				$("#compas_bp").removeClass('btn-danger');
				$("#compas_bp").addClass('btn-primary');
			}
			else
			{
				$("#compas_bp").removeClass('btn-primary');
				$("#compas_bp").addClass('btn-danger');
			}
			if(compas_bp_2==true)
			{
				$("#compas_bp_2").removeClass('btn-danger');
				$("#compas_bp_2").addClass('btn-primary');
			}
			else
			{
				$("#compas_bp_2").removeClass('btn-primary');
				$("#compas_bp_2").addClass('btn-danger');
			}

			if(count_AID==0)
			{
				Selector_gen();
				count_AID++;
			}
			else{
				count_AID++;
				if (count_AID>=5) {count_AID=0;}
			}
			

			var classname = document.getElementsByClassName("AID");
			var AID_array_update = function()
			{
				//console.log(this.value);
				//console.log(this.name);
				var name=this.name;
				AID_array[name.replace("AID_", "")]=this.value;
			};

			for (var i = 0; i < classname.length; i++) {
				classname[i].addEventListener('change', AID_array_update, false);
			}


		};

		function FFS2_map()
		{
			Options={
				center: {lat: 47.087, lng: 2.8759},
				zoom: 2.75,
				//center: {lat: 48.7134, lng: 2.3902},
				//zoom: 13 ,
				//mapTypeId: google.maps.MapTypeId.SATELLITE,

				scaleControl: true,
				autozoom: true,
				refreshTime: refreshTime_aev,
				mapTypeControl: true,
				mapTypeControlOptions: {
					style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
					position: google.maps.ControlPosition.RIGHT_TOP 
				},
				//styles: my_styles,
				zoomControl: true,
				zoomControlOptions: {
					position: google.maps.ControlPosition.TOP_RIGHT
				},

			};
			setInterval(function () { liveRefresh(); }, Options.refreshTime);

			liveRefresh();



			map = new google.maps.Map(document.getElementById('ffs_map'),Options);
			google.maps.event.addListener(map, 'idle', function() {Selector_gen();});

			function center_set()
			{
				uprlat = map.getBounds().getNorthEast().lat();
				uprlng = map.getBounds().getNorthEast().lng();
				dwnlat = map.getBounds().getSouthWest().lat();
				dwnlng = map.getBounds().getSouthWest().lng();
				zoom = map.getZoom();
				center = map.getCenter();

				document.getElementById('x1').innerHTML='X1: '+Number((uprlat).toFixed(4));
				document.getElementById('y1').innerHTML='Y1: '+Number((uprlng).toFixed(4));
				document.getElementById('x2').innerHTML='X2: '+Number((dwnlat).toFixed(4));
				document.getElementById('y2').innerHTML='Y2: '+Number((dwnlng).toFixed(4));
				document.getElementById('zoom').innerHTML='Zoom: '+zoom;
				document.getElementById('x_center').innerHTML='Center x: '+Number((center.lat()).toFixed(4));
				document.getElementById('y_center').innerHTML='Center y: '+Number((center.lng()).toFixed(4));
			};

			var geocoder = new google.maps.Geocoder();

			function geocodeAddress(geocoder, resultsMap)
			{
				var address = document.getElementById('address').value;
				geocoder.geocode({'address': address}, function(results, status) {
					if (status === google.maps.GeocoderStatus.OK) {
						resultsMap.setCenter(results[0].geometry.location);
						var marker = new google.maps.Marker({
							map: resultsMap,
							position: results[0].geometry.location
						});
					} else {
						alert('Geocode was not successful for the following reason: ' + status);
					}
				});
			};

			document.getElementById('submit').addEventListener('click', function() {
				geocodeAddress(geocoder, map);
			});

			google.maps.event.addListener(map, "center_changed", function(){center_set();});
			google.maps.event.addListener(map, "idle", function(){center_set();});
			google.maps.event.addListener(map, "center_changed", function(){count_metar_top=0;});


		}

	</script>
	<script src="https://maps.googleapis.com/maps/api/js?key=<?=api_google_map_key?>&libraries=geometry&callback=FFS2_map"></script>
	<script type="text/javascript" src="/e107_web/lib/jquery-once/jquery.once.min.js?1496563042"></script>

	<script src="./../<?=e_PLUGIN ?>mark_42_multi/page/m_map/js/maplabel.js" type="text/javascript"></script>


	<?php

	require_once(FOOTERF);
	exit;
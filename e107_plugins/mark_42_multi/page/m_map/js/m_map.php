<?php
//activation de la protection recursive pour les éléments qui seront intégrés
define ('PROTECTIONINCLUDE', TRUE);

//intégration de fichier de configuration général du site
require_once ($_SERVER["DOCUMENT_ROOT"] . '/core/config/config_gen.php');

?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="robots" content="index, nofollow,noarchive" />
	<title>Fs France Simulateur 2 RELOADED</title>

	<meta name="description" content="La Simulation autrement" />

	<!--CSS de foundation -->
	<link rel="stylesheet" href="./includes/foundation-6/css/foundation.css"/>
	<!--Icone des boutons de foundation -->
	<link rel="icon" href="./includes/foundation-6/assets/img/icons/favicon.ico" type="image/x-icon">
	<!--Correctif additionel du CSS de foundation -->
	<link rel="stylesheet" href="./includes/css/style1.css"/>
	<!--CSS pour les tableaux à trie dynamique -->
	<link rel="stylesheet" href="./includes/css/tab_sort.css"/>
	<!--CSS pour la gestion des Icone des boutons de foundation -->
	<link rel="stylesheet" href="./includes/foundation-6/icone/foundation-icons.css"/>

	<!--CSS de corrrection -->
	<link rel="stylesheet" href="./type/membre/m_map/js/include/m_map.css"/>

	<!--script pour jquery -->
	<script type="text/javascript" src="./includes/foundation-6/js/vendor/jquery.min.js"></script>

	<!--script pour la communication entre la bd et la carte -->
	<script type="text/javascript" src="./includes/js/app.js"></script>
</head>

<body>
	


	
	<div id="ffs_map" style="//float:right;"></div>
	<div id="left_map">
		<span class="button" id='aircraft_bp' onclick="if(aircraft_bp === true){aircraft_bp = false;aircrafts.delete_info();}else{aircraft_bp = true;}">Appareils</span><br>
		<span class="button alert" id='airport_bp' onclick="if(airport_bp === true){airport_bp = false;airports.delete_info();}else{airport_bp = true;}">Aéroports</span><br>
		<span class="button alert" id='runway_bp' onclick="if(runway_bp === true){runway_bp = false;runway.delete_info();}else{runway_bp = true;}">Pistes</span><br>
		<span class="button alert" id='ils_bp' onclick="if(ils_bp === true){ils_bp = false;ils.delete_info();}else{ils_bp = true;}">ILS</span><br>
		<span class="button alert" id='vor_bp' onclick="if(vor_bp === true){vor_bp = false;}else{vor_bp = true;vor.delete_info();}">VOR</span><br>
		<span class="button alert" id='ndb_bp' onclick="if(ndb_bp === true){ndb_bp = false;}else{ndb_bp = true;ndb.delete_info();}">NDB</span><br>
		<span class="button alert" id='metar_bp' onclick="if(metar_bp === true){metar_bp = false;metar.delete_info();}else{metar_bp = true;}">METAR</span><br>
		<span class="button alert" id='focus_bp' onclick="if(focus_bp === true){focus_bp = false;if(focus_selected!=''){focus_selected=''}}else{focus_bp = true;}">focus</span><br>
		<span>
			<select id="focus_selector" style="//width: 90%;//margin-left: auto;//margin-right: auto;">
			</select>
		</span><br>
	</div>
	
	<div id="info">
		<div id="leftColOverlay" style="//float: left; //width: 225px; //overflow-x: hidden;">
			<a class="close" title="Close" onclick="removeLine();$('#info').hide(1000);$('#loader_data_map').show();document.getElementById('wrapper').style.width = 'calc(100% - 0em)';"></a>
			<br>
			<div id="info_data_map"></div>
		</div>
	</div>
	<div id="wrapper">
		<div id="top_map" style="font-size: 14px;">
			<div>
				<span id="x1" class="position"></span>	
				<span id="y1" class="position"></span>
				<span id="x2" class="position"></span>
				<span id="y2" class="position"></span>
				<span id="zoom" class="position"></span>
				<span id="x_center" class="position"></span>
				<span id="y_center" class="position"></span>
				<span>
					<select id="styles_selector" style="width: inherit;">
						<option>Sélectionnez un style de carte</option>
						<option value="vfr_rouds_style">VRF Routes</option>
						<option value="dark_style">Style obscure</option>
						<option value="water_only_style">Pieds dans l'eau?</option>
						<option value="tactical_style">Frontières</option>
						<option value="black_styles">Fond noir</option>
						<option value="unsaturated_browns_style">Unsaturated browns</option>
					</select>
				</span>
				<hr>

				
				<span style="display: inline-flex;" class="position">
					<input id="address" type="textbox" value="LFPO" style="color:black">
				</span>
				<span style="display: inline-flex;" class="position">
					<input id="submit" type="button" value="Rechercher" style="color:black">
				</span>
			</div>
		</div>
	</div>
	<script>
	////variable de l'écran de carte////

	var uprlat, uprlng, dwnlat, dwnlng,zoom,x_center,y_center,center;

	document.getElementById('styles_selector').addEventListener("change", function()
	{
		var style_to_load=eval(document.getElementById('styles_selector').value);
		//if(label_control !=undefined)
		//{
			//mapLabel_ai[label_control].set('align', document.getElementById('styles_selector').value);

		//}

		map.setOptions({styles: style_to_load});
	});
	var black_styles=[{"featureType":"all","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"all","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#000000"},{"visibility":"on"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]}];
	var vfr_rouds_style=[{"featureType":"water","elementType":"all","stylers":[{"hue":"#0B1E0C"},{"saturation":2},{"lightness":-89},{"visibility":"simplified"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"hue":"#0B1E0C"},{"saturation":26},{"lightness":-91},{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"all","stylers":[{"hue":"#0B1E0C"},{"saturation":37},{"lightness":-92},{"visibility":"on"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"hue":"#0B1E0C"},{"saturation":6},{"lightness":-90},{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"all","stylers":[{"hue":"#0B1E0C"},{"saturation":26},{"lightness":-91},{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-22},{"visibility":"on"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-22},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-35},{"visibility":"on"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-50},{"visibility":"on"}]},{"featureType":"administrative","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-2},{"visibility":"off"}]},{"featureType":"poi.park","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-36},{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":50},{"visibility":"simplified"}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-36},{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"all","stylers":[{"hue":"#00FF00"},{"saturation":100},{"lightness":-41},{"visibility":"off"}]},{"featureType":"water","elementType":"all","stylers":[]}];

	var dark_style=[{featureType:"water",elementType:"geometry",stylers:[{color:"#193341"}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#2c5a71"}]},{featureType:"road",elementType:"geometry",stylers:[{color:"#29768a"},{lightness:-37}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#406d80"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#406d80"}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#3e606f"},{weight:2},{gamma:.84}]},{elementType:"labels.text.fill",stylers:[{color:"#ffffff"}]},{featureType:"administrative",elementType:"geometry",stylers:[{weight:.6},{color:"#1a3541"}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#2c5a71"}]}];

	var water_only_style=[{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"},{"color":"#202020"}]},{"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#12608d"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]}];

	var tactical_style=[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#0f9324"},{"lightness":20},{"visibility":"on"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#00700a"},{"lightness":17},{"weight":1.2},{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"color":"#93c991"},{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20},{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]

	var unsaturated_browns_style=[{"elementType":"geometry","stylers":[{"hue":"#ff4400"},{"saturation":-68},{"lightness":-4},{"gamma":0.72}]},{"featureType":"road","elementType":"labels.icon"},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"hue":"#0077ff"},{"gamma":3.1}]},{"featureType":"water","stylers":[{"hue":"#00ccff"},{"gamma":0.44},{"saturation":-33}]},{"featureType":"poi.park","stylers":[{"hue":"#44ff00"},{"saturation":-23}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"hue":"#007fff"},{"gamma":0.77},{"saturation":65},{"lightness":99}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"gamma":0.11},{"weight":5.6},{"saturation":99},{"hue":"#0091ff"},{"lightness":-86}]},{"featureType":"transit.line","elementType":"geometry","stylers":[{"lightness":-48},{"hue":"#ff5e00"},{"gamma":1.2},{"saturation":-23}]},{"featureType":"transit","elementType":"labels.text.stroke","stylers":[{"saturation":-64},{"hue":"#ff9100"},{"lightness":16},{"gamma":0.47},{"weight":2.7}]}]
	/*var metar = new element(settings_metar);
	metar.search_info('GET',"/type/membre/m_map/php/data_req.php",'uprlat=49.8663588500153&uprlng=1.1219498809814468&dwnlat=49.032508846469916&dwnlng=-0.43948200378417823&mod=metar');

	var vor = new element(settings_vor);
	vor.search_info('GET',"/type/membre/m_map/php/data_req.php",'uprlat=50.266562877423304&uprlng=2.2333714660644546&dwnlat=48.59845190426723&dwnlng=-0.8894923034667954&mod=map_vor');

	var ndb = new element(settings_ndb);
	ndb.search_info('GET',"/type/membre/m_map/php/data_req.php",'uprlat=50.266562877423304&uprlng=2.2333714660644546&dwnlat=48.59845190426723&dwnlng=-0.8894923034667954&mod=map_ndb');

	var ils = new element(settings_ils);
	ils.search_info('GET',"/type/membre/m_map/php/data_req.php",'uprlat=50.266562877423304&uprlng=2.2333714660644546&dwnlat=48.59845190426723&dwnlng=-0.8894923034667954&mod=map_ils');
	*/

	/*var runway = new element(settings_runways);
	runway.search_info('GET',"/type/membre/m_map/php/data_req.php",'uprlat=50.266562877423304&uprlng=2.2333714660644546&dwnlat=48.59845190426723&dwnlng=-0.8894923034667954&mod=map_runway');
	*/

	/*var airports = new element(settings_airports);
	airports.search_info('GET',"/type/membre/m_map/php/data_req.php",'uprlat=50.266562877423304&uprlng=2.2333714660644546&dwnlat=48.59845190426723&dwnlng=-0.8894923034667954&mod=map_airport');
	*/
	/*var aircrafts = new element(settings_aircrafts);
	aircrafts.search_info('GET',"/type/membre/m_map/php/data_req.php",'mod=map_aircraft');
	*/

	var Options;
	var settings_metar={
		name:'metar',
		labels:[],
		points:[],
		draw:[],
		update:false,
		icon:'http://maps.google.com/mapfiles/marker.png',
		point:true,
		label:true,
		drawing:false,
		info_window:true,
		label_fontSize: 13,
		label_strokeColor: '#5B3400',
		label_fontColor: '#FEFEFE',
		label_align: 'right',
		label_strokeWeight: 3,
		draw_strokeColor: null,
		draw_strokeOpacity: null,
		draw_strokeWeight: null,
	};
	var settings_vor={
		name:'vor',
		labels:[],
		points:[],
		draw:[],
		update:false,
		icon:'/type/membre/m_map/images/vor.png',
		point:true,
		label:true,
		drawing:false,
		info_window:false,
		label_fontSize: 13,
		label_strokeColor: '#5B3400',
		label_fontColor: '#FE2F2F',
		label_align: 'right',
		label_strokeWeight: 3,
		draw_strokeColor: null,
		draw_strokeOpacity: null,
		draw_strokeWeight: null,
	};
	var settings_ndb={
		name:'ndb',
		labels:[],
		points:[],
		draw:[],
		update:false,
		icon: '/type/membre/m_map/images/ndb.png',
		point:true,
		label:true,
		drawing:false,
		info_window:false,
		label_fontSize: 13,
		label_strokeColor: '#5B3400',
		label_fontColor: '#FEFEFE',
		label_align: 'right',
		label_strokeWeight: 3,
		draw_strokeColor: null,
		draw_strokeOpacity: null,
		draw_strokeWeight: null,
	};
	var settings_ils={
		name:'ils',
		labels:[],
		points:[],
		draw:[],
		update:false,
		icon: 'http://maps.google.com/mapfiles/marker.png',
		point:false,
		label:true,
		drawing:true,
		info_window:false,
		label_fontSize: 13,
		label_strokeColor: '#5B3400',
		label_fontColor: '#ED8B0B',
		label_align: 'right',
		label_strokeWeight: 3,
		draw_strokeColor: '#EDCF0B',
		draw_strokeOpacity: 0.8,
		draw_strokeWeight: 2,
	};

	var settings_runways={
		name:'runways',
		labels:[],
		points:[],
		draw:[],
		update:false,
		icon: 'http://maps.google.com/mapfiles/marker.png',
		point:false,
		label:false,
		drawing:true,
		info_window:false,
		label_fontSize: null,
		label_strokeColor: null,
		label_fontColor: null,
		label_align: null,
		label_strokeWeight:null,
		draw_strokeColor: '#96CA2D',
		draw_strokeOpacity: 1.0,
		draw_strokeWeight: 4,
	};
	var settings_airports={
		name:'airports',
		labels:[],
		points:[],
		draw:[],
		update:false,
		icon: '/type/membre/map/images/aer.png',
		point:true,
		label:true,
		drawing:false,
		info_window:true,
		label_fontSize: 13,
		label_strokeColor: '#5B3400',
		label_fontColor: '#F0FE2F',
		label_align: 'left',
		label_strokeWeight:3,
		draw_strokeColor: null,
		draw_strokeOpacity: null,
		draw_strokeWeight: null,
	};
	var settings_aircrafts={
		name:'aircrafts',
		labels:[],
		points:[],
		draw:[],
		update:true,
		icon: 'http://maps.google.com/mapfiles/marker.png',
		point:true,
		label:true,
		drawing:false,
		info_window:true,
		label_fontSize: 13,
		label_strokeColor: '#000000',
		label_fontColor: '#AEEE00',
		label_align: 'left',
		label_strokeWeight:3,
		draw_strokeColor: null,
		draw_strokeOpacity: null,
		draw_strokeWeight: null,
	};

	var refreshTime_aev=2000;
	//var AI_to_Map = [];
	//var AI_from_Map = [];
	//var AIRPORT_to_Map=[];
	//var RUNWAY_to_Map=[];
	//var RUNWAY_on_map=[];
	//var ILS_on_Map=[];
	//var VOR_on_Map=[];
	//var NDB_on_Map=[];
	//var mapLabel_ai=[];
	//var mapLabel_ils=[];
	//var mapLabel_vor=[];
	//var mapLabel_ndb=[];
	//var mapLabel_airport=[]
	//var mapLabel_metar=[];
	//var markers = [];
	//var METAR_on_Map=[]
	var run_once = false;
	var label_control;
	var first=0;

	function element(settings)
	{
		this.attributs={name:settings.name,labels:settings.labels,points:settings.points,point:settings.point,draw:settings.draw,label:settings.label,drawing:settings.drawing,update:settings.update,icon:settings.icon,info_window:settings.info_window,label_fontSize: settings.label_fontSize,label_strokeColor: settings.label_strokeColor,label_fontColor: settings.label_fontColor,label_strokeWeight: settings.label_strokeWeight,label_align: settings.label_align,draw_strokeColor: settings.draw_strokeColor,draw_strokeOpacity: settings.draw_strokeOpacity,draw_strokeWeight: settings.draw_strokeWeight,};
		
		this.search_info=function (Methode,Url,String,attributs=this.attributs)
		{
			var xhr = getHttpRequest();
			xhr.onreadystatechange = function ()
			{
				if (xhr.readyState === 4 && xhr.status == 200)
				{
					result=xhr.responseText;
					if(result!="" || result!="null")
					{
						distrib_data(JSON.parse(result),attributs);
					}
				}
			};
			xhr.open(Methode, Url + '?' + String, true);
			xhr.send();
		};

		this.delete_info=function (attributs=this.attributs)
		{
			if(attributs.draw!= null && attributs.drawing===true)
			{
				if(attributs.draw.length > 0)
				{
					for(var index in attributs.draw)
					{ 
						attributs.draw[index].setMap(null);
						delete attributs.draw[index];
					}
				}
				attributs.draw.length = 0;
			}
			if(attributs.labels!= null && attributs.label===true)
			{
				if(attributs.labels.length > 0)
				{
					for(var index in attributs.labels)
					{ 
						attributs.labels[index].setMap(null);
						delete attributs.labels[index];
					}
				}
				attributs.labels.length = 0;
			}
			if(attributs.points!= null && attributs.point===true)
			{
				if(attributs.points.length > 0)
				{
					for(var index in attributs.points)
					{ 
						attributs.points[index].setMap(null);
						delete attributs.points[index];
					}
				}
				attributs.points.length = 0;
			}
		};

		function alert_data(data,op)
		{
			alertObject(data);
		}

		function distrib_data(data_map,attributs)
		{
			if (data_map!= null)
			{
				for(var index in attributs.points)
				{ 
					var present=false;
					for (var i = 0; i < data_map.length; i++) 
					{
						if(index==data_map[i].ID)
						{
							present=true;
							break;	
						}
					}

					if(present==false)
					{
						attributs.points[index].setMap(null);
						if(attributs.label)
						{
							attributs.labels[index].setMap(null);
							delete attributs.labels[index];
						}
						delete attributs.points[index];
					}
				}
				for (var i = 0; i < data_map.length; i++) 
				{
					if (data_map[i]!= null)
					{
						if(attributs.point)
						{

							if(attributs.points[data_map[i].ID] == undefined)
							{
								points_create(data_map[i],attributs);
							}
							else if (attributs.update)
							{
								if(attributs.name==="aircrafts")
								{
									if((data_map[i].pilot_id ==='0'))
										{color_file=1;}
									else{color_file=0;}
									var pos = new google.maps.LatLng(data_map[i].lat, data_map[i].lng);

									color_file = (data_map[i].pilot_id ==='0') ? 1 : 0;
									attributs.icon=icon_Map_AI(data_map[i].heading,10,color_file);

									if(focus_bp==true && focus_selected==data_map[i].pilotname)
									{
										map.setCenter(pos);
									}
								}

								attributs.points[data_map[i].ID].setPosition(pos);
								attributs.points[data_map[i].ID].setIcon(icon_Map_AI(data_map[i].heading,10,color_file));



							}

							if(attributs.info_window)
							{
								windows_create(data_map[i],attributs);
							}
						}
						if(attributs.label)
						{
							if(attributs.labels[data_map[i].ID]== undefined)
							{
								labels_create(data_map[i],attributs);
							}
							else if (attributs.update)
							{
								if(attributs.name==="aircrafts")
								{
									attributs.labels[data_map[i].ID].set('position', new google.maps.LatLng(data_map[i].lat, data_map[i].lng));
									attributs.labels[data_map[i].ID].set('text', data_map[i].pilotname+' '+data_map[i].heading+'°   '+data_map[i].alt+' Ft   '+data_map[i].gs+' Kts');
								}

							}
						}
						if(attributs.drawing)
						{
							if(attributs.draw[data_map[i].ID]== undefined)
							{
								draw_create(data_map[i],attributs);
							}
						}
					}
				}
			}
		};

		function points_create(data_map,attributs)
		{
			var point_name='none'
			if(attributs.name==="aircrafts")
			{
				var pos = new google.maps.LatLng(data_map.lat, data_map.lng);

				color_file = (data_map.pilot_id ==='0') ? 1 : 0;
				attributs.icon=icon_Map_AI(data_map.heading,10,color_file);
				point_name=data_map.pilotname;








	/*			function update_Map_AI(data_map)
	{
		//var bounds = new google.maps.LatLngBounds();
		var pos = new google.maps.LatLng(data_map.lat, data_map.lng);
		lat = data_map.lat; lng = data_map.lng;

		if((data_map.pilot_id ==='0'))
			{color_file=1;}
		else{color_file=0;}

		AI_to_Map[data_map.id].setPosition(pos);
		AI_to_Map[data_map.id].setIcon(icon_Map_AI(data_map.heading,10,color_file));

		mapLabel_ai[data_map.id].set('position', new google.maps.LatLng(lat, lng));
		mapLabel_ai[data_map.id].set('text', data_map.pilotname+' '+data_map.heading+'°   '+data_map.alt+' Ft   '+data_map.gs+' Kts');

		if(focus_bp==true && focus_selected==data_map.ID)
		{
			//map.setCenter(pos);
		}
	}*/
}
else{var pos = new google.maps.LatLng(data_map.Latitude, data_map.Longitude);}

var title;

attributs.points[data_map.ID] = new google.maps.Marker({
	position: pos,
	map: map,
	icon: attributs.icon,
	optimized: false,
	flightdetails: data_map,
	name:point_name,

});
}

function draw_create(data_map,attributs)
{
	if(attributs.name==="ils")
	{
		var p1 = new google.maps.LatLng(data_map.Latitude, data_map.Longitude);
		var distance_1=-18000;
		var distance_2=-17000;
		var angle_1=parseInt(data_map.LocCourse)+2;
		var angle_2=parseInt(data_map.LocCourse)-2;

		var p2 = new google.maps.geometry.spherical.computeOffset(p1, distance_1, angle_1);
		var p3 = new google.maps.geometry.spherical.computeOffset(p1, distance_2, data_map.LocCourse);
		var p4 = new google.maps.geometry.spherical.computeOffset(p1, distance_1, angle_2);

		var drawn_Coords = [p1, p2,p3,p4];
	}
	if(attributs.name==="runways")
	{
		var startLL = new google.maps.LatLng(data_map.Latitude,data_map.Longitude);
		var distance=0.3048*data_map.Length;
		var endLL = new google.maps.geometry.spherical.computeOffset(startLL, distance, data_map.TrueHeading);

		var drawn_Coords = [startLL, endLL];
	}

	attributs.draw[data_map.ID] = new google.maps.Polygon({
		paths: drawn_Coords,
		strokeColor: attributs.draw_strokeColor,
		strokeOpacity: attributs.draw_strokeOpacity,
		strokeWeight: attributs.draw_strokeWeight,
	});
	attributs.draw[data_map.ID].setMap(map);
}

function labels_create(data_map,attributs)
{
	var pos=p1 = new google.maps.LatLng(data_map.Latitude, data_map.Longitude);
	if(attributs.name==="metar")
	{
		var text= data_map.station_id;
	}
	if (attributs.name==="vor")
	{
		var text= data_map.Ident+' '+convertFromBaseToBase(data_map.Freq, 10, 16)+'   '+data_map.Elevation+' Ft';
	}
	if (attributs.name==="ndb")
	{
		var text= data_map.Ident+' '+convertFromBaseToBase(data_map.Freq, 10, 16);
	}
	if(attributs.name==="ils")
	{
		var text=data_map.Ident+'    '+data_map.LocCourse+'°    '+convertFromBaseToBase(data_map.Freq, 10, 16)+'    Glide'+data_map.GsAngle+'°';
		attributs.label_align = (data_map.LocCourse<180) ? 'right' : 'left';

		var distance_1=-18000;
		var angle_2=parseInt(data_map.LocCourse)-2;
		pos=new google.maps.geometry.spherical.computeOffset(p1, distance_1, angle_2);

	}
	if(attributs.name==="aircrafts")
	{
		var text=data_map.pilotname+' '+data_map.heading+'°   '+data_map.alt+' Ft   '+data_map.gs+' Kts';
		pos = new google.maps.LatLng(data_map.lat, data_map.lng);
	}
	if(attributs.name==="airports")
	{
		var text=data_map.ICAO+'    '+data_map.Name;
	}

	attributs.labels[data_map.ID] = new MapLabel({
		text: text,
		position: pos,
		map: map,
		fontSize: attributs.label_fontSize,
		strokeColor: attributs.label_strokeColor,
		fontColor: attributs.label_fontColor,
		align: attributs.label_align,
		strokeWeight:attributs.label_strokeWeight,
	});
}

function windows_create(data_map,attributs)
{
	if(attributs.name==='metar')
	{
		google.maps.event.addListener(attributs.points[data_map.ID], 'click', function() 
		{
			var info_metar='<div style="color: #84d484;text-shadow: -1px 0 #025802, 0 1px #025802, 1px 0 #025802, 0 -1px #025802;"><span style="font-size: 1.5em;font-weight: 800;">Station de '+data_map.station_id+'</span><br><br><B>Mise à jourle</B>: '+data_map.observation_time+'<br><br><B>Tempéraure</B>: '+data_map.temp_c+'°c<br><B>Point de rosée</B>: '+data_map.dewpoint_c+'°c<br><B>Direction du vent</B>: '+data_map.wind_dir_degrees+'°<br><B>Vitesse du vent</B>: '+data_map.wind_speed_kt+' Noeuds<br><B>Visibilité</B>: '+data_map.visibility_statute_mi+' Miles Nautique<br><B>Altimètre</B>: '+data_map.altim_in_hg+' inches Hg';

			var sky_condition=JSON.parse(data_map.sky_condition);

			var level='';
			if (sky_condition!= null)
			{
				for (var i = 0; i < sky_condition.length; i++) 
				{
					if(sky_condition[i].sky_cover=='OVC'){level=level+'<br>COUVERT à '+sky_condition[i].cloud_base_ft_agl+' pieds';}
					if(sky_condition[i].sky_cover=='SCT'){level=level+'<br>EPART à '+sky_condition[i].cloud_base_ft_agl+' pieds';}
					if(sky_condition[i].sky_cover=='BKN'){level=level+'<br>FRAGMENTE à '+sky_condition[i].cloud_base_ft_agl+' pieds';}
					if(sky_condition[i].sky_cover=='FEW'){level=level+'<br>PEU NOMBREUX à '+sky_condition[i].cloud_base_ft_agl+' pieds';}
					if(sky_condition[i].sky_cover=='NSC'){level=level+'<br>AUCUN NUAGE SOUS 5000 pieds';}
					if(sky_condition[i].sky_cover=='NCD'){level=level+'<br>PAS DE NUAGE DETECTE';}
					if(sky_condition[i].sky_cover=='SKC'){level=level+'<br>CIEL CLAIR';}
				}
			}
			info_metar=info_metar+'<br><br><U><B>Planchés nuageux</U></B>'+level;

			$('#info').show(1000);
			document.getElementById("wrapper").style.width = 'calc(100% - 23em)';
			document.getElementById('info_data_map').innerHTML=info_metar+"</div>";
		});
	}
	if(attributs.name==='aircrafts')
	{
		var info_text='Aucune info disponible.';

		google.maps.event.addListener(attributs.points[data_map.ID], 'click', function() 
		{
			console.log('click sur '+data_map.ID);

			var String = 'mod=data_flight&pilot_id='+data_map.pilot_id;
			var Url="/type/membre/m_map/php/data_req.php";
			var Methode="GET";

			var xhr = getHttpRequest();
			xhr.onreadystatechange = function ()
			{
				if (xhr.readyState === 4 && xhr.status == 200)
				{
					result=xhr.responseText;
					if(result!="" || result!="null")
					{
						document.getElementById("wrapper").style.width = 'calc(100% - 23em)';
						document.getElementById('info_data_map').innerHTML=result
						$('#info').show(1000);
					}
				}
			};
			xhr.open(Methode, Url + '?' + String, true);
			xhr.send();

		});
	}

	if(attributs.name==='airports')
	{
		var info_text='Aucune info disponible.';

		google.maps.event.addListener(attributs.points[data_map.ID], 'click', function() 
		{
			var String = 'ID='+data_map.ID+'&mod=info_aiport';
			var Url="/type/membre/m_map/php/data_req.php";
			var Methode="GET";

			var xhr = getHttpRequest();
			xhr.onreadystatechange = function ()
			{
				if (xhr.readyState === 4 && xhr.status == 200)
				{
					result=xhr.responseText;
					if(result!="" || result!="null")
					{
						data = JSON.parse(result);
						info_text="<div style='color: #84d484;text-shadow: -1px 0 #025802, 0 1px #025802, 1px 0 #025802, 0 -1px #025802;'><span style='font-size: 1.5em;font-weight: 800;'>"+data_map.ICAO+' '+data_map.Name+':</span><br><br>';
						for (var i = 0; i < data.length; i++) 
						{
							info_text=info_text+"<U>Piste "+Math.round(Math.round(data[i].TrueHeading)/10)+":</U><br><B>Axe de piste</B>: "+Math.round(data[i].TrueHeading)+"<br><B>Surface: </B>"+data[i].Surface+"<br><B>Longueur: </B>"+data[i].Length+"<br><B>Largeur: </B>"+data[i].Width+"<br><B>Elevation:</B>"+data[i].Elevation+"<br><br>";
						}

						$('#info').show(1000);
						document.getElementById("wrapper").style.width = 'calc(100% - 23em)';
						document.getElementById('info_data_map').innerHTML=info_text+"</div>";
					}
				}
			};
			xhr.open(Methode, Url + '?' + String, true);
			xhr.send();
		});
	}


}
}
/*nouveaux systéme en cours de bascule*/
var metar = new element(settings_metar);
var vor = new element(settings_vor);
var ndb = new element(settings_ndb);
var ils = new element(settings_ils);
var runway = new element(settings_runways);
var airports = new element(settings_airports);
var aircrafts = new element(settings_aircrafts);
/*nouveaux systéme en cours de bascule*/

var airport_bp=runway_bp=vor_bp=ils_bp=ndb_bp=false;
var aircraft_bp = true;
var focus_bp = false;
var focus_select = false;
var focus_selected='';
var option_focus='<option selected>focus sur</option>';

var	routeMarkers,flightPath,flightPath2,depMarker,arrMarker,map;

var color_file=0;

function alertObject(obj){console.log(obj);}

function convertFromBaseToBase(str, fromBase, toBase){

	var num = parseInt(str, fromBase);
	if(str!=""){
		var result=num.toString(toBase);

		if(result.toString()=="NaN"){
			result="Invalid Input";
		}
		return result/10000;
	}
	else
	{
		return;	
	}
}




	/*function distrib_Map_AI (data)
	{
		option_focus='<option selected>focus sur</option>';
		if (data!= null)
		{
			for(var index in AI_to_Map)
			{ 
				var present=false;
				for (var i = 0; i < data.length; i++) 
				{
					if(index==data[i].id)
					{
						present=true;
						break;	
					}

				}

				if(present==false)
				{
					AI_to_Map[index].setMap(null);
					mapLabel_ai[index].setMap(null);
					delete mapLabel_ai[index];
					delete AI_to_Map[index];
				}
			}
			for (var i = 0; i < data.length; i++) 
			{
				if (data[i]!= null)
				{
					if(AI_to_Map[data[i].id]!= undefined){update_Map_AI(data[i]);}
					else{create_Map_AI(data[i]);}
				}
				option_focus=option_focus+'<option value="'+data[i].id+'">'+data[i].pilotname+'</option>';
			}

			document.getElementById('focus_selector').innerHTML=option_focus;
		}
	};*/
/*
	
*/
	/*function create_Map_AI(data_map)
	{
		var bounds = new google.maps.LatLngBounds();
		var pos = new google.maps.LatLng(data_map.lat, data_map.lng);
		lat = data_map.lat; lng = data_map.lng;

		if((data_map.pilot_id ==='0'))
			{color_file=1;}
		else{color_file=0;}

		AI_to_Map[data_map.id] = new google.maps.Marker({
			position: pos,
			map: map,
			icon: icon_Map_AI(data_map.heading,10,color_file),
			optimized: false,
			flightdetails: data_map,
			title: data_map.pilotname+'->'+data_map.aircraft+' : '+data_map.flightnum,
			name: data_map.pilotname,
		});
		bounds.extend(pos);

		mapLabel_ai[data_map.id] = new MapLabel({
			text: data_map.pilotname+' '+data_map.heading+'°   '+data_map.alt+' Ft   '+data_map.gs+' Kts',
			position: new google.maps.LatLng(lat, lng),
			map: map,
			fontSize: 15,
			align: 'right'
		});

		google.maps.event.addListener(AI_to_Map[data_map.id], 'click', function() 
		{
			var infowindow = new google.maps.InfoWindow({
				content: data_map.lastupdate,
				maxWidth: 200
			});

			infowindow.open(map, AI_to_Map[data_map.id]);
		});

		if(label_control==data_map.id)
		{
			//map.setCenter(pos);
		}
	};*/
/*
	
*/



	/*function update_Map_AI(data_map)
	{
		//var bounds = new google.maps.LatLngBounds();
		var pos = new google.maps.LatLng(data_map.lat, data_map.lng);
		lat = data_map.lat; lng = data_map.lng;

		if((data_map.pilot_id ==='0'))
			{color_file=1;}
		else{color_file=0;}

		AI_to_Map[data_map.id].setPosition(pos);
		AI_to_Map[data_map.id].setIcon(icon_Map_AI(data_map.heading,10,color_file));

		mapLabel_ai[data_map.id].set('position', new google.maps.LatLng(lat, lng));
		mapLabel_ai[data_map.id].set('text', data_map.pilotname+' '+data_map.heading+'°   '+data_map.alt+' Ft   '+data_map.gs+' Kts');

		if(focus_bp==true && focus_selected==data_map.ID)
		{
			//map.setCenter(pos);
		}
	};*/
/*
*/



function icon_Map_AI (hdg,type,spec)
{
	if (hdg >= 0 || hdg <= 360)
	{
		var colone_image=(Math.round((hdg)/15))+12;
		if (colone_image>23){colone_image=colone_image-24;}
	};

	if (spec == 0) {url_icon='/type/membre/map/images/yellow_large.png';}
	else {url_icon='/type/membre/map/images/red_large.png';}

	var image = {
		url: url_icon,
			// This marker is 20 pixels wide by 32 pixels high.
			size: new google.maps.Size(48, 48),
			// The origin for this image is (0, 0).
			origin: new google.maps.Point(48*colone_image, 48*type),
			// The anchor for this image is the base of the flagpole at (0, 32).
			anchor: new google.maps.Point(24, 24),
			//scaledSize: new google.maps.Size(60, 60)
		};
		return image;
	}


	document.getElementById("focus_selector").addEventListener("change", function()
	{
		focus_selected =document.getElementById("focus_selector").value;
	});




	function removeLine()
	{
		Clear_route_schedules();
		Clear_route_black_box();
		clearRouteMarkers();
	};

	function error_log(jqXHR, textStatus, errorThrown)
	{ 
		console.log('jqXHR:');
		console.log(jqXHR);
		console.log('textStatus:');
		console.log(textStatus);
		console.log('errorThrown:');
		console.log(errorThrown);
	};

	function clearRouteMarkers()
	{
		if(routeMarkers!= null)
		{
			if(routeMarkers.length > 0)
			{
				for(var i = 0; i < routeMarkers.length; i++)
				{
					routeMarkers[i].setMap(null);
				}
			}
			routeMarkers.length = 0;
		}
	};

	function Clear_route_schedules()
	{
		if(flightPath != null)
		{
			flightPath.setMap(null);
			flightPath = null;
		}
		if(depMarker != null)
		{
			depMarker.setMap(null);
			depMarker = null;
		}

		if(arrMarker != null)
		{
			arrMarker.setMap(null);
			arrMarker = null;
		}
	};

	function Clear_route_black_box()
	{
		if(flightPath2 != null)
		{
			flightPath2.setMap(null);
			flightPath2 = null;
		}
	};

	function FFS2_map()
	{
		Options={
			center: {lat: 47.087, lng: 2.8759},
			zoom: 7,
			//center: {lat: 48.7134, lng: 2.3902},
			//zoom: 13 ,
			//mapTypeId: google.maps.MapTypeId.SATELLITE,
			
			scaleControl: true,
			autozoom: true,
			refreshTime: refreshTime_aev,
			mapTypeControl: true,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.RIGHT_TOP 
			},
			//styles: my_styles,
			zoomControl: true,
			zoomControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			},

		};
		setInterval(function () { liveRefresh(); }, Options.refreshTime);

		liveRefresh();

		map = new google.maps.Map(document.getElementById('ffs_map'),Options);

		google.maps.event.addListener(map, 'idle', function() {
			showVisibleMarkers();
		});



		function showVisibleMarkers() {

			var bounds = map.getBounds(),
			count = 0;
			var option_text='<option>Centrer sur</option>';

			for(var index in aircrafts.attributs.points)
			{ 
				var attr = aircrafts.attributs.points[index];
				//alertObject(aircrafts.attributs.points[index].name);
				var marker = attr;
				var is_selected=''
				if(aircrafts.attributs.points[index].name==focus_selected)
				{
					is_selected=' selected ';
				}
				option_text=option_text+'<option value="'+aircrafts.attributs.points[index].name+'"'+is_selected+'>'+aircrafts.attributs.points[index].name+'</option>';
			}
			document.getElementById('focus_selector').innerHTML=option_text;
		}

		function liveRefresh()
		{
			if(aircraft_bp==true)
			{
				var String = 'mod=map_aircraft';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				aircrafts.search_info('GET',Url,String);
				$("#aircraft_bp").removeClass('alert');
			}
			else
			{
				aircrafts.delete_info();
				$("#aircraft_bp").addClass('alert');
			}
			if(zoom>=8 && airport_bp==true)
			{
				var String = 'uprlat='+uprlat+'&uprlng='+uprlng+'&dwnlat='+dwnlat+'&dwnlng='+dwnlng+'&mod=map_airport';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				airports.search_info('GET',Url,String);
				$("#airport_bp").removeClass('alert');
			}
			else
			{
				airports.delete_info();
				$("#airport_bp").addClass('alert');
			}

			if(zoom>=12 && runway_bp==true)
			{
				var String = 'uprlat='+uprlat+'&uprlng='+uprlng+'&dwnlat='+dwnlat+'&dwnlng='+dwnlng+'&mod=map_runway';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				runway.search_info('GET',Url,String);
				$("#runway_bp").removeClass('alert');
			}
			else
			{
				runway.delete_info();
				$("#runway_bp").addClass('alert');
			}
			if(zoom>=8 && ils_bp==true)
			{
				var String = 'uprlat='+uprlat+'&uprlng='+uprlng+'&dwnlat='+dwnlat+'&dwnlng='+dwnlng+'&mod=map_ils';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				ils.search_info('GET',Url,String);
				$("#ils_bp").removeClass('alert');
			}
			else
			{
				ils.delete_info();
				$("#ils_bp").addClass('alert');
			}

			if(zoom>=7 && vor_bp==true)
			{
				var String = 'uprlat='+uprlat+'&uprlng='+uprlng+'&dwnlat='+dwnlat+'&dwnlng='+dwnlng+'&mod=map_vor';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				vor.search_info('GET',Url,String);
				$("#vor_bp").removeClass('alert');
			}
			else
			{
				vor.delete_info();
				$("#vor_bp").addClass('alert');
			}
			if(zoom>=7 && ndb_bp==true)
			{
				var String = 'uprlat='+uprlat+'&uprlng='+uprlng+'&dwnlat='+dwnlat+'&dwnlng='+dwnlng+'&mod=map_ndb';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				ndb.search_info('GET',Url,String);
				$("#ndb_bp").removeClass('alert');
			}
			else
			{
				ndb.delete_info();
				$("#ndb_bp").addClass('alert');
			}

			if(zoom>=7 && metar_bp==true)
			{
				var String = 'uprlat='+uprlat+'&uprlng='+uprlng+'&dwnlat='+dwnlat+'&dwnlng='+dwnlng+'&mod=metar';
				var Url="/type/membre/m_map/php/data_req.php";
				var Methode="GET";
				metar.search_info('GET',Url,String);
				$("#metar_bp").removeClass('alert');
			}
			else
			{
				metar.delete_info();
				$("#metar_bp").addClass('alert');
			}

			if(focus_bp==true)
			{
				$("#focus_bp").removeClass('alert');
			}
			else
			{
				$("#focus_bp").addClass('alert');
			}

		};

		function center_set()
		{
			uprlat = map.getBounds().getNorthEast().lat();
			uprlng = map.getBounds().getNorthEast().lng();
			dwnlat = map.getBounds().getSouthWest().lat();
			dwnlng = map.getBounds().getSouthWest().lng();
			zoom = map.getZoom();
			center = map.getCenter();

			document.getElementById('x1').innerHTML='X1: '+Number((uprlat).toFixed(4));
			document.getElementById('y1').innerHTML='Y1: '+Number((uprlng).toFixed(4));
			document.getElementById('x2').innerHTML='X2: '+Number((dwnlat).toFixed(4));
			document.getElementById('y2').innerHTML='Y2: '+Number((dwnlng).toFixed(4));
			document.getElementById('zoom').innerHTML='Zoom: '+zoom;
			document.getElementById('x_center').innerHTML='Center x: '+Number((center.lat()).toFixed(4));
			document.getElementById('y_center').innerHTML='Center y: '+Number((center.lng()).toFixed(4));
		};

		var geocoder = new google.maps.Geocoder();

		function geocodeAddress(geocoder, resultsMap)
		{
			var address = document.getElementById('address').value;
			geocoder.geocode({'address': address}, function(results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					resultsMap.setCenter(results[0].geometry.location);
					var marker = new google.maps.Marker({
						map: resultsMap,
						position: results[0].geometry.location
					});
				} else {
					alert('Geocode was not successful for the following reason: ' + status);
				}
			});
		};

		document.getElementById('submit').addEventListener('click', function() {
			geocodeAddress(geocoder, map);
		});

		google.maps.event.addListener(map, "center_changed", function(){center_set();});
		google.maps.event.addListener(map, "idle", function(){center_set();});
	}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?=api_google_map_key?>&libraries=geometry&callback=FFS2_map"></script>
<script src="./type/membre/m_map/js/maplabel.js" type="text/javascript">

</script>

<!--script pour les tableaux avec trie dynamique -->
<script type="text/javascript" src="./includes/js/jquery.dataTables.min.js"></script>
<!--script pour les tableaux avec trie dynamique -->
<script type="text/javascript" src="./includes/js/dataTables.foundation.min.js"></script>
<!--script d'interaction de founadtion -->
<script type="text/javascript" src="./includes/foundation-6/js/vendor/what-input.min.js"></script>
<!--script de fonctionnement général de founadtion -->
<script type="text/javascript" src="./includes/foundation-6/js/foundation.min.js"></script>
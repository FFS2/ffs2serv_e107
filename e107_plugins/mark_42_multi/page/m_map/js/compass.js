var compass_image="https://upload.wikimedia.org/wikipedia/commons/1/1d/Kompa%C3%9F_360_%28de%29.svg";
var Compas;

var compass_image="http://360.g8dhe.net/HAB_Flights/Google_Earth_Tools/Compass_Tool.png";
function compas_make()
{
	var pos_compass = new google.maps.LatLng(center.lat(), center.lng());

	Compas = new google.maps.Marker({
		position: pos_compass,
		map: map,
		icon:  {
			url: "http://360.g8dhe.net/HAB_Flights/Google_Earth_Tools/Compass_Tool.png",
			// This marker is 20 pixels wide by 32 pixels high.
			size: new google.maps.Size(500, 500),
			// The anchor for this image is the base of the flagpole at (250, 250).
			anchor: new google.maps.Point(250, 250),
			//scaledSize: new google.maps.Size(60, 60)
		},
		optimized: false,
	});
}

function free_compas_make()
{
	var pos_compass = new google.maps.LatLng(center.lat(), center.lng());

	Compas = new google.maps.Marker({
		position: pos_compass,
		map: map,
		icon:  {
			url: "http://360.g8dhe.net/HAB_Flights/Google_Earth_Tools/Compass_Tool.png",
			// This marker is 20 pixels wide by 32 pixels high.
			size: new google.maps.Size(500, 500),
			// The anchor for this image is the base of the flagpole at (250, 250).
			anchor: new google.maps.Point(250, 250),
			//scaledSize: new google.maps.Size(60, 60)
		},
		draggable:true,
		optimized: false,
	});
}

function compas_destroy()
{
	Compas.setMap(null);
}
var settings_metar={
	name:'metar',
	labels:[],
	points:[],
	draw:[],
	update:false,
	last_req:'',
	icon: './../e107_plugin/mark_42_multi/page/map/images/metar.png',
	point:true,
	label:true,
	drawing:false,
	info_window:true,
	label_fontSize: 13,
	label_strokeColor: '#5B3400',
	label_fontColor: '#FEFEFE',
	label_align: 'right',
	label_strokeWeight: 3,
	draw_strokeColor: null,
	draw_strokeOpacity: null,
	draw_strokeWeight: null,
};
var settings_vor={
	name:'vor',
	labels:[],
	points:[],
	draw:[],
	update:false,
	last_req:'',
	icon:'./../e107_plugin/mark_42_multi/page/map/images/vor.png',
	point:true,
	label:true,
	drawing:false,
	info_window:false,
	label_fontSize: 13,
	label_strokeColor: '#5B3400',
	label_fontColor: '#FE2F2F',
	label_align: 'right',
	label_strokeWeight: 3,
	draw_strokeColor: null,
	draw_strokeOpacity: null,
	draw_strokeWeight: null,
};
var settings_ndb={
	name:'ndb',
	labels:[],
	points:[],
	draw:[],
	update:false,
	last_req:'',
	icon: './../e107_plugin/mark_42_multi/page/map/images/ndb.png',
	point:true,
	label:true,
	drawing:false,
	info_window:false,
	label_fontSize: 13,
	label_strokeColor: '#5B3400',
	label_fontColor: '#FEFEFE',
	label_align: 'right',
	label_strokeWeight: 3,
	draw_strokeColor: null,
	draw_strokeOpacity: null,
	draw_strokeWeight: null,
};
var settings_ils={
	name:'ils',
	labels:[],
	points:[],
	draw:[],
	update:false,
	last_req:'',
	icon: 'http://maps.google.com/mapfiles/marker.png',
	point:false,
	label:true,
	drawing:true,
	info_window:false,
	label_fontSize: 13,
	label_strokeColor: '#5B3400',
	label_fontColor: '#ED8B0B',
	label_align: 'right',
	label_strokeWeight: 3,
	draw_strokeColor: '#EDCF0B',
	draw_strokeOpacity: 0.8,
	draw_strokeWeight: 2,
};
var settings_runways={
	name:'runways',
	labels:[],
	points:[],
	draw:[],
	update:false,
	last_req:'',
	icon: 'http://maps.google.com/mapfiles/marker.png',
	point:false,
	label:false,
	drawing:true,
	info_window:false,
	label_fontSize: null,
	label_strokeColor: null,
	label_fontColor: null,
	label_align: null,
	label_strokeWeight:null,
	draw_strokeColor: '#96CA2D',
	draw_strokeOpacity: 1.0,
	draw_strokeWeight: 4,
};
var settings_airports={
	name:'airports',
	labels:[],
	points:[],
	draw:[],
	update:false,
	last_req:'',
	icon: './../e107_plugin/mark_42_multi/page/map/images/aer.png',
	point:true,
	label:true,
	drawing:false,
	info_window:true,
	label_fontSize: 13,
	label_strokeColor: '#5B3400',
	label_fontColor: '#F0FE2F',
	label_align: 'left',
	label_strokeWeight:3,
	draw_strokeColor: null,
	draw_strokeOpacity: null,
	draw_strokeWeight: null,
};
var settings_aircrafts={
	name:'aircrafts',
	labels:[],
	points:[],
	draw:[],
	update:true,
	last_req:'',
	icon: 'http://maps.google.com/mapfiles/marker.png',
	point:true,
	label:true,
	drawing:false,
	info_window:true,
	label_fontSize: 13,
	label_strokeColor: '#000000',
	label_fontColor: '#AEEE00',
	label_align: 'left',
	label_strokeWeight:3,
	draw_strokeColor: null,
	draw_strokeOpacity: null,
	draw_strokeWeight: null,
};
/*
var metar = new element(settings_metar);
var vor = new element(settings_vor);
var ndb = new element(settings_ndb);
var ils = new element(settings_ils);
var runway = new element(settings_runways);
var airports = new element(settings_airports);
var aircrafts = new element(settings_aircrafts);
*/
/*

function element(settings)
{
	this.attributs={name:settings.name,labels:settings.labels,points:settings.points,point:settings.point,draw:settings.draw,label:settings.label,drawing:settings.drawing,update:settings.update,icon:settings.icon,info_window:settings.info_window,label_fontSize: settings.label_fontSize,label_strokeColor: settings.label_strokeColor,label_fontColor: settings.label_fontColor,label_strokeWeight: settings.label_strokeWeight,label_align: settings.label_align,draw_strokeColor: settings.draw_strokeColor,draw_strokeOpacity: settings.draw_strokeOpacity,draw_strokeWeight: settings.draw_strokeWeight,};

	this.search_info=function (Methode,Url,String,attributs=this.attributs)
	{
		var xhr = getHttpRequest();
		xhr.onreadystatechange = function ()
		{
			if (xhr.readyState === 4 && xhr.status == 200)
			{
				result=xhr.responseText;
				if(result!="" || result!="null")
				{
					//alertObject(result);
					//alert(JSON.parse(result));
					//alertObject(JSON.parse(result));
					attributs.last_req=String;
					distrib_data(JSON.parse(result),attributs);
				}
			}
		};
		xhr.open(Methode, Url + '?' + String, true);
		xhr.send();
	};

	this.delete_info=function (attributs=this.attributs)
	{
		if(attributs.draw!= null && attributs.drawing===true)
		{
			if(attributs.draw.length >= 0)
			{
				for(var index in attributs.draw)
				{ 
					attributs.draw[index].setMap(null);
					delete attributs.draw[index];
				}
			}
			attributs.draw.length = 0;
		}
		if(attributs.labels!= null && attributs.label===true)
		{
			if(attributs.labels.length >= 0)
			{
				for(var index in attributs.labels)
				{ 
					attributs.labels[index].setMap(null);
					delete attributs.labels[index];
				}
			}
			attributs.labels.length = 0;
		}

		if(attributs.points!= null && attributs.point===true)
		{
			if(attributs.points.length >= 0)
			{
				for(var index in attributs.points)
				{ 
					attributs.points[index].setMap(null);
					delete attributs.points[index];
				}
			}
			attributs.points.length = 0;
		}
	};
};

*/
function populate_Map_Aircraft_Free(data_map)
		{

			var bounds = new google.maps.LatLngBounds();

			lat = data_map.lat; lng = data_map.lng;

			var pos = new google.maps.LatLng(lat, lng);

			if((data_map.pilot_id ==='0'))
				{color_file=1;}
			else{color_file=0;}

			avion_en_vol_free[avion_en_vol_free.length] = new google.maps.Marker({
				position: pos,
				map: map,
				icon: icon(data_map.heading,10,color_file),
				optimized: false,
				flightdetails: data_map,
				title: data_map.pilotname+'->'+data_map.aircraft+' : '+data_map.flightnum,
				name: data_map.pilotname,
			});
			if(mapLabel[data_map.pilotname]!= undefined)
			{
				mapLabel[data_map.pilotname].set('position', new google.maps.LatLng(lat, lng));
				//alert('test');
			}
			else
			{
				mapLabel[data_map.pilotname] = new MapLabel({
					text: data_map.pilotname+'\t'+data_map.hdg,
					position: new google.maps.LatLng(lat, lng),
					map: map,
					fontSize: 15,
					align: 'right'
				});
			}

			


			google.maps.event.addListener(avion_en_vol_free[avion_en_vol_free.length - 1], 'click', function() 
			{

				removeLine();
				$('#info').hide();
				$('#loader_data_map').show();
				$('#info').show(500);




				var focus_bounds = new google.maps.LatLngBounds();
				var String = 'mod=data_flight&pilot_id='+data_map.pilot_id;
				$.ajax({
					type: "GET",
					url: "/type/membre/map/php/data_req.php",
					data: String,
					dataType: "html",
					cache: false,
					success: function(data) 
					{
						$('#loader_data_map').hide()
						$('#info_data_map').html(data);
					},
					error: function(jqXHR, textStatus, errorThrown){error_log(jqXHR, textStatus, errorThrown)}
				});

				var String = 'mod=data_schedule&fn='+data_map.flightnum;
				$.ajax({
					type: "GET",
					url: "/type/membre/map/php/data_req.php",
					data: String,
					dataType: "json",
					cache: false,
					success: function(data) 
					{
						var dep_location = new google.maps.LatLng(data['dep']['lat'], data['dep']['lng']);
						var arr_location = new google.maps.LatLng(data['arr']['lat'], data['arr']['lng']);
						var path = new Array();
						path[path.length] = dep_location;
						path[path.length] = arr_location

						depMarker = new google.maps.Marker({
							position: dep_location,
							map: map,
							icon: '/type/membre/map/images/towerdeparture.png',
							title: data['dep']['name'],
							zIndex: 100
						});

						arrMarker = new google.maps.Marker({
							position: arr_location,
							map: map,
							icon: '/type/membre/map/images/towerarrival.png',
							title: data['arr']['name'],
							zIndex: 100
						});
						;

						flightPath = new google.maps.Polyline({
							path: path,
							geodesic: true,
							strokeColor: '#FF0000',
							strokeOpacity: 1.0,
							strokeWeight: 2
						});

						flightPath.setMap(map);

					},
				});

				var String = 'mod=data_black_box&fn='+data_map.flightnum;
				$.ajax({
					type: "GET",
					url: "/type/membre/map/php/data_req.php",
					data: String,
					dataType: "html",
					cache: false,
					success: function(data2) 
					{

						if (Array.isArray(eval(data2))==true)
						{
							var flightPlanCoordinates = eval(data2);
							flightPath2 = new google.maps.Polyline({
								path: flightPlanCoordinates,
								geodesic: true,
								strokeColor: '#FFFF00',
								strokeOpacity: 1.0,
								strokeWeight: 2
							});
							flightPath2.setMap(map);
						};

					},
					error: function(jqXHR, textStatus, errorThrown){error_log(jqXHR, textStatus, errorThrown)}
				});


				clearRouteMarkers();
			});

			bounds.extend(pos);
		}
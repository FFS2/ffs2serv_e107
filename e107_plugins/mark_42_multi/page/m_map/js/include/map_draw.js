function removeLine()
{
	Clear_route_schedules();
	Clear_route_black_box();
	clearRouteMarkers();
};

function error_log(jqXHR, textStatus, errorThrown)
{ 
	console.log('jqXHR:');
	console.log(jqXHR);
	console.log('textStatus:');
	console.log(textStatus);
	console.log('errorThrown:');
	console.log(errorThrown);
};

function clearRouteMarkers()
{
	if(routeMarkers!= null)
	{
		if(routeMarkers.length > 0)
		{
			for(var i = 0; i < routeMarkers.length; i++) {
				routeMarkers[i].setMap(null);
			}
		}
		routeMarkers.length = 0;
	}
};

function Clear_route_schedules()
{
	if(flightPath != null)
	{
		flightPath.setMap(null);
		flightPath = null;
	}
	if(depMarker != null)
	{
		depMarker.setMap(null);
		depMarker = null;
	}

	if(arrMarker != null)
	{
		arrMarker.setMap(null);
		arrMarker = null;
	}
};

function Clear_route_black_box()
{
	if(flightPath2 != null)
	{
		flightPath2.setMap(null);
		flightPath2 = null;
	}
};

function clearAircraft_Free()
{
	if(avion_en_vol_free.length > 0)
	{
		for(var i = 0; i < avion_en_vol_free.length; i++) {
			avion_en_vol_free[i].setMap(null);
		}
	}

	avion_en_vol_free.length = 0;
	clearRouteMarkers()
};
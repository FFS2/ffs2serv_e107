<?php 
//activation de la protection recursive pour les éléments qui seront intégrés
define ('PROTECTIONINCLUDE', TRUE);

//intégration de fichier de configuration général du site
require_once ($_SERVER["DOCUMENT_ROOT"] . '/core/config/config_gen.php');

//error_reporting (0);
error_reporting(E_ERROR | E_PARSE);

//intégration du compilateur de sortie  XML et de ses outils
require_once (mark_5 . '/core/function/tracker.php');

//intégration du gestionnaire de class site
require_once (mark_5 . '/core/app/Auto_loader.php');


Auto_loader::register ();
$db_m_5 = Db_mark5::getInstance ('MARK_5_ENR');
$db_airac = Db_mark5::getInstance ('AIRAC');
//var_dump($db_m_5);

function map_aircraft($db_m_5)
{

		//$where = " 1 ";

	$now = $date = date ("Y-m-d H:i:s");
	$date = new DateTime($now);
	$date->sub (new DateInterval('PT30M'));
	$time = $date->format ("Y-m-d H:i:s");

	$where = '`lastupdate` between  "' . $time . '" and "' . $now . '"';

	$data = '*';

	$result = $db_m_5->select ('`' . PF_MARK_5_ENR . 'acarsdata`', $data, $where,  '', 'list');
		//$result=$db_TKS->query_list("SELECT * FROM `tks_acarsdata`/* where `lastupdate` > '$expire'*/");


	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}

function map_airport($db_airac,$uprlat,$uprlng,$dwnlat,$dwnlng)
{
	$result=array($uprlat,$uprlng,$dwnlat,$dwnlng

		);
	$where = '`Latitude` < "'.$uprlat.'" and `Latitude` > "'.$dwnlat.'" and `Longitude` < "'.$uprlng.'" and `Longitude` > "'.$dwnlng. '"';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'Airports`', $data, $where,  '', 'list');


	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}

function map_runway($db_airac,$uprlat,$uprlng,$dwnlat,$dwnlng)
{
	$where = '`Latitude` < "'.$uprlat.'" and `Latitude` > "'.$dwnlat.'" and `Longitude` < "'.$uprlng.'" and `Longitude` > "'.$dwnlng. '"';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'Runways`', $data, $where,  '', 'list');

	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}
function info_aiport($db_airac,$ID)
{
	$where = '`AirportID` = "'.$ID.'"';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'Runways`', $data, $where,  '', 'list');

	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}

function metar($db_airac,$uprlat,$uprlng,$dwnlat,$dwnlng)
{

	$where = '`Latitude` < "'.$uprlat.'" and `Latitude` > "'.$dwnlat.'" and `Longitude` < "'.$uprlng.'" and `Longitude` > "'.$dwnlng. '"';
	//$data = '`latitude`,`longitude`,`raw_text`,`ID`,`station_id`';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'metar`', $data, $where,  '', 'list');

	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}

function map_vor($db_airac,$uprlat,$uprlng,$dwnlat,$dwnlng)
{
	$where = '(`Latitude` < "'.$uprlat.'" and `Latitude` > "'.$dwnlat.'" and `Longitude` < "'.$uprlng.'" and `Longitude` > "'.$dwnlng. '") and (`Type`="1" or `Type`="2" or `Type`="3" or `Type`="4")';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'Navaids`', $data, $where,  '', 'list');

	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}
function map_ndb($db_airac,$uprlat,$uprlng,$dwnlat,$dwnlng)
{
	$where = '(`Latitude` < "'.$uprlat.'" and `Latitude` > "'.$dwnlat.'" and `Longitude` < "'.$uprlng.'" and `Longitude` > "'.$dwnlng. '") and (`Type`="5" or `Type`="7")';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'Navaids`', $data, $where,  '', 'list');

	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}
function map_ils($db_airac,$uprlat,$uprlng,$dwnlat,$dwnlng)
{
	$where = '`Latitude` < "'.$uprlat.'" and `Latitude` > "'.$dwnlat.'" and `Longitude` < "'.$uprlng.'" and `Longitude` > "'.$dwnlng. '"';
	$data = '*';
	$result = $db_airac->select ('`' . PF_AIRAC . 'ILSes`', $data, $where,  '', 'list');

	if ($result!=null){echo json_encode($result);}
	else{echo json_encode(null);}
}
function data_black_box($db_m_5,$fn=null)
{

	if($fn!=null)
	{
		$where = '`flightnum` ="'. $fn.'"';

		$data = '*';

		$result = $db_m_5->select ('`' . PF_MARK_5_ENR . 'blackbox`', $data, $where, 'ORDER BY `id` DESC');

			//var_dump($result);

		$data_record=json_decode($result->record);

		foreach ($data_record as $key => $value) {
			if($value->data_type=='cycle')
			{
						//$real_route[]=array('lat' => $value->Lat,'lng' => $value->Lng);
				$real_route[]='{lat: '.$value->Lat.', lng: '.$value->Lng.'}';
			}
		}
		echo str_replace('"', '', json_encode($real_route));
	}
}

function data_schedule($db_m_5,$fn=null)
{
	if($fn!=null)
	{
			//$flightnum=explode('-', $fn, 2);
			//$flightnum[1]="-".$flightnum[1];

		$where = '`flightnum` ="'. $fn.'"';

		$data = '*';

		$result = $db_m_5->select ('`' . PF_MARK_5_ENR . 'resas`', $data, $where);

			//var_dump($result->depicao);

			//$resa=$db_TKS->query_list("SELECT * FROM `tks_schedules` where `flightnum` = '$flightnum[1]' and `code` = '$flightnum[0]'");
		$dep= new Airport($result->depicao);
		$arr= new Airport($result->arricao);
		$points=array(
			'dep' 	=> array('lat'=>$dep->lat,'lng'=>$dep->lng,'name'=>$dep->name,),
			'arr'	=> array('lat'=>$arr->lat,'lng'=>$arr->lng,'name'=>$arr->name,),
			);
		echo json_encode($points);
	}
	else{echo json_encode(null);}

}

function data_flight($db_m_5,$pilot_id)
{
		//$where = array(`flightnum` =>$fn;);
	$data = '*';
	$where = "`pilot_id` = '" . $pilot_id . "'";
	$acardata = $db_m_5->select ('`' . PF_MARK_5_ENR . 'acarsdata`', $data, $where,'');

	$where = "`flightnum` = '" . $acardata->flightnum . "' and `points`>2";
	$bb=$db_m_5->select ('`' . PF_MARK_5_ENR . 'blackbox`', $data, $where,'');

	$where = "`flightnum` = '" . $acardata->flightnum . "'";
	$resa=$db_m_5->select ('`' . PF_MARK_5_ENR . 'resas`', $data, $where,'');

		//if($bb!==FALSE)
		//{
	$data_on_map='
	<div class="aircraftInfoGridContainer" style="height: 387px; display: block;">
		<table class="aircraftInfoGrid">
			<tbody>
				<tr class="first">
					<td class="iconContainer">
						<span class="icon aircraft"></span>
					</td>
					<td colspan="2">
						Aircraft<br><span class="right strong" id="aircraftIcaoVal">( '.$acardata->aircraft.' )</span><br>
						<span id="aircraftVal" class="strong">'.$aircraft->fullname.'</span>
					</td>
				</tr>
				<tr>
					<td class="iconContainer"></td>
					<td colspan="2">
						Registration<br><span class="right strong">( '.$acardata->pilotname.' )</span><br>
						<span id="registrationVal" class="strong"><a class="regLink">'.$reg_aircraft.'</a></span>
					</td>
				</tr>
				<tr>
					<td class="iconContainer">
						<span class="icon cloud"></span>
					</td>
					<td>
						Altitude<br>
						<span id="altitudeVal" class="strong">'.$acardata->alt.'</span>
					</td>
					<td>
						Vertical Speed<br>
						<span id="vspdVal" class="strong">INF</span>
					</td>
				</tr>
				<tr>
					<td class="iconContainer"></td>
					<td>
						Speed<br>
						<span id="speedVal" class="strong">'.$acardata->gs.' kt</span>
					</td>
					<td>
						Track<br>
						<span id="trackVal" class="strong">'.$acardata->heading.'°</span>
					</td>
				</tr>
				<tr>
					<td class="iconContainer">
						<span class="icon satellite"></span>
					</td>
					<td>
						Latitude<br>
						<span id="latVal" class="strong">'.$acardata->lat.'</span>
					</td>
					<td>
						Longitude<br>
						<span id="lonVal" class="strong">'.$acardata->lng.'</span>
					</td>
				</tr>
				<tr>
					<td class="iconContainer"></td>
					<td>
						Radar<br>
						<span id="radarVal" class="strong">F-LFPO1</span>
					</td>
					<td>
						Squawk<br>
						<span id="squawkVal" class="strong">INF</span>
					</td>
				</tr>
				<tr>
					<td class="iconContainer"></td>
					<td colspan="2">
						Phase<br>
						<span id="latVal" class="strong">'.$acardata->phasedetail.'</span><br><br>
						<span id="latVal" class="strong">'.$acardata->arrapt.'</span>
					</td>


				</tr>


			</tbody>
		</table>
	</div>

	';
		//}

		//var_dump($resa);

	if($resa!==FALSE)
	{
		$percent=$resa->distance/100;
		$percent=100-($acardata->distremain/$percent);
		
		$aircraft=new Aircraft($resa->aircraft_id,$resa->code);

			//var_dump($aircraft);

		$data_on_map_2='
		<div class="aircraftImage"><a href="https://external.flightradar24.com/redir.php?url=http%3A%2F%2Fwww.jetphotos.net%2Fphoto%2F8153149" target="_blank" title="Voir en plus grand sur Jetphotos.net"><img src="'.$aircraft->image_thumb.'"></a></div>
		<div class="routeInformationBox">
			<div class="titleBox">'.$resa->flightnum.'</div>
			<div class="airportInfo" style="display: -webkit-box;">
				<span class="arrow" data-tooltip-maxwidth="200" data-tooltip-value="" data-iata=""></span>
				<div class="airport left from  airportLink" ><h4>'.$resa->depicao.'</h4>'.$dep->name.'</div>
				<div class="airport right to  airportLink" ><h4>'.$resa->arricao.'</h4>'.$arr->name.'</div>
				<br class="clear">
			</div>
			<div class="progressBar " style="display: block;">
				<div class="progress" style="width: '.$percent.'%;"></div>
			</div>
			<div class="statusInfo">
				<div class="airport left from">
					<div id="stdWrapper"><span class="small blue " >STD </span><span id="stdVal">'.$resa->deptime.'</span><br></div>
					<div id="atdWrapper"><span class="small blue " >ATD </span><span id="atdVal" class="hasTooltip" data-tooltip-value="00:54 ago">'.$acardata->deptime.'</span> <span class="small">CET</span></div>
				</div>
				<div class="airport right to">
					<div id="staWrapper"><span class="small blue " >STA </span><span id="staVal">'.$resa->arrtime.'</span><br></div>
					<div id="etaWrapper"><span class="small blue " >ETA </span><span id="etaVal" class="hasTooltip" data-tooltip-value="in 01:08">'.$acardata->arrtime.'</span> <span class="small">CET</span></div>
				</div>
				<br class="clear">
			</div>
		</div>';
	}
		/*echo '	  <ul class="aircraftActions">';
		echo '		<li class="first">';
		echo '		  <a class="showRouteLink" title="Show Route" data-callsign="RYR3JY">';
		echo '			<span class="icon route"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a id="follow-aircraft" title="Follow Aircraft">';
		echo '			<span class="icon center"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a class="pilotViewLink" title="Cockpit View" data-callsign="RYR3JY">';
		echo '			<span class="icon threeD"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li class="last">';
		echo '		  <a class="shareFlightLink" title="Share" data-callsign="RYR3JY" data-aircraft="884603e">';
		echo '			<span class="icon share"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '	  </ul>';
		}

		//var_dump($acardata);
		//echo 'test';

		/*$flightnum=explode('-', $fn, 2);
		$flightnum[1]="-".$flightnum[1];
		$acardata=new Data_map_2($fn);

		//$acardata=$db_TKS->query_list("SELECT * FROM `tks_acarsdata` where `flightnum` = '$fn'");

		//var_dump($acardata);

		$resa=$db_TKS->query_list("SELECT * FROM `tks_schedules` where `flightnum` = '$flightnum[1]' and `code` = '$flightnum[0]'");
		
		$type=$resa[0]->escad;

		$percent=$resa[0]->distance/100;
		$percent=100-($acardata->distremain/$percent);

		$dep= new Airport_2($resa[0]->depicao);
		$arr= new Airport_2($resa[0]->arricao);
		//var_dump($resa[0]->notes);
		
		$temp=explode('*', $resa[0]->notes);
		$temp=explode(':', $temp[0]);
		$id_achat=$temp[1];

		$aircraft_reg=new P_aircraft($id_achat,$type);

		$id_aircraft=$aircraft_reg->id_aircraft;
		
		$aircraft=new Aircraft($id_aircraft,$type);

		$reg_aircraft=strtoupper ($aircraft_reg->type)."-".$aircraft_reg->id_pilot."-".$aircraft_reg->id_achat;*/
		//if ($resa!=null){echo clean_JSON($resa);}
		//else{echo "pas d'info pour ce vol";}
		/*echo '<div class="aircraftSummary" style="display: block;">';
		if($acardata->flightnum!=0){


		echo '	  <div class="aircraftImage"><a href="https://external.flightradar24.com/redir.php?url=http%3A%2F%2Fwww.jetphotos.net%2Fphoto%2F8153149" target="_blank" title="Voir en plus grand sur Jetphotos.net"><img src="'.$aircraft->image_type_1.'"></a></div>';
		echo '	  <div class="routeInformationBox">';
		echo '		<div class="titleBox">'.$resa[0]->code.''.$resa[0]->flightnum.'</div>';
		echo '		<div class="airportInfo" style="display: -webkit-box;">';
		echo '		  <span class="arrow" data-tooltip-maxwidth="200" data-tooltip-value="" data-iata=""></span>';
		echo '		  <div class="airport left from  airportLink" ><h4>'.$resa[0]->depicao.'</h4>'.$dep->name.'</div>';
		echo '		  <div class="airport right to  airportLink" ><h4>'.$resa[0]->arricao.'</h4>'.$arr->name.'</div>';
		echo '		  <br class="clear">';
		echo '		</div>';
		echo '		<div class="progressBar " style="display: block;">';
		echo '		  <div class="progress" style="width: '.$percent.'%;"></div>';
		echo '		</div>';
		echo '		<div class="statusInfo">';
		echo '		  <div class="airport left from">';
		echo '			<div id="stdWrapper"><span class="small blue " >STD </span><span id="stdVal">'.$resa[0]->deptime.'</span><br></div>';
		echo '			<div id="atdWrapper"><span class="small blue " >ATD </span><span id="atdVal" class="hasTooltip" data-tooltip-value="00:54 ago">'.$acardata->deptime.'</span> <span class="small">CET</span></div>';
		echo '		  </div>';
		echo '		  <div class="airport right to">';
		echo '			<div id="staWrapper"><span class="small blue " >STA </span><span id="staVal">'.$resa[0]->arrtime.'</span><br></div>';
		echo '			<div id="etaWrapper"><span class="small blue " >ETA </span><span id="etaVal" class="hasTooltip" data-tooltip-value="in 01:08">'.$acardata->arrtime.'</span> <span class="small">CET</span></div>';
		echo '		  </div>';
		echo '		  <br class="clear">';
		echo '		</div>';
		echo '	  </div>';
		}
		/*echo '	  <ul class="aircraftActions">';
		echo '		<li class="first">';
		echo '		  <a class="showRouteLink" title="Show Route" data-callsign="RYR3JY">';
		echo '			<span class="icon route"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a id="follow-aircraft" title="Follow Aircraft">';
		echo '			<span class="icon center"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li>';
		echo '		  <a class="pilotViewLink" title="Cockpit View" data-callsign="RYR3JY">';
		echo '			<span class="icon threeD"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '		<li class="last">';
		echo '		  <a class="shareFlightLink" title="Share" data-callsign="RYR3JY" data-aircraft="884603e">';
		echo '			<span class="icon share"></span>';
		echo '		  </a>';
		echo '		</li>';
		echo '	  </ul>';*/
		echo '	</div>';

		echo $data_on_map_2;
		echo $data_on_map;
	}

	if (!empty($_GET))
	{
		if (isset($_GET['mod']) && $_GET['mod'] =='map_aircraft')
		{
			map_aircraft($db_m_5,date('Y-m-d H:i:s',strtotime("-1 hours")));
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='map_airport' && !empty($_GET['uprlat']) && !empty($_GET['uprlng']) && !empty($_GET['dwnlat']) && !empty($_GET['dwnlng']))
		{
			map_airport($db_airac,$_GET['uprlat'],$_GET['uprlng'],$_GET['dwnlat'],$_GET['dwnlng']);
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='map_vor' && !empty($_GET['uprlat']) && !empty($_GET['uprlng']) && !empty($_GET['dwnlat']) && !empty($_GET['dwnlng']))
		{
			map_vor($db_airac,$_GET['uprlat'],$_GET['uprlng'],$_GET['dwnlat'],$_GET['dwnlng']);
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='map_ndb' && !empty($_GET['uprlat']) && !empty($_GET['uprlng']) && !empty($_GET['dwnlat']) && !empty($_GET['dwnlng']))
		{
			map_ndb($db_airac,$_GET['uprlat'],$_GET['uprlng'],$_GET['dwnlat'],$_GET['dwnlng']);
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='map_runway' && !empty($_GET['uprlat']) && !empty($_GET['uprlng']) && !empty($_GET['dwnlat']) && !empty($_GET['dwnlng']))
		{
			map_runway($db_airac,$_GET['uprlat'],$_GET['uprlng'],$_GET['dwnlat'],$_GET['dwnlng']); 
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='map_ils' && !empty($_GET['uprlat']) && !empty($_GET['uprlng']) && !empty($_GET['dwnlat']) && !empty($_GET['dwnlng']))
		{
			map_ils($db_airac,$_GET['uprlat'],$_GET['uprlng'],$_GET['dwnlat'],$_GET['dwnlng']); 
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='data_flight' && isset($_GET['fn']) && !empty($_GET['fn']))
		{
			data_flight($db_m_5,$_GET['fn']);
				//echo 'test';
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='data_flight' && isset($_GET['pilot_id']))
		{
			data_flight($db_m_5,$_GET['pilot_id']);
			//echo 'test';
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='data_schedule' && isset($_GET['fn']) && !empty($_GET['fn']))
		{
			data_schedule($db_m_5,$_GET['fn']);
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='data_black_box' && isset($_GET['fn']) && !empty($_GET['fn']))
		{
			data_black_box($db_m_5,$_GET['fn']);
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='info_aiport' && !empty($_GET['ID']))
		{
			info_aiport($db_airac,$_GET['ID']); 
		}
		elseif (isset($_GET['mod']) && $_GET['mod'] =='metar' && !empty($_GET['uprlat']) && !empty($_GET['uprlng']) && !empty($_GET['dwnlat']) && !empty($_GET['dwnlng']))
		{
			metar($db_airac,$_GET['uprlat'],$_GET['uprlng'],$_GET['dwnlat'],$_GET['dwnlng']); 
		}
//var_dump($_GET);

	}
	//echo 'test';
	$db_m_5->clear();
	$db_airac->clear();
	?>


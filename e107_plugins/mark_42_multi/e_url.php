<?php
/*
 * e107 Bootstrap CMS
 *
 * Copyright (C) 2008-2015 e107 Inc (e107.org)
 * Released under the terms and conditions of the
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
 * 
 * IMPORTANT: Make sure the redirect script uses the following code to load class2.php: 
 * 
 * 	if (!defined('e107_INIT'))
 * 	{
 * 		require_once("../../class2.php");
 * 	}
 * 
 */
 
if (!defined('e107_INIT')) { exit; }

// v2.x Standard  - Simple mod-rewrite module. 

class mark_42_multi_url // plugin-folder + '_url'
{
	function config() 
	{
		$config = array();

		$config['other'] = array(
			'alias'         => 'mark_42_multi',                            // default alias 'mark_42_multi'. {alias} is substituted with this value below. Allows for customization within the admin area.
			'regex'			=> '^{alias}/?$', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> '{alias}', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/mark_42_multi.php', 		// file-path of what to load when the regex returns true.

		);


		$config['index'] = array(
			'regex'			=> '^mark_42_multi/?$', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> 'mark_42_multi', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/mark_42_multi.php', 		// file-path of what to load when the regex returns true.

		);

		$config['setting_serv'] = array(
			'regex'			=> '^setting-serv/?$', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> 'mark_42_', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/page/serveur_setting.php', 		// file-path of what to load when the regex returns true.

		);
		$config['switch2'] = array(
			'regex'			=> '^switch-serv/([\w]*)', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> 'mark_42_multi', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/page/my_current_serveur.php?serveur=$1', 		// file-path of what to load when the regex returns true.

		);

		$config['download_ffs'] = array(
			'regex'			=> '^download_ffs/?$', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> 'mark_42_', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/page/download_ffs.php', 		// file-path of what to load when the regex returns true.

		);
		$config['download_ffs_P3D'] = array(
			'regex'			=> '^download_ffs_p3d/?$', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> 'mark_42_', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/page/download_ffs_p3d.php', 		// file-path of what to load when the regex returns true.

		);
		$config['m_map'] = array(
			'regex'			=> '^m_map/?$', 						// matched against url, and if true, redirected to 'redirect' below.
			'sef'			=> 'mark_42_', 							// used by e107::url(); to create a url from the db table.
			'redirect'		=> '{e_PLUGIN}mark_42_multi/page/m_map/js/map.php', 		// file-path of what to load when the regex returns true.

		);

		return $config;
	}
	
}
<?php
/*
 * e107 website system
 *
 * Copyright (C) 2008-2014 e107 Inc (e107.org)
 * Released under the terms and conditions of the
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
 *
 * Related configuration module - News
 *
 *
*/

if (!defined('e107_INIT')) { exit; }



class mark_42_multi_related // include plugin-folder in the name.
{


	function compile($tags,$parm=array()) 
	{
		$sql = e107::getDb();
		$items = array();
			
		$tag_regexp = "'(^|,)(".str_replace(",", "|", $tags).")(,|$)'";
		
		$query = "SELECT * FROM `#mark_42_multi` WHERE mark_42_multi_id != ".$parm['current']." AND mark_42_multi_keywords REGEXP ".$tag_regexp."  ORDER BY mark_42_multi_datestamp DESC LIMIT ".$parm['limit'];
			
		if($sql->gen($query))
		{		
			while($row = $sql->fetch())
			{

				$items[] = array(
					'title'			=> varset($row['mark_42_multi_title']),
					'url'			=> e107::url('other',$row), // '{e_BASE}news.php?extend.'.$row['news_id'],
					'summary'		=> varset($row['mark_42_multi_summary']),
					'image'			=> '{e_PLUGIN}mark_42_multi/images/image.png'
				);
			}
			
			return $items;
	    }
		elseif(ADMIN)
		{
		//	return array(array('title'=>$query,'url'=>''));	
		}
	}
	
}



?>
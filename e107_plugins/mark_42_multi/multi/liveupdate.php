<?php
//initialisiation des slots et recherche d'un slot personnel
$slot=New Session_mp($xml->switch->key,'key_f');

//si un slot au nom du pilote existe
if($slot->id!=NULL)
{
	$slot->update_my_slot();
	//$slot->update_my_slot('none',base64_encode($send_AES_key))== TRUE
	//renvoie des slot des autres joueurs présents
	$data_xml["whazzup"] =$slot->get_other_slot($slot->server,base64_decode($slot->pu_key),$slot->tx_mod);



}

//préparation de la position de l'appareil pour effectuer une recherche sur NOAA
$lat_for_metar=(str_replace(",", ".",$xml->liveupdate->latitude));
$lng_for_metar=(str_replace(",", ".",$xml->liveupdate->longitude));


//echo "SELECT `icao`,`longitude`,`latitude`,`name`,( 3959 * acos ( cos ( radians('".$lat_for_metar."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$lng_for_metar."') ) + sin ( radians('".$lat_for_metar."') ) * sin( radians( latitude ) ) ) ) AS distance FROM `e107_tks_airports` HAVING distance < 60 ORDER BY distance LIMIT 10";

$sql->gen("SELECT `icao`,`longitude`,`latitude`,`name`,( 3959 * acos ( cos ( radians('".$lat_for_metar."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$lng_for_metar."') ) + sin ( radians('".$lat_for_metar."') ) * sin( radians( latitude ) ) ) ) AS distance FROM `e107_tks_airports` HAVING distance < 100 ORDER BY distance ASC LIMIT 10");

//$aiport=$sql->fetch();
$aiport=array();
$key_metar=0;
while($row = $sql->fetch())
{
	$aiport[$key_metar]['icao']=$row['icao'];
	$aiport[$key_metar]['distance']=$row['distance'];
	$key_metar++;



	/*$vat_sim_metar=file_get_contents("http://metar.vatsim.net/metar.php?id=".$row['icao']);
	if(strpos($vat_sim_metar, 'No METAR available')!=false)
	{
		$data_metar=explode(' ', $vat_sim_metar);
		$vat_sim_metar=str_replace("AUTO ", "", $vat_sim_metar);
		$vat_sim_metar=str_replace(" NOSIG", "", $vat_sim_metar);
		$vat_sim_metar=str_replace("MPS ", "KT", $vat_sim_metar);
		$vat_sim_metar=str_replace("&#xD;", "", $vat_sim_metar);

		$data_xml["metar"][$key_metar]["metar_station_id"]=$row['icao'];
		$data_xml["metar"][$key_metar]["station_id"]=$row['icao'];
		$data_xml["metar"][$key_metar]["raw_text"]=$vat_sim_metar;
		$data_xml["metar"][$key_metar]["distance"]=intval($row['distance']);
		$data_xml["metar"][$key_metar]["observation_time"]=$data_metar[1];
		$key_metar++;
	}
	else
	{
		$inter_com->addMessage (10, $msg, 'pas de météo retournée sur '.$row['icao']);//Vous avez été identifé avec un compte valide
	}*/

    //var_dump($row);
}
$key_metar=0;
foreach ($aiport as $key => $value)
{
	$sql->gen("SELECT * FROM `e107_tks_metar` WHERE `station_id`='".$value['icao']."' ORDER BY `observation_time` DESC");

	while($row = $sql->fetch())
	{
		//var_dump($row);
		$file_metar=str_replace("AUTO ", "", $row["raw_text"]);
		$file_metar=str_replace(" NOSIG", "", $file_metar);
		$file_metar=str_replace("MPS ", "KT", $file_metar);
		$file_metar=str_replace("&#xD;", "", $file_metar);
		$file_metar=str_replace("\$;", "", $file_metar);

		$data_xml["metar"][$key_metar]["metar_station_id"]=$value['icao'];
		$data_xml["metar"][$key_metar]["station_id"]=$value['icao'];
		$data_xml["metar"][$key_metar]["raw_text"]=$file_metar;
		$data_xml["metar"][$key_metar]["distance"]=intval($value['distance']);
		$data_xml["metar"][$key_metar]["observation_time"]=$row["observation_time"];
		$key_metar++;
	}

}


//var_dump($aiport);


/*
$sql->gen("SELECT `icao`,`longitude`,`latitude`,`name`,( 3959 * acos ( cos ( radians('".$lat_for_metar."') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('".$lng_for_metar."') ) + sin ( radians('".$lat_for_metar."') ) * sin( radians( latitude ) ) ) ) AS distance FROM `e107_tks_airports` HAVING distance < 150 ORDER BY distance LIMIT 0 , 1");

$aiport=$sql->fetch();

//echo $aiport['icao'];
//var_dump($aiport);


$metar=file_get_contents("https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=".$aiport['icao']."&hoursBeforeNow=4");

//echo "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=".$aiport['icao']."&hoursBeforeNow=4";

//$metar=file_get_contents("https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=LFRG&hoursBeforeNow=4");

//var_dump($metar);
//$xml = xml_extract ($metar);
$array_metar = XML2Array::createArray($metar);
//var_dump($array_metar);
if($array_metar!=NULL)
{

	foreach ($array_metar['response']['data']["METAR"] as $key => $value)
	{
		$i=0;
		foreach ($value['sky_condition'] as $key2 => $value2)
		{
			$sky_condition[$i]['sky_cover']=$value2['@attributes']['sky_cover'];
			$sky_condition[$i]['cloud_base_ft_agl']=$value2['@attributes']['cloud_base_ft_agl'];
			$i++;
		}


		$data=array(
			'station_id'=> strval($value['station_id']),
			'raw_text'=> strval($value['raw_text']),
			'observation_time'=> strval($value['observation_time']),
			'Latitude'=> strval($value['latitude']),
			'Longitude'=> strval($value['longitude']),
			'temp_c'=> strval($value['temp_c']),
			'dewpoint_c'=> strval($value['dewpoint_c']),
			'wind_dir_degrees'=> strval($value['wind_dir_degrees']),
			'wind_speed_kt'=> strval($value['wind_speed_kt']),
			'visibility_statute_mi'=> strval($value['visibility_statute_mi']),
			'altim_in_hg'=> strval($value['altim_in_hg']),
			'sky_condition' => json_encode($sky_condition),
			'sea_level_pressure_mb'=> strval($value['sea_level_pressure_mb']),
			'wx_string'=> strval($value['wx_string']),
			'flight_category'=> strval($value['flight_category']),
			'metar_type'=> strval($value['metar_type']),
			'elevation_m'=> strval($value['elevation_m']),
			);

		$data_xml["metar"][$key_metar]["metar_station_id"]="GLOB";
		$data_xml["metar"][$key_metar]["station_id"]=$value['station_id'];
		$data_xml["metar"][$key_metar]["raw_text"]=$value['raw_text'];
		//$data_xml["metar"][$key_metar]["raw_text"]=str_replace($row['station_id'], "GLOB", $row['raw_text']);
		$data_xml["metar"][$key_metar]["raw_text"]=str_replace("AUTO ", "", $data_xml["metar"][$key_metar]["raw_text"]);

		$data_xml["metar"][$key_metar]['observation_time']=$value['observation_time'];
		//$data_xml["metar"][$key_metar]['raw_text']=$row['raw_text'];
		$data_xml["metar"][$key_metar]['distance']=round($aiport['distance'], 2);


	}
}

*/



//recherche de donnée météo sur la copie de NOAA sur un rayon de 150 Nautiques, les plus proches sera donnée
/*$sql->gen("SELECT * FROM `e107_tks_metar` WHERE station_id='".$aiport."'");

$key_metar=0;
while($row = $sql->fetch())
{
	$key_metar++;


		//$data_xml["metar"][$key_metar]["metar_station_id"]=$row['station_id'];
	$data_xml["metar"][$key_metar]["metar_station_id"]="GLOB";
	$data_xml["metar"][$key_metar]["station_id"]=$row['station_id'];
	$data_xml["metar"][$key_metar]["raw_text"]=$row['raw_text'];
		//$data_xml["metar"][$key_metar]["raw_text"]=str_replace($row['station_id'], "GLOB", $row['raw_text']);
	$data_xml["metar"][$key_metar]["raw_text"]=str_replace("AUTO ", "", $data_xml["metar"][$key_metar]["raw_text"]);
	$data_xml["metar"][$key_metar]["raw_text"]=str_replace("MPS ", "KT", $data_xml["metar"][$key_metar]["raw_text"]);
	$data_xml["metar"][$key_metar]["raw_text"]=str_replace("NOSIG ", "", $data_xml["metar"][$key_metar]["raw_text"]);

	$data_xml["metar"][$key_metar]['observation_time']=$row['observation_time'];
		//$data_xml["metar"][$key_metar]['raw_text']=$row['raw_text'];
	//$data_xml["metar"][$key_metar]['distance']=round($row['distance'], 2);
}
unset($data_xml["metar"]);*/




//$vat_sim_metar=file_get_contents("http://metar.vatsim.net/metar.php?id=".$aiport);
//$data_xml["metar"][$key_metar]["metar_station_id"]="GLOB";
//$data_xml["metar"][$key_metar]["station_id"]=$aiport;
//$vat_sim_metar=str_replace("AUTO ", "", $vat_sim_metar);
//$vat_sim_metar=str_replace(" NOSIG", "", $vat_sim_metar);
//$vat_sim_metar=str_replace("MPS ", "KT", $vat_sim_metar);
//$vat_sim_metar=str_replace("&#xD;", "", $vat_sim_metar);
//$data_xml["metar"][$key_metar]["raw_text"]=$vat_sim_metar;


		//$data_xml["metar"][$key_metar]["raw_text"]=str_replace($row['station_id'], "GLOB", $row['raw_text']);
//$data_xml["metar"][$key_metar]["raw_text"]=str_replace("AUTO ", "", $data_xml["metar"][$key_metar]["raw_text"]);
//$data_xml["metar"][$key_metar]["raw_text"]=str_replace("MPS ", "KT", $data_xml["metar"][$key_metar]["raw_text"]);

	//$data_xml["metar"][$key_metar]['observation_time']=$row['observation_time'];




//initialisation des positions radars
$map=New Data_map($slot->pilotname);
//var_dump($map);
if($map->pilotname==NULL)
{
	$map->create_map($slot->pilot_id,$slot->pilotname,$xml);
}
else
{
	$map->update_map($slot->pilotname,$xml);
}
//var_dump($map);
?>

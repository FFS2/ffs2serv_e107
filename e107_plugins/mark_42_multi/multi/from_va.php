<?php 
error_reporting(E_ALL);
$data_xml;

define ('debug_ffst', false);

try
{
    //tentative de chargement des classes de E107
    if (!isset ($_E107))
    {
        if (!@include_once("./../../../class2.php")) throw new Exception ("Votre E107 est introuvable");
    }


    //tentative de chargement du protocole xml
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/xml.php")) throw new Exception ("Votre systéme XML est introuvable");

    //tentative de chargement des protocoles de cryptage
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Crypt/Random.php"))
    {
        throw new Exception ("Votre élément de cryptage Random  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Crypt/Base.php"))
    {
        throw new Exception ("Votre élément de cryptage Base  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Crypt/Rijndael.php"))
    {
        throw new Exception ("Votre élément de cryptage Rijndael  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Crypt/Hash.php"))
    {
        throw new Exception ("Votre élément de cryptage Hash  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Math/BigInteger.php"))
    {
        throw new Exception ("Votre élément de cryptage BigInteger  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Crypt/RSA.php"))
    {
        throw new Exception ("Votre élément de cryptage RSA  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/phpseclib/Crypt/AES.php"))
    {
        throw new Exception ("Votre élément de cryptage AES  est introuvable");
    }
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/crypto.php"))
    {
        throw new Exception ("Le systéme de cryptage simplifié est introuvable");
    }

    //tentative de chargement des sessions
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/Session_mp.php")) throw new Exception ("Le systéme de sessions est introuvable");
    //tentative de chargement des logs
    if (!@include_once("./" . e_PLUGIN . "mark_42_multi/class/erreur_log.php")) throw new Exception ("Le systéme d'inter-com est introuvable");

    //vérification de l'initialisation de E107
    if (!defined ('e107_INIT')) throw new Exception ("Votre E107 n'est pas initialisé");
}
catch (Exception $e)
{
    //en cas de soucis on affiche l'origine du dysfonctionnement
    echo $e->getMessage ();
    //on stop l'execution du programme
    die;
}

date_default_timezone_set ("Europe/Brussels");

//initialisation de l'objet pour les messages de retour sur FFSTracker
$inter_com = New Erreur();



//initialisiation des slots et recherche d'un slot personnel
//$slot=New Session_mp('1-28-1503400417','key_f');
//$data_xml["whazzup"] =$slot->get_other_slot('0',base64_decode('r4N5cx9Yu1Gam/qbScsmYUEfGO3VZ6EIjlP8JW3rI44='),TRUE,'1'); 
//echo $_GET['pu_key'];
$slot=New Session_mp();

//var_dump($_GET);

//create_slot($s_member["user_id"],$username, $xml,'1',base64_encode($send_AES_key),'0');

$slot->create_passerelle_slot('0000',"FFS2_".$_GET['u'], '0',$_GET['puk'],$_GET['i'],$_GET['lip'],$_GET['p']);

//var_dump($slot);
//$slot->create_passerelle_slot(000,$username = NULL, $xml = NULL,$server=0,$pu_key=null,$tx_mod=null,$attached=0)
//$pu_key=$_GET['puk'];
//$new_gen_aes=$_GET['ng'];
$data_xml=$slot->get_other_slot_nenc('0',base64_decode($_GET['puk']),$_GET['ng'],'1'); 



//$data_xml["whazzup"] =$slot->get_other_slot($slot->server,base64_decode($slot->pu_key),$new_gen_aes,$slot->tx_mod); 

//Rajout des messages dans le XML
if ($inter_com->getMessage () != NULL)
{
    $data_xml["erreur"] = $inter_com->getMessage ();
}
if ($inter_com->getWarning () != NULL)
{
    $data_xml["erreur1"] = $inter_com->getWarning ();
}
if ($inter_com->getError () != NULL)
{
    $data_xml["erreur2"] = $inter_com->getError ();
}
if ($inter_com->getDebug () != NULL)
{
    $data_xml["erreur3"] = $inter_com->getDebug ();
}
/*

echo "test";
//var_dump($slot);
var_dump($data_xml);
echo "<br><br><br>";


$data2 = pre_xml ($data_xml);
$xml_va   = new Array2XML();

$xml_va->buildXML ($data2);
var_dump($xml_va);
print($xml_va);*/

//echo $test = json_encode($data_xml);
//var_dump(json_decode($test));

echo serialize($data_xml);


//var_dump($data2);
 ?>
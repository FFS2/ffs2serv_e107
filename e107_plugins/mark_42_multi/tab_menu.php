<?php
/*
 * e107 website system
 *
 * Copyright (C) 2008-2013 e107 Inc (e107.org)
 * Released under the terms and conditions of the
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
 *
 */

if (!defined('e107_INIT')) 
{
		//tentative de chargement des classes de E107
	if (! @include_once("./../../class2.php"))
		throw new Exception ("Votre E107 est introuvable");
	//tentative de chargement du gestionnaire de carte
	if (! @include_once("./".e_PLUGIN."mark_42_multi/class/map_data.php"))
		throw new Exception ("Le gestionnaire de carte est introuvable");

	echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>';
};

?>
<style type="text/css">
	@font-face {
		font-family: "num_display";
		src: url(<?=e_PLUGIN."mark_42_multi"?>/page/fonts/DS-DIGIB.TTF) format("truetype");
	}
	.tab_descript {padding: 3px 4px;
		color: #FF8900;
		background: #32331D;
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
		text-align: center;
		text-transform: uppercase;
		font-size: 1.5em;

	}
	.like_display{
		color: orange;
		font-family: 'num_display';
		text-align: center;
		text-transform: uppercase;
		font-size: 1.25em;
		background: black;
	}
</style>
<hr style="border-color:#70787d;">
<h3 class="caption">Pilotes en vols</h3>
<hr style="border-color:#70787d;margin-top: 0px; margin-bottom: 5px;">
<hr style="border-color:#70787d;margin-top: 0px; margin-bottom: 5px;">
	

<div style="text-align: left;margin: 1em;">
	
	<div id="info" style="position: absolute;z-index: 2;background: #FFF;//height: 95%; display:none;border-radius: 0 8px 0 0;">
		
	</div>
	<div id="lab">menu en dev</div>
</div>
<BR><BR>



<script>

	var sort="none";
		
	var tab_start='';
	var tab_end='';
	var tab_line='';


	function alertObject(obj){console.log(obj);}

	var refreshTime=10000;
	live_Refresh();

	function error_log(jqXHR, textStatus, errorThrown)
	{ 
		console.log('jqXHR:');
		console.log(jqXHR);
		console.log('textStatus:');
		console.log(textStatus);
		console.log('errorThrown:');
		console.log(errorThrown);
	};
	
	setInterval(function () { live_Refresh(); }, refreshTime);

	function live_Refresh()
	{
		var String = 'mod=tab_aircraft&sort='+sort;

		//alertObject(String);
		$.ajax({
			type: "GET",
			url: "<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
			data: String,
			//dataType: "json",
			cache: false,
			success: function(data) 
			{
				
				tab_line='';
				tab_line=data;
				document.getElementById('lab').innerHTML=tab_start+tab_line+tab_end;
			},
			error : function(resultat, statut, erreur){
				alertObject(resultat.responseText);
			},

		});
	}
	





</script>
<script type="text/javascript" src="/e107_web/lib/jquery-once/jquery.once.min.js?1496563042"></script>

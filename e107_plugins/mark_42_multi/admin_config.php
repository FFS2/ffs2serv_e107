<?php

	// Generated e107 Plugin Admin Area

	require_once ('../../class2.php');
	if (!getperms ('P')) {
		e107::redirect ('admin');
		exit;
	}

	// e107::lan('mark_42_multi',true);


	class mark_42_multi_adminArea extends e_admin_dispatcher
	{

		protected $modes = array (

			'main' => array ('controller' => 'tks_mp_ui', 'path' => NULL, 'ui' => 'tks_mp_form_ui', 'uipath' => NULL), 'main2' => array ('controller' => 'tks_security_ui', 'path' => NULL, 'ui' => 'tks_security_form_ui', 'uipath' => NULL), 'main3' => array ('controller' => 'tks_acarsdata_ui', 'path' => NULL, 'ui' => 'tks_acarsdata_form_ui', 'uipath' => NULL), 'main4' => array ('controller' => 'tks_serveur_ui', 'path' => NULL, 'ui' => 'tks_serveur_form_ui', 'uipath' => NULL), 'main5' => array ('controller' => 'tks_serveur_member_ui', 'path' => NULL, 'ui' => 'tks_serveur_member_form_ui', 'uipath' => NULL), 'main6' => array ('controller' => 'tks_airports_ui', 'path' => NULL, 'ui' => 'tks_airports_form_ui', 'uipath' => NULL), 'airacs' => array ('controller' => 'tks_airac_ui', 'path' => NULL, 'ui' => NULL, 'uipath' => NULL),


		);


		protected $adminMenu = array (

			'main/list' => array ('caption' => 'Liste des joeurs connectés', 'perm' => 'P'), //'main/create'		=> array('caption'=> LAN_CREATE, 'perm' => 'P'),
			'divider1' => array ('divider' => TRUE), 'main2/list' => array ('caption' => "Liste des éléments de sécurité", 'perm' => 'P'), 'main2/create' => array ('caption' => "Ajourter un éléments de sécurité", 'perm' => 'P'), 'divider3' => array ('divider' => TRUE), 'main3/list' => array ('caption' => "En temps réel", 'perm' => 'P'), 'divider4' => array ('divider' => TRUE), 'main4/list' => array ('caption' => "Liste des serveurs", 'perm' => 'P'), 'main4/create' => array ('caption' => "Ajourter un serveur", 'perm' => 'P'), 'divider5' => array ('divider' => TRUE), 'main5/list' => array ('caption' => "Liste des attributions<br> joueurs/serveurs", 'perm' => 'P'), 'main5/create' => array ('caption' => "Ajourter une attributions", 'perm' => 'P'), 'divider6' => array ('divider' => TRUE), 'main6/list' => array ('caption' => "Gèrer les aéroports", 'perm' => 'P'), 'main6/create' => array ('caption' => "Créer un aéroport", 'perm' => 'P'), 'divider7' => array ('divider' => TRUE), 'airacs/install_airac' => array ('caption' => "Mettre à jour<br> les Airacs<br>EAGLE SOFT ", 'perm' => 'P'),

			// 'main/custom'		=> array('caption'=> 'Custom Page', 'perm' => 'P')
		);

		protected $adminMenuAliases = array ('main/edit' => 'main/list', 'main2/edit' => 'main2/list', 'main3/edit' => 'main3/list', 'main4/edit' => 'main4/list', 'main5/edit' => 'main5/list', 'main6/edit' => 'main6/list', 'airacs/edit' => 'airacs/list',);

		protected $menuTitle = 'Multi';
	}

	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_mp_ui.php");
	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_security_ui.php");
	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_acarsdata_ui.php");
	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_serveur_ui.php");
	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_serveur_member_ui.php");
	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_airport_ui.php");
	require_once (e_PLUGIN . "/mark_42_multi/admin_ui/tks_airac_ui.php");


	class tks_mp_form_ui extends e_admin_form_ui
	{

	}

	class tks_security_form_ui extends e_admin_form_ui
	{

	}

	class tks_acarsdata_form_ui extends e_admin_form_ui
	{

	}

	class tks_serveur_form_ui extends e_admin_form_ui
	{

	}

	class tks_serveur_member_form_ui extends e_admin_form_ui
	{

	}

	class tks_airports_form_ui extends e_admin_form_ui
	{

	}

	$get_task = array ();
	foreach ($_GET as $key => $value) {
		$get_task[$key] = $value;
	}
	foreach ($_POST as $key => $value) {
		$get_task[$key] = $value;
	}


	new mark_42_multi_adminArea();


	require_once (e_ADMIN . "auth.php");
	error_reporting (E_ALL);
	if ($get_task['mode'] == "airacs" && $get_task['action'] == "install_airac") {
		echo "<h3>Aéroports</h3>";
		echo '<form method="POST" enctype="multipart/form-data" style="margin-top: 50px;">';
		echo '<input type="hidden" name="MAX_FILE_SIZE" value="10000000">';
		echo '<input type="hidden" name="mode" value="Airports">';
		echo '<input type="hidden" name="action" value="load_Airports">';
		echo '<p>Fichier airac : <input type="file" name="airac_file" accept=".csv"></p>';
		echo '<br><br>';
		echo '<p><input type="submit" name="envoyer" value="Mettre à jour les aéroports"></p>';
		echo '</form>';
		echo "<hr>";

		echo "<h3>Pistes</h3>";
		echo '<form method="POST" enctype="multipart/form-data" style="margin-top: 50px;">';
		echo '<input type="hidden" name="MAX_FILE_SIZE" value="10000000">';
		echo '<input type="hidden" name="mode" value="Runways">';
		echo '<input type="hidden" name="action" value="load_runways">';
		echo '<p>Plan de vol : <input type="file" name="airac_file" accept=".csv"></p>';
		echo '<br><br>';
		echo '<p><input type="submit" name="envoyer" value="Mettre à jour les pistes"></p>';
		echo '</form>';
		echo "<hr>";

		echo "<h3>Vor et NDB</h3>";
		echo '<form method="POST" enctype="multipart/form-data" style="margin-top: 50px;">';
		echo '<input type="hidden" name="MAX_FILE_SIZE" value="10000000">';
		echo '<input type="hidden" name="mode" value="Navaid">';
		echo '<input type="hidden" name="action" value="load_navaid">';
		echo '<p>Plan de vol : <input type="file" name="airac_file" accept=".csv"></p>';
		echo '<br><br>';
		echo '<p><input type="submit" name="envoyer" value="Mettre à jour les Vor et NDB"></p>';
		echo '</form>';
		echo "<hr>";

		echo "<h3>Ils</h3>";
		echo '<form method="POST" enctype="multipart/form-data" style="margin-top: 50px;">';
		echo '<input type="hidden" name="MAX_FILE_SIZE" value="10000000">';
		echo '<input type="hidden" name="mode" value="ILSes">';
		echo '<input type="hidden" name="action" value="load_ils">';
		echo '<p>Plan de vol : <input type="file" name="airac_file" accept=".csv"></p>';
		echo '<br><br>';
		echo '<p><input type="submit" name="envoyer" value="Mettre à jour les Ils"></p>';
		echo '</form>';
		echo "<hr>";


	}
	elseif (($get_task['mode'] == "Airports" or $get_task['mode'] == "Runways" or $get_task['mode'] == "Navaid" or $get_task['mode'] == "ILSes") && $get_task['action'] != "install_airac") {
		if ($get_task['mode'] == "Airports" && $get_task['action'] == "load_Airports") {
			include (e_PLUGIN . "/mark_42_multi/admin_ui/airac_loader/airac_airports.php");
		}
		if ($get_task['mode'] == "Runways" && $get_task['action'] == "load_runways") {
			include (e_PLUGIN . "/mark_42_multi/admin_ui/airac_loader/airac_runways.php");
		}
		if ($get_task['mode'] == "Navaid" && $get_task['action'] == "load_navaid") {
			include (e_PLUGIN . "/mark_42_multi/admin_ui/airac_loader/airac_navaid.php");
		}
		if ($get_task['mode'] == "ILSes" && $get_task['action'] == "load_ils") {
			include (e_PLUGIN . "/mark_42_multi/admin_ui/airac_loader/airac_ils.php");
		}

	}
	else {
		e107::getAdminUI ()->runPage ();
	}


	require_once (e_ADMIN . "footer.php");
	exit;

?>
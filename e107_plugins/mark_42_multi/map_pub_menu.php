<?php
/*
 * e107 website system
 *
 * Copyright (C) 2008-2013 e107 Inc (e107.org)
 * Released under the terms and conditions of the
 * GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
 *
 */

if (!defined('e107_INIT')) 
{
		//tentative de chargement des classes de E107
	if (! @include_once("./../../class2.php"))
		throw new Exception ("Votre E107 est introuvable");
	//tentative de chargement du gestionnaire de carte
	if (! @include_once("./".e_PLUGIN."mark_42_multi/class/map_data.php"))
		throw new Exception ("Le gestionnaire de carte est introuvable");

	echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>';
};
define('api_google_map_key','you_api_key');
?>
<style >
	#ffs_map {height: 500px;}

	.gmnoprint img { max-width: none; }

	#map-canvas *, #map-canvas *:before, #map-canvas *:after {
		-moz-box-sizing: content-box!important;
		-webkit-box-sizing: content-box!important;
		box-sizing: content-box!important;  
	}
	#map-canvas img {
		max-width: none;
	}
	#map-canvas label {
		width: auto;
		display: inline;
	}
</style>

<style >
	a.close {width: 23px;height: 23px;display: block;position: absolute;top: 0;right: 0;cursor: pointer;z-index: 100;color: #333;text-decoration: none;}

	/*-------------------------------------------
	    Left col overlay
	-------------------------------------------*/

	#leftColOverlay { display: block; //position: absolute; z-index: 1101; //width: 225px; //height: 500px; top: 51px; left: -227px; background: #fff; overflow: hidden; -moz-border-radius: 0 8px 0 0; -webkit-border-radius: 0 8px 0 0; border-radius: 0 8px 0 0; box-shadow: 1px 0 2px rgba(50, 50, 50, 0.9); -moz-box-shadow: 1px 0 2px rgba(50, 50, 50, 0.9); -webkit-box-shadow: 1px 0 2px rgba(50, 50, 50, 0.9); }
	#leftColOverlay a.close { width: 23px; height: 23px; background: url('https://cdn.flightradar24.com/images/userUI_sprite.png?14') 0 -104px no-repeat; display: block; position: absolute; top: 0; left: 200; cursor: pointer; z-index: 100; }
	#leftColOverlay span.loader { background: url('https://cdn.flightradar24.com/images/loader_info.gif'); display: block; width: 32px; height: 32px; margin: 90px 97px 0 97px; }
	#leftColOverlay span.loader.error { background: url('https://cdn.flightradar24.com/images/loader_error.png') no-repeat; }
	#leftColOverlay span.errorMsg { color: #bbb; display: block; text-align: center; font-size: 0.95em; padding: 15px; }
	#leftColOverlay .aircraftImage { position: relative; margin: 7px 7px 2px 7px; }
	#leftColOverlay .imageOwner { position: absolute; bottom: 0; width: 204px; padding: 2px 3px; color: #f0f0f0; font-size: 0.8em; text-align: left; text-shadow: 0 1px 0 #000;}
	#leftColOverlay .imageOwner a { display: inline-block; font-weight: bold; float: right; color: inherit; text-shadow: inherit; }
	#leftColOverlay .imageOwner a i { font-size: 1.8em; font-weight: bold; margin-top: -6px; margin-right: 4px; }
	#leftColOverlay .aircraftImage a { cursor: pointer; }
	#leftColOverlay .aircraftImage img { width: 210px; vertical-align: bottom; min-height: 130px; display: block; }
	#leftColOverlay .aircraftImage img.sideView { min-height: 94px; }
	#leftColOverlay .aircraftImage .sideview-mag {position: absolute;border-radius: 3px;width: 18px;height: 18px;font-size: 16px;padding: 1px 0 0 2px;box-sizing: border-box;right: 4px;bottom: 4px;color: #fff;cursor: pointer;}

	#leftColOverlay .titleBox { background: #000; margin: 2px 7px 7px 7px; border: 1px solid #000; padding: 2px 6px; color: #fff; font-size: 0.9em; text-shadow: 0 1px 0 #000; position: relative;
		box-shadow: 0 1px 3px rgba(0,0,0,0.6);
		-moz-box-shadow: 0 1px 3px rgba(0,0,0,0.6);
		-webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.6);
		background: #808080; /* Old browsers */
		background: -moz-linear-gradient(top,  #808080 0%, #303030 2%, #0c0c0c 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#808080), color-stop(2%,#303030), color-stop(100%,#0c0c0c)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #808080 0%,#303030 2%,#0c0c0c 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #808080 0%,#303030 2%,#0c0c0c 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #808080 0%,#303030 2%,#0c0c0c 100%); /* IE10+ */
		background: linear-gradient(to bottom,  #808080 0%,#303030 2%,#0c0c0c 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#808080', endColorstr='#0c0c0c',GradientType=0 ); /* IE6-9 */
	}
	#leftColOverlay .titleBox span.large { font-size: 1.6em; font-weight: bold; }
	#leftColOverlay .titleBox a.flightLink { color: #fff; cursor: pointer; }
	#leftColOverlay .routeInformationBox { border-top: 1px solid #005c90; border-bottom: 1px solid #005c90; margin-top: -29px; color: #fff; padding-bottom: 5px;
		box-shadow: 0 0 1px #9BE3F6 inset, 0 1px 0 #9BE3F6 inset, 0 -1px 3px rgba(0,0,0,0.4);
		-moz-box-shadow: 0 0 1px #9BE3F6 inset, 0 1px 0 #9BE3F6 inset, 0 -1px 3px rgba(0,0,0,0.4);
		-webkit-box-shadow: 0 0 1px #9BE3F6 inset, 0 1px 0 #9BE3F6 inset, 0 -1px 3px rgba(0,0,0,0.4);
		background: rgb(92,184,229); /* Old browsers */
		background: -moz-linear-gradient(top,  rgba(92,184,229,1) 0%, rgba(43,114,173,1) 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(92,184,229,1)), color-stop(100%,rgba(43,114,173,1))); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  rgba(92,184,229,1) 0%,rgba(43,114,173,1) 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  rgba(92,184,229,1) 0%,rgba(43,114,173,1) 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  rgba(92,184,229,1) 0%,rgba(43,114,173,1) 100%); /* IE10+ */
		background: linear-gradient(to bottom,  rgba(92,184,229,1) 0%,rgba(43,114,173,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#5cb8e5', endColorstr='#2b72ad',GradientType=0 ); /* IE6-9 */
	}
	#leftColOverlay .routeInformationBox .airportInfo { margin: 25px 0 0 0; font-size: 0.95em; text-shadow: 0 1px 0 #003958; position: relative;}
	#leftColOverlay .routeInformationBox .airportInfo .airport { cursor: pointer; }
	#leftColOverlay .routeInformationBox .airport { width: 48%; text-align: center; padding: 0 2px; }
	#leftColOverlay .routeInformationBox .airportInfo .airport h3 { font-size: 1.95em; line-height: 1.1em; text-shadow: 0 1px 0 #6fc9ff, 0 2px 0 #1F77AC, 0 2px 1px #000; }
	#leftColOverlay .routeInformationBox .airportInfo span.arrow { width: 45px; height: 16px; display: block; position: absolute; top: 7px; left: 88px; border: none;
		background: url('https://cdn.flightradar24.com/images/userUI_sprite.png?14') 0 -464px no-repeat;
	}
	#leftColOverlay .routeInformationBox .airportInfo span.fa-random { background: none; font-size: 20px; color: #fff; text-align: center; -webkit-animation: animateIcon 2s linear 0s infinite alternate; animation: animateIcon 2s linear 0s infinite alternate; cursor: pointer; }
	@-webkit-keyframes animateIcon { 0%   { color: #fff; } 25%  { color: #C0E8FF; } 50%  { color: #83AEC6; } 75%  { color: #C0E8FF; } 100% { color: #fff; } }
	@keyframes animateIcon { 0%   { color: #fff; } 25%  { color: #C0E8FF; } 50%  { color: #83AEC6; } 75%  { color: #C0E8FF; } 100% { color: #fff; } }
	#leftColOverlay .routeInformationBox .progressBar { background: #33699E; width: 211px; height: 8px; margin: 5px 7px 0 7px;
		border-radius: 10px;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		box-shadow: 0 2px 5px #0F396D inset, 0 1px 0 #5092c0;
		-moz-box-shadow: 0 2px 5px #0F396D inset, 0 1px 0 #5092c0;
		-webkit-box-shadow: 0 2px 5px #0F396D inset, 0 1px 0 #5092c0;
	}
	#leftColOverlay .routeInformationBox .progressBar .progress { width: 40%; height: 100%;
		border-radius: 10px;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		box-shadow: 0 1px 0 #0060ab;
		-moz-box-shadow: 0 1px 0 #0060ab;
		-webkit-box-shadow: 0 1px 0 #0060ab;
		background: #ecf6fc; /* Old browsers */
		background: -moz-linear-gradient(top,  #ecf6fc 0%, #b3d7f2 100%); /* FF3.6+ */
		background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ecf6fc), color-stop(100%,#b3d7f2)); /* Chrome,Safari4+ */
		background: -webkit-linear-gradient(top,  #ecf6fc 0%,#b3d7f2 100%); /* Chrome10+,Safari5.1+ */
		background: -o-linear-gradient(top,  #ecf6fc 0%,#b3d7f2 100%); /* Opera 11.10+ */
		background: -ms-linear-gradient(top,  #ecf6fc 0%,#b3d7f2 100%); /* IE10+ */
		background: linear-gradient(to bottom,  #ecf6fc 0%,#b3d7f2 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ecf6fc', endColorstr='#b3d7f2',GradientType=0 ); /* IE6-9 */
	}
	#leftColOverlay .routeInformationBox .statusInfo { margin: 5px 0 0 0; font-size: 0.9em; text-shadow: 0 1px 0 #253b4b; }
	#leftColOverlay .routeInformationBox .statusInfo #atdWrapper { font-size: 1.1em; }
	#leftColOverlay .routeInformationBox .statusInfo #etaWrapper { font-size: 1.1em; }
	#leftColOverlay .routeInformationBox .statusInfo #atdVal { font-weight: bold; }
	#leftColOverlay .routeInformationBox .statusInfo #etaVal { font-weight: bold; }
	#leftColOverlay .routeInformationBox .statusInfo span.small { font-size: 0.8em; }
	#leftColOverlay .routeInformationBox .statusInfo span.blue { color: #c1eaff; }
	#leftColOverlay table.aircraftInfoGrid { width: 100%; border-collapse: collapse; }
	#leftColOverlay table.aircraftInfoGrid td { padding: 3px 6px; vertical-align: top; line-height: 1.1em; color: #666; border-left: 1px solid #dadada; border-bottom: 1px solid #dadada; }
	#leftColOverlay table.aircraftInfoGrid tr.first td { padding-top: 5px; }
	#leftColOverlay table.aircraftInfoGrid td span.strong { color: #333; font-weight: bold; font-size: 1.1em; line-height: 1.3em; }
	#leftColOverlay table.aircraftInfoGrid tr td.iconContainer { width: 31px; background: #dadada; padding: 0; }
	#leftColOverlay table.aircraftInfoGrid tr td.iconContainer span.icon { display: block; background: url('https://cdn.flightradar24.com/images/userUI_sprite.png?14') -208px -464px no-repeat; }
	#leftColOverlay table.aircraftInfoGrid tr td.iconContainer span.icon.aircraft { width: 23px; height: 26px; background-position: -208px -464px; margin: 8px 0 0 4px; }
	#leftColOverlay table.aircraftInfoGrid tr td.iconContainer span.icon.cloud { width: 24px; height: 17px; background-position: -178px -464px; margin: 3px 0 0 4px; }
	#leftColOverlay table.aircraftInfoGrid tr td.iconContainer span.icon.satellite { width: 20px; height: 27px; background-position: -153px -464px; margin: 1px 0 0 6px; }
	#leftColOverlay .regLink { cursor: pointer; }

	#ffs_map {height: 500px;}

	.gmnoprint img { max-width: none; }

	 #map-canvas *, #map-canvas *:before, #map-canvas *:after {
        -moz-box-sizing: content-box!important;
        -webkit-box-sizing: content-box!important;
        box-sizing: content-box!important;  
    }
    #map-canvas img {
        max-width: none;
    }
    #map-canvas label {
        width: auto;
        display: inline;
    }
</style>

<!-- Modal -->
	<div class="modal fade" id="info_data" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="aircraft_info_title">Information sur l'objet</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<div id="leftColOverlay">
				<div id="info_data_map"><div id="aircraft_info_text"></div></div>
			</div>
	       	
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
	      </div>
	    </div>
	  </div>
	</div>	


<hr style="border-color:#70787d;">
<h3 class="caption">Mini carte</h3>
<hr style="border-color:#70787d;margin-top: 0px; margin-bottom: 5px;">
<hr style="border-color:#70787d;margin-top: 0px; margin-bottom: 5px;">

<div style="text-align: left;margin: 1em;">
	
	<div id="info" style="position: absolute;z-index: 2;background: #FFF;//height: 95%; display:none;border-radius: 0 8px 0 0;">
		<div id="leftColOverlay" style="//float: left; width: 225px; max-height:500px; overflow-x: hidden; overflow-y: scroll;">
			<a class="close" title="Close" onclick="removeLine();$('#info').hide();$('#loader_data_map').show();"></a>
			<br>
			<div id="loader_data_map"><img src="./<?=e_PLUGIN."mark_42_multi"?>/page/map/images/loader.gif"></div>
			<div id="info_data_map"></div>
		</div>
	</div>
	<div id="ffs_map"></div>

</div><BR><BR>




<script>
	function alertObject(obj){console.log(obj);}

	var refreshTime_aev=10000;
	var avion_en_vol_free = [];
	var run_once = false;

	var	routeMarkers;
	var flightPath;
	var flightPath2;
	var depMarker;
	var arrMarker;
	var map;


	function removeLine()
	{
		Clear_route_schedules();
		Clear_route_black_box();
		clearRouteMarkers();
	}

	function error_log(jqXHR, textStatus, errorThrown)
	{ 
		console.log('jqXHR:');
		console.log(jqXHR);
		console.log('textStatus:');
		console.log(textStatus);
		console.log('errorThrown:');
		console.log(errorThrown);
	};
	function clearRouteMarkers()
	{
		if(routeMarkers!= null)
		{
			if(routeMarkers.length > 0)
			{
				for(var i = 0; i < routeMarkers.length; i++) {
					routeMarkers[i].setMap(null);
				}
			}
			routeMarkers.length = 0;
		}
	}
	function Clear_route_schedules()
	{
		if(flightPath != null)
		{
			flightPath.setMap(null);
			flightPath = null;
		}
		if(depMarker != null)
		{
			depMarker.setMap(null);
			depMarker = null;
		}

		if(arrMarker != null)
		{
			arrMarker.setMap(null);
			arrMarker = null;
		}
	};
	function Clear_route_black_box()
	{
		if(flightPath2 != null)
		{
			flightPath2.setMap(null);
			flightPath2 = null;
		}
	};
	function clearAircraft_Free()
	{
		if(avion_en_vol_free.length > 0)
		{
			for(var i = 0; i < avion_en_vol_free.length; i++) {
				avion_en_vol_free[i].setMap(null);
			}
		}

		avion_en_vol_free.length = 0;
		clearRouteMarkers()
	}
	function CoordMapType(tileSize) {this.tileSize = tileSize;};



	CoordMapType.prototype.getTile = function(coord, zoom, ownerDocument)
	{
		var div = ownerDocument.createElement('div');
		div.innerHTML = coord;
		div.style.width = this.tileSize.width + 'px';
		div.style.height = this.tileSize.height + 'px';
		div.style.fontSize = '10';
		div.style.color = '#AAAAAA';
		div.style.borderStyle = 'solid';
		div.style.borderWidth = '1px';
		div.style.borderColor = '#AAAAAA';
		return div;
	};

	function FFS2_map()
	{
		var Options={
			center: {lat: 38.11953, lng: 20.50382},
			mapTypeId: google.maps.MapTypeId.SATELLITE,
			zoom: 1.5,
			autozoom: true,
			refreshTime: refreshTime_aev,
			mapTypeControl: true,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.RIGHT_TOP 
			},
			zoomControl: true,
			zoomControlOptions: {
				position: google.maps.ControlPosition.TOP_RIGHT
			},

		};
		setInterval(function () { liveRefresh(); }, Options.refreshTime);



		liveRefresh();


		map = new google.maps.Map(document.getElementById('ffs_map'),Options);

		map.overlayMapTypes.insertAt(0, new CoordMapType(new google.maps.Size(256, 256)));



		function icon (hdg,type,spec)
		{
			if (hdg >= 0 || hdg <= 360)
			{
				colone_image=(Math.round((hdg)/15))+12;
				if (colone_image>23){colone_image=colone_image-24;}
			};

			if (spec == 0) {url_icon='<?=e_PLUGIN."mark_42_multi"?>/page/map/images/yellow_large.png';}
			else {url_icon='<?=e_PLUGIN."mark_42_multi"?>/page/map/images/yellow_large.png';}

			var image = {

				url: url_icon,
					// This marker is 20 pixels wide by 32 pixels high.
					size: new google.maps.Size(48, 48),

					// The origin for this image is (0, 0).
					origin: new google.maps.Point(48*colone_image, 48*type),
					// The anchor for this image is the base of the flagpole at (0, 32).
					anchor: new google.maps.Point(24, 24),
					//scaledSize: new google.maps.Size(60, 60)
				};
				return image;
			}
			function liveRefresh()
			{
				var String = 'mod=map_aircraft';

				//alertObject(String);
				$.ajax({
					type: "GET",
					url: "<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
					data: String,
					dataType: "json",
					cache: false,
					success: function(data) 
					{
						//console.log(data);
						//alertObject(data);
						clearAircraft_Free();
						distrib (data)
					},
					error : function(resultat, statut, erreur){
						//alertObject(resultat.responseText);
					},

				});
			}
			function distrib (data)
			{
				if (data!= null)
				{
					for (var i = 0; i < data.length; i++) 
					{
						if (data[i]!= null)
						{
								/*switch(data[i].type)
								{
									case "LIBRE": 
									if (free!= 0) {populate_Map_Aircraft_Free(data[i]);} else{clearAircraft_Free();};
									break;
								}*/

								populate_Map_Aircraft_Free(data[i]);
							}
						}
					}
				}


				function populate_Map_Aircraft_Free(data_map)
				{

					var bounds = new google.maps.LatLngBounds();

					lat = data_map.Latitude; lng = data_map.Longitude;

					var pos = new google.maps.LatLng(lat, lng);

					if((data_map.pilot_id ==='0'))
						{color_file=1;}
					else{color_file=0;}

					//console.log('color'+color+'  '+data_map.pilot_id);
					avion_en_vol_free[avion_en_vol_free.length] = new google.maps.Marker({
						position: pos,
						map: map,
						icon: icon(data_map.heading,10,color_file),
						optimized: false,
						flightdetails: data_map,
						title: data_map.pilotname+'->'+data_map.aircraft+' : '+data_map.flightnum,
						//texte:texte_aircraft(data)

					});

					google.maps.event.addListener(avion_en_vol_free[avion_en_vol_free.length - 1], 'click', function() 
					{

						//removeLine();
						//$('#info').hide();
						//$('#loader_data_map').show();
						//$('#info').show(500);

						var String = 'mod=data_flight&pilot_id='+data_map.pilot_id;

						//alertObject(String);
						$.ajax({
							type: "GET",
							url: "./../../<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
							//url: "<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
							data: String,
							dataType: "html",
							cache: false,
							success: function(data) 
							{
								document.getElementById('info_data_map').innerHTML=data;
								$('#info_data').modal('show');
							},
							error: function(jqXHR, textStatus, errorThrown){error_log(jqXHR, textStatus, errorThrown)}

						});


					/*

						var focus_bounds = new google.maps.LatLngBounds();
						var String = 'mod=data_flight&pilot_id='+data_map.pilot_id;
						$.ajax({
							type: "GET",
							url: "<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
							data: String,
							dataType: "html",
							cache: false,
							success: function(data) 
							{
									//alertObject(data2);
									$('#loader_data_map').hide()
									$('#info_data_map').html(data);
								},
								error: function(jqXHR, textStatus, errorThrown){error_log(jqXHR, textStatus, errorThrown)}
							});

						var String = 'mod=data_schedule&fn='+data_map.flightnum;
						$.ajax({
							type: "GET",
							url: "<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
							data: String,
							dataType: "json",
							cache: false,
							success: function(data) 
							{
								var dep_location = new google.maps.LatLng(data['dep']['lat'], data['dep']['lng']);
								var arr_location = new google.maps.LatLng(data['arr']['lat'], data['arr']['lng']);
								var path = new Array();
								path[path.length] = dep_location;
								path[path.length] = arr_location

								depMarker = new google.maps.Marker({
									position: dep_location,
									map: map,
									icon: '<?=e_PLUGIN."mark_42_multi"?>/page/map/images/towerdeparture.png',
									title: data['dep']['name'],
									zIndex: 100
								});

								arrMarker = new google.maps.Marker({
									position: arr_location,
									map: map,
									icon: '<?=e_PLUGIN."mark_42_multi"?>/page/map/images/towerarrival.png',
									title: data['arr']['name'],
									zIndex: 100
								});
								;

								flightPath = new google.maps.Polyline({
									path: path,
									geodesic: true,
									strokeColor: '#FF0000',
									strokeOpacity: 1.0,
									strokeWeight: 2
								});

								flightPath.setMap(map);

							},
								//error: function(jqXHR, textStatus, errorThrown){error_log(jqXHR, textStatus, errorThrown)}
							});

						var String = 'mod=data_black_box&fn='+data_map.flightnum;
						$.ajax({
							type: "GET",
							url: "<?=e_PLUGIN."mark_42_multi"?>/page/map/php/data_req.php",
							data: String,
							dataType: "html",
							cache: false,
							success: function(data2) 
							{

								if (Array.isArray(eval(data2))==true)
								{
									var flightPlanCoordinates = eval(data2);
									flightPath2 = new google.maps.Polyline({
										path: flightPlanCoordinates,
										geodesic: true,
										strokeColor: '#FFFF00',
										strokeOpacity: 1.0,
										strokeWeight: 2
									});
									flightPath2.setMap(map);
								};

							},
							error: function(jqXHR, textStatus, errorThrown){error_log(jqXHR, textStatus, errorThrown)}
						});


						clearRouteMarkers();
						*/
					});

					bounds.extend(pos);
				}
			}

		</script>
		<script src="https://maps.googleapis.com/maps/api/js?key=<?=api_google_map_key?>&libraries=places&callback=FFS2_map" async defer></script>
		<script type="text/javascript" src="/e107_web/lib/jquery-once/jquery.once.min.js?1496563042"></script>

		<script type="text/javascript">

		</script>

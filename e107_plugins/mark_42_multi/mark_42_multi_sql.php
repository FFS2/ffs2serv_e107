CREATE TABLE `tks_mp` (
`id` int(11) NOT NULL AUTO_INCREMENT,
`pilot_id` varchar(11) NOT NULL DEFAULT '0',
`ip` varchar(30) NOT NULL,
`local_ip` text NOT NULL,
`pilotname` varchar(100) NOT NULL DEFAULT '',
`port` int(6) NOT NULL,
`pu_key` text,
`key_f` varchar(100) NOT NULL,
`attached` varchar(5) NOT NULL,
`server` int(2) NOT NULL,
`tx_mod` int(2) NOT NULL,
`lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tks_security` (
`id` int(2) NOT NULL AUTO_INCREMENT,
`name` varchar(256) NOT NULL,
`value` text NOT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_acarsdata` (
`map_id` int(5) NOT NULL AUTO_INCREMENT,
`pilot_id` int(5) NOT NULL DEFAULT '0',
`flightnum` varchar(40) NOT NULL DEFAULT '0',
`pilotname` varchar(100) NOT NULL DEFAULT '',
`aircraft` varchar(50) DEFAULT NULL,
`lat` float  NOT NULL DEFAULT '0',
`lng` float  NOT NULL DEFAULT '0',
`heading` smallint(6) NOT NULL DEFAULT '0',
`alt` int(6) NOT NULL DEFAULT '0',
`gs` int(11) NOT NULL DEFAULT '0',
`ias` int(5) DEFAULT '0',
`squawk` int(4) DEFAULT '1200',
`depicao` varchar(4) NOT NULL DEFAULT 'INF',
`depapt` varchar(255) NOT NULL DEFAULT 'INF',
`arricao` varchar(4) NOT NULL DEFAULT 'INF',
`arrapt` varchar(255) NOT NULL DEFAULT 'INF',
`deptime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`timeremaining` varchar(6) NOT NULL DEFAULT '',
`arrtime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`route` text NOT NULL,
`route_details` text NOT NULL,
`distremain` varchar(6) NOT NULL DEFAULT '',
`phasedetail` varchar(255) NOT NULL DEFAULT '',
`lastupdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
`client` varchar(20) NOT NULL DEFAULT '',
PRIMARY KEY (`map_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_serveur` (
`serveur_id` int(2) NOT NULL AUTO_INCREMENT,
`serveur_name` varchar(256) NOT NULL,
`administrateur` text,
`private` tinyint(1)  DEFAULT '0',
PRIMARY KEY (`serveur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_serveur_member` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `serveur_id` int(5) NOT NULL,
  `user_id` int(5) NOT NULL,
  `administrateur` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_airports` (
  `ID` int(11) NOT NULL,
  `ICAO` varchar(4) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Latitude` double DEFAULT '0',
  `Longitude` double DEFAULT '0',
  `Elevation` int(7) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_runways` (
  `ID` int(7) NOT NULL,
  `Airport_ID` int(7) NOT NULL,
  `Ident` varchar(4) CHARACTER SET utf8 NOT NULL,
  `TrueHeading` double DEFAULT '0',
  `Length` int(6) NOT NULL,
  `Width` int(6) NOT NULL,
  `Surface` varchar(10) CHARACTER SET utf8 NOT NULL,
  `Latitude` double DEFAULT '0',
  `Longitude` double DEFAULT '0',
  `Elevation` int(7) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_Navaids` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `Ident` varchar(4) DEFAULT NULL,
  `Type` tinyint(3) UNSIGNED DEFAULT '0',
  `Name` varchar(38) DEFAULT NULL,
  `Freq` int(11) DEFAULT NULL,
  `Channel` varchar(4) DEFAULT NULL,
  `Usage` varchar(1) DEFAULT NULL,
  `Latitude` double DEFAULT '0',
  `Longitude` double DEFAULT '0',
  `Elevation` int(11) DEFAULT '0',
  `SlavedVar` float DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_ILSes` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `RunwayID` int(11) DEFAULT '0',
  `Freq` int(11) DEFAULT '0',
  `GsAngle` float DEFAULT '0',
  `Latitude` double DEFAULT '0',
  `Longitude` double DEFAULT '0',
  `Category` tinyint(3) UNSIGNED DEFAULT '0',
  `Ident` varchar(5) DEFAULT NULL,
  `LocCourse` float DEFAULT '0',
  `CrossingHeight` tinyint(3) UNSIGNED DEFAULT '0',
  `HasDme` tinyint(1) DEFAULT NULL,
  `Elevation` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `tks_metar` (
  `station_id` varchar(10) NOT NULL,
  `raw_text` text NOT NULL,
  `observation_time` varchar(256) NOT NULL,
  `Latitude` double NOT NULL,
  `Longitude` double NOT NULL,
  `temp_c` float NOT NULL,
  `dewpoint_c` float NOT NULL,
  `wind_dir_degrees` int(11) NOT NULL,
  `wind_speed_kt` int(11) NOT NULL,
  `visibility_statute_mi` float NOT NULL,
  `altim_in_hg` float NOT NULL,
  `sky_condition` text NOT NULL,
  `sea_level_pressure_mb` float NOT NULL,
  `wx_string` varchar(30) DEFAULT NULL,
  `flight_category` varchar(10) NOT NULL,
  `metar_type` varchar(10) NOT NULL,
  `elevation_m` float NOT NULL,
  PRIMARY KEY (`station_id`),
  UNIQUE KEY `station_id` (`station_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







CREATE TABLE `tks_AirwayLegs` (
`ID` int(11) NOT NULL DEFAULT '0',
`AirwayID` int(11) DEFAULT '0',
`Level` varchar(1) DEFAULT NULL,
`Waypoint1ID` int(11) DEFAULT '0',
`Waypoint2ID` int(11) DEFAULT '0',
`IsStart` tinyint(1) DEFAULT NULL,
`IsEnd` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_Airways` (
`ID` int(11) NOT NULL DEFAULT '0',
`Ident` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_Com_airports` (
  `ID` int(6) NOT NULL,
  `ID_airport` int(5) NOT NULL,
  `ICAO` varchar(6) NOT NULL,
  `Type` varchar(10) NOT NULL,
  `Frequency` varchar(10) NOT NULL,
  `Name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE `tks_NavaidLookup` (
  `Ident` varchar(4) NOT NULL,
  `Type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `Country` varchar(2) NOT NULL,
  `NavKeyCode` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `ID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `tks_NavaidTypes` (
  `Type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `Desc` varchar(24) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `tks_SurfaceTypes` (
  `SurfaceType` varchar(3) NOT NULL,
  `Description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_TerminalLegs` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `TerminalID` int(11) NOT NULL DEFAULT '0',
  `Type` varchar(1) DEFAULT NULL,
  `Transition` varchar(5) DEFAULT NULL,
  `TrackCode` varchar(2) DEFAULT NULL,
  `WptID` int(11) DEFAULT '0',
  `WptLat` double DEFAULT '0',
  `WptLon` double DEFAULT '0',
  `TurnDir` varchar(1) DEFAULT NULL,
  `NavID` int(11) DEFAULT '0',
  `NavLat` double DEFAULT '0',
  `NavLon` double DEFAULT '0',
  `NavBear` float DEFAULT '0',
  `NavDist` float DEFAULT '0',
  `Course` float DEFAULT '0',
  `Distance` float DEFAULT '0',
  `Alt` varchar(12) DEFAULT NULL,
  `Vnav` float DEFAULT '0',
  `CenterID` int(11) DEFAULT '0',
  `CenterLat` double DEFAULT '0',
  `CenterLon` double DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_TerminalLegsEx` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `IsFlyOver` tinyint(1) DEFAULT NULL,
  `SpeedLimit` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_Terminals` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `AirportID` int(11) DEFAULT '0',
  `Proc` tinyint(3) UNSIGNED DEFAULT '0',
  `ICAO` varchar(4) NOT NULL,
  `FullName` varchar(28) NOT NULL,
  `Name` varchar(8) NOT NULL,
  `Rwy` varchar(3) DEFAULT NULL,
  `RwyID` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_TrmLegTypes` (
  `Code` varchar(2) NOT NULL,
  `Description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_WaypointLookup` (
  `Ident` varchar(5) NOT NULL,
  `Country` varchar(2) NOT NULL,
  `ID` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `tks_Waypoints` (
  `ID` int(11) NOT NULL DEFAULT '0',
  `Ident` varchar(5) NOT NULL,
  `Collocated` tinyint(1) DEFAULT NULL,
  `Name` varchar(25) DEFAULT NULL,
  `Latitude` double DEFAULT '0',
  `Longitude` double DEFAULT '0',
  `NavaidID` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


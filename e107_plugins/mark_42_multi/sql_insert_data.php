<?php

	/*
	 * configuration pour la génération des clé RSA
	 *
	 */
	$config = array ('digest_alg' => 'sha1', 'private_key_bits' => 4096, 'private_key_type' => OPENSSL_KEYTYPE_RSA,);

	$numberofdays = 3650;
	$privkey      = openssl_pkey_new ();
	$csr          = openssl_csr_new ($config, $privkey);
	$sscert       = openssl_csr_sign ($csr, NULL, $privkey, $numberofdays);
	openssl_x509_export ($sscert, $publickey);
	openssl_pkey_export ($privkey, $privatekey);
	openssl_csr_export ($csr, $csrStr);

	$e107_tks_security = array (array ('id' => '2', 'name' => 'CertRSA', 'value' => $publickey), array ('id' => '3', 'name' => 'PrivateRSAKey', 'value' => $privatekey), array ('id' => '4', 'name' => 'ATC_mod', 'value' => 'true'), array ('id' => '5', 'name' => 'admin_serv', 'value' => "Admin"),);


	$e107_tks_serveur = array (array ('serveur_id' => '1', 'serveur_name' => 'serveur libe 1', 'administrateur' => '999999', 'private' => '0'), array ('serveur_id' => '2', 'serveur_name' => 'Serveur libre 2', 'administrateur' => '999999', 'private' => '0'), array ('serveur_id' => '3', 'serveur_name' => 'Serveur Libre 3', 'administrateur' => '999999', 'private' => '0'), array ('serveur_id' => '4', 'serveur_name' => 'Serveur libre 4', 'administrateur' => '999999', 'private' => '0'), array ('serveur_id' => '5', 'serveur_name' => 'Serveur de test', 'administrateur' => '2', 'private' => '1'), array ('serveur_id' => '6', 'serveur_name' => 'France VFR', 'administrateur' => '1860', 'private' => '1'), array ('serveur_id' => '7', 'serveur_name' => 'Bush Airways', 'administrateur' => '1727', 'private' => '1'), array ('serveur_id' => '8', 'serveur_name' => 'Nephis Airlines', 'administrateur' => '1768', 'private' => '1'), array ('serveur_id' => '9', 'serveur_name' => 'Virtuel Air Alsace', 'administrateur' => '1757', 'private' => '1'), array ('serveur_id' => '10', 'serveur_name' => 'Avions Militaire et Civil', 'administrateur' => '1691', 'private' => '1'));


?>
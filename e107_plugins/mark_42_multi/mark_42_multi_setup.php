<?php
/*
* e107 website system
*
* Copyright (C) 2008-2013 e107 Inc (e107.org)
* Released under the terms and conditions of the
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)
*
* Custom install/uninstall/update routines for mark_42_multi plugin
**
*/


if(!class_exists("mark_42_multi_setup"))
{
	class mark_42_multi_setup
	{

	    function install_pre($var)
		{
			// print_a($var);
			// echo "custom install 'pre' function<br /><br />";
		}

		/**
		 * For inserting default database content during install after table has been created by the mark_42_multi_sql.php file.
		 */
		function install_post($var)
		{
			$sql = e107::getDb();
			$mes = e107::getMessage();

			require_once(e_PLUGIN."/mark_42_multi/sql_insert_data.php");

			function insert_mark_42_multi_db_sql_data($data_db,$sql,$mes)
			{
				foreach ($data_db as $my_tab => $my_data)
				{
					$count_insert=0;
					$count_db=count($my_data['data']);
					foreach ($my_data['data'] as $key => $value)
					{
						if($sql->insert($my_tab, $value))$count_insert++;

					}
					if($count_insert==$count_db){ $mes->add($my_data['succes']."".$count_insert."/".$count_db." Entrée(s)", E_MESSAGE_SUCCESS);}
					elseif($count_insert!=$count_db and $count_insert>0){ $mes->add($my_data['warning']."".$count_insert."/".$count_db." Entrée(s)", E_MESSAGE_WARNING);}
					else {$mes->add($my_data['error'], E_MESSAGE_ERROR);}
				}
			}

			function insert_mark_42_db_airac_csv_data($file_name,$sql,$mes)
			{
				$file = fopen(e_PLUGIN.'mark_42_multi/airac_data/'.$file_name.'.txt', 'r');

				$a=0;

				while (($line = fgetcsv($file)) !== FALSE)
				{
					$airports=0;

					if($line[0]=='A')
					{
						$to_insert =array('airport_id' => '','icao' => $line[1],'name' => $line[2],'latitude' => $line[3],'longitude' => $line[4],'elevation' => $line[5],'transition_altitude' => $line[6],'transition_level' => $line[7],'max_lenght' => $line[8]);
						$sql->insert('tks_airports', $to_insert);
						$airports++;
					}

					
				}
				fclose($file);
				$mes->add("Initialisation des aéroports Réussie: ".$airports." Entrée(s)", E_MESSAGE_SUCCESS);
			}

			


			$data_to_insert=array(
				'tks_security' =>array(
					'data'	 	=> $e107_tks_security,
					'succes'	=>"Initialisation des données de sécurité Réussie. ",
					'error'		=>"Echec de l'initialisation des sécurités."
					),
				);

			insert_mark_42_multi_db_sql_data($data_to_insert,$sql,$mes);

			$data_to_insert=array(
				'tks_serveur' =>array(
					'data'	 	=> $e107_tks_serveur,
					'succes'	=>"Initialisation des données des serveurs Réussie. ",
					'error'		=>"Echec de l'initialisation des serveurs."
					),
				);

			insert_mark_42_multi_db_sql_data($data_to_insert,$sql,$mes);

			//suprimer les // pour activer le chargement automatique des aéroports
			//insert_mark_42_db_airac_csv_data('Airports',$sql,$mes);
			

		}

		function uninstall_options()
		{

			/*$listoptions = array(0=>'option 1',1=>'option 2');

			$options = array();
			$options['mypref'] = array(
					'label'		=> 'Custom Uninstall Label',
					'preview'	=> 'Preview Area',
					'helpText'	=> 'Custom Help Text',
					'itemList'	=> $listoptions,
					'itemDefault'	=> 1
			);

			return $options;*/
		}


		function uninstall_post($var)
		{
			// print_a($var);
		}

		function upgrade_post($var)
		{
			// $sql = e107::getDb();
		}

	}

}
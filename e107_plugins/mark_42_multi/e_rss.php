<?php

if (!defined('e107_INIT')) { exit; }


// v2.x Standard

class mark_42_multi_rss // plugin-folder + '_rss'
{
	/**
	 * Admin RSS Configuration
	 */
	function config()
	{
		$config = array();

		$config[] = array(
			'name'			=> 'Feed Name',
			'url'			=> 'mark_42_multi',
			'topic_id'		=> '',
			'description'	=> 'this is the rss feed for the mark_42_multi plugin', // that's 'description' not 'text'
			'class'			=> e_UC_MEMBER,
			'limit'			=> '9'
		);

		return $config;
	}

	/**
	 * Compile RSS Data
	 * @param array $parms
	 * @param string $parms['url']
	 * @param int $parms['limit']
	 * @param int $parms['id']
	 * @return array
	 */
	function data($parms=array())
	{
		$sql = e107::getDb();

		$rss = array();
		$i=0;

		if($items = $sql->select('mark_42_multi', "*", "mark_42_multi_field = 1 LIMIT 0,".$parms['limit']))
		{

			while($row = $sql->fetch())
			{

				$rss[$i]['author']			= $row['mark_42_multi_user_id'];
				$rss[$i]['author_email']	= $row['mark_42_multi_user_email'];
				$rss[$i]['link']			= "mark_42_multi/mark_42_multi.php?";
				$rss[$i]['linkid']			= $row['mark_42_multi_id'];
				$rss[$i]['title']			= $row['mark_42_multi_title'];
				$rss[$i]['description']		= $row['mark_42_multi_message'];
				$rss[$i]['category_name']	= '';
				$rss[$i]['category_link']	= '';
				$rss[$i]['datestamp']		= $row['mark_42_multi_datestamp'];
				$rss[$i]['enc_url']			= "";
				$rss[$i]['enc_leng']		= "";
				$rss[$i]['enc_type']		= "";
				$i++;
			}

		}

		return $rss;
	}



}

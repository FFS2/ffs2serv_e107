<?php 
class tks_airports_ui extends e_admin_ui
{

	protected $pluginTitle		= 'Gestion des aéroport';
	protected $pluginName		= 'mark_42_multi';
	//	protected $eventName		= 'tks_airports-tks_airports'; // remove comment to enable event triggers in admin. 		
	protected $table			= 'tks_airports';
	protected $pid				= 'airport_id';
	protected $perPage			= 10; 
	protected $batchDelete		= true;
	protected $batchExport     = true;
	protected $batchCopy		= true;
	//	protected $sortField		= 'somefield_order';
	//	protected $orderStep		= 10;
	//	protected $tabs				= array('Tabl 1','Tab 2'); // Use 'tab'=>0  OR 'tab'=>1 in the $fields below to enable. 

	//	protected $listQry      	= "SELECT * FROM `#tableName` WHERE field != '' "; // Example Custom Query. LEFT JOINS allowed. Should be without any Order or Limit.
	
	protected $listOrder		= 'airport_id DESC';
	
	protected $fields 		= array (  
		'checkboxes' =>   array (
			'title' => '', 
			'type' => null, 
			'data' => null, 
			'width' => '5%', 
			'thclass' => 
			'center', 
			'forced' => '1', 
			'class' => 'center', 
			'toggle' => 'e-multiselect',
			),
		'airport_id' =>   array (
			'title' => 'Identifiant interne', 
			'data' => 'int', 
			'width' => '5%', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'icao' =>   array (
			'title' => 'Code ICAO', 
			'type' => 'text', 
			'data' => 'str', 
			'width' => 'auto',
			'inline' => true, 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'name' =>   array (
			'title' => 'Nom de l\'aéroport', 
			'type' => 'text', 
			'data' => 'str', 
			'width' => 'auto', 
			'inline' => true, 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'latitude' =>   array (
			'title' => 'Lattitude', 
			'type' => 'text', 
			'data' => 'str', 
			'width' => 'auto', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'longitude' =>   array (
			'title' => 'Longitude', 
			'type' => 'text', 
			'data' => 'str', 
			'width' => 'auto', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'elevation' =>   array (
			'title' => 'Altitude de l\'aéroport', 
			'type' => 'number', 
			'data' => 'int', 
			'width' => 'auto', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'transition_altitude' =>   array (
			'title' => 'Altitude de transition', 
			'type' => 'number', 
			'data' => 'int', 
			'width' => 'auto', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'max_lenght' =>   array (
			'title' => 'Longueur de piste maximale', 
			'type' => 'number', 
			'data' => 'int', 
			'width' => 'auto', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'transition_level' =>   array (
			'title' => 'Charlink', 
			'type' => 'number', 
			'data' => 'int', 
			'width' => 'auto', 
			'help' => '', 
			'readParms' => '', 
			'writeParms' => '', 
			'class' => 'left', 
			'thclass' => 'left',
			),
		'options' =>   array (
			'title' => LAN_OPTIONS, 
			'type' => null, 
			'data' => null, 
			'width' => '10%', 
			'thclass' => 'center last', 
			'class' => 'center last', 
			'forced' => '1', 
			),
		);		

	protected $fieldpref = array('name', 'country');


	//	protected $preftabs        = array('General', 'Other' );
	protected $prefs = array(
		); 

	
	public function init()
	{
			// Set drop-down values (if any). 

	}


		// ------- Customize Create --------

	public function beforeCreate($new_data,$old_data)
	{
		return $new_data;
	}
	
	public function afterCreate($new_data, $old_data, $id)
	{
			// do something
	}

	public function onCreateError($new_data, $old_data)
	{
			// do something		
	}		


		// ------- Customize Update --------

	public function beforeUpdate($new_data, $old_data, $id)
	{
		return $new_data;
	}

	public function afterUpdate($new_data, $old_data, $id)
	{
			// do something	
	}

	public function onUpdateError($new_data, $old_data, $id)
	{
			// do something		
	}		


	/*	
		// optional - a custom page.  
		public function customPage()
		{
			$text = 'Hello World!';
			$otherField  = $this->getController()->getFieldVar('other_field_name');
			return $text;
			
		}
	*/

	}
	?>
<?php
	$mes        = e107::getMessage ();
	$extensions = array ('.txt', '.TXT', '.csv', '.CSV');
	$extension  = strrchr ($_FILES['airac_file']['name'], '.');

	if (!in_array ($extension, $extensions)) {
		$erreur = 'Vous devez uploader un fichier de type txt';
	}
	else {
		echo "Avancement de l'installation des aéroports:<br>";
		echo "<div id='install_progres'></div><br>";
		echo "<div id='install_info'></div><br>";

		$file_airac_name = time () . "_" . $_FILES['airac_file']['name'];
		$rep             = "./" . e_PLUGIN . "mark_42_multi/admin_ui/temp/";
		if (move_uploaded_file ($_FILES['airac_file']['tmp_name'], $rep . $file_airac_name)) {
			echo 'Ouverture du fichier: ' . $rep . $file_airac_name . '<br>';
			$data = file_get_contents ($rep . $file_airac_name);

			echo 'Modification du format<br>';
			$data = str_replace (',', '.', $data);
			$data = str_replace ('|', ',', $data);

			echo 'Sauvegarde du format<br>';

			file_put_contents ($rep . $file_airac_name, $data);

			$file = new SplFileObject($rep . $file_airac_name);  // 996kb
			$file->setFlags (SplFileObject::READ_CSV);
			$file->seek ($file->getSize ());
			$tot_ligne = intval ($file->key ());
			$interval  = intval ($tot_ligne / 100);
			echo 'Votre fichier comporte ' . $file->key () . ' lignes<br><br>';
			echo 'Interval de: ' . $interval . '<br>';

			$valid_data = TRUE;
			$airports   = 0;
			$runway     = 0;
			$offset     = array ();
			$first      = TRUE;
			$rol        = 0;

			while ($rol < $tot_ligne) {
				if (($rol + 1 + $interval) < $tot_ligne) {
					$offset[] = array ($rol + 1, $rol + 1 + $interval);

				}
				else {
					$offset[] = array ($rol + 1, $tot_ligne - 1);
				}
				$rol = $rol + $interval;
			}
			$sql->gen ("TRUNCATE e107_tks_airports");
			sleep (2);
			$js_offset = json_encode ($offset);
			?>
            <script type="text/javascript">
                var key_json = 0;
                var js_offset = JSON.parse('<?= $js_offset ?>');
                airac_send_data_to_bd(js_offset, key_json);

                function airac_send_data_to_bd(js_offset, key_json) {

                    $.ajax({
                        type: "POST",
                        url: "/<?=e_PLUGIN . 'mark_42_multi/admin_ui/js_airport.php '?>",
                        data: 'file=<?= $file_airac_name ?>&offset_start=' + js_offset[key_json][0] + '&offset_end=' + js_offset[key_json][1] + '&mod=ap',
                        cache: false,
                        success: function (result) {
                            key_json++;
                            console.log(result);
                            document.getElementById('install_progres').innerHTML = '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="' + parseInt(key_json) + '" aria-valuemin="0" valuemax="100" style="width:' + parseInt(key_json) + '%">' + parseInt(key_json) + '%</div></div>';





                            //document.getElementById('install_info').innerHTML=  'Ecriture des lignes '+js_offset[key_json][0]+" à "+js_offset[key_json][1];
                            console.log(key_json + ": " + js_offset[key_json][0] + " à " + js_offset[key_json][1]);




                            if ((key_json) < (js_offset.length)) {
                                airac_send_data_to_bd(js_offset, key_json);
                            }
                            else {
                                document.getElementById('install_progres').innerHTML = '<div class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">100%</div></div>';
                            }
                        },
                        error: function (resultat, statut, erreur) {
                            alertObject(resultat.responseText);
                        },
                    });
                };

                function alertObject(obj) {
                    console.log(obj);
                }

                function error_log(jqXHR, textStatus, errorThrown) {
                    console.log('jqXHR:');
                    console.log(jqXHR);
                    console.log('textStatus:');
                    console.log(textStatus);
                    console.log('errorThrown:');
                    console.log(errorThrown);
                };
            </script>


			<?php
			$mes->add ("Installation des aéroports Réussie: " . $airports . " Entrée(s)", E_MESSAGE_SUCCESS);
			$mes->add ("Installation des piste Réussie: " . $runway . " Entrée(s)", E_MESSAGE_SUCCESS);


			echo $mes->render ();
			$file = NULL;

		}

	}
?>
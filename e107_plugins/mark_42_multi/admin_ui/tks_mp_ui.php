<?php 


class tks_mp_ui extends e_admin_ui
{

	protected $pluginTitle		= 'Multi';
	protected $pluginName		= 'mark_42_multi';
	//	protected $eventName		= 'mark_42_multi-tks_mp'; // remove comment to enable event triggers in admin. 		
	protected $table			= 'tks_mp';
	protected $pid				= 'id';
	protected $perPage			= 10; 
	protected $batchDelete		= true;
	protected $batchExport     = true;
	protected $batchCopy		= true;
	//	protected $sortField		= 'somefield_order';
	//	protected $orderStep		= 10;
	//	protected $tabs				= array('Tabl 1','Tab 2'); // Use 'tab'=>0  OR 'tab'=>1 in the $fields below to enable. 

	//	protected $listQry      	= "SELECT * FROM `#tableName` WHERE field != '' "; // Example Custom Query. LEFT JOINS allowed. Should be without any Order or Limit.
	
	protected $listOrder		= 'id DESC';
	
	protected $fields 		= array (  
		'checkboxes' =>   array ( 
			'title' => '',
			'type' => null,
			'data' => null,
			'width' => '5%',
			'thclass' => 'center',
			'forced' => '1',
			'class' => 'center',
			'toggle' => 'e-multiselect', 
			),
		'id' =>   array (
			'title' => 'Identifiant slot',
			'data' => 'int',
			'width' => '5%',
			'help' => 'Identifiant du slot',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'pilot_id' =>   array (
			'title' => 'identifiant du pilote',
			'type' => 'number',
			'data' => 'str',
			'width' => '5%',
			'help' => 'Identifiant du joueur',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',
			),
		'ip' =>   array (
			'title' => 'IP externe',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Ip distant du joueur',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'local_ip' =>   array (
			'title' => 'IP local crypté',
			'type' => 'textarea',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Ip local du joueur',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'pilotname' =>   array (
			'title' => 'Nom du pilote',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Nom du pilote',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'port' =>   array (
			'title' => 'Port',
			'type' => 'number',
			'data' => 'int',
			'width' => 'auto',
			'help' => 'Port du peer to peer',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'key_f' =>   array (
			'title' => 'clé de rappel',
			'type' => 'textarea',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Clé de rappel',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'server' =>   array (
			'title' => 'Server',
			'type' => 'dropdown',
			'data' => 'int',
			'width' => 'auto',
			'help' => 'Serveur utilisé',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'tx_mod' =>   array (
			'title' => 'Mod',
			'type' => 'dropdown',
			'data' => 'int',
			'width' => 'auto',
			'help' => 'Mod de transmission',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'lastupdate' =>   array (
			'title' => 'Lastupdate',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Dernière mise à jours',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'options' =>   array (
			'title' => LAN_OPTIONS,
			'type' => null,
			'data' => null,
			'width' => '10%',
			'thclass' => 'center last',
			'class' => 'center last',
			'forced' => '1', 
			),
		);		

	protected $fieldpref = array('pilot_id', 'ip', 'local_ip');


	//	protected $preftabs        = array('General', 'Other' );
	protected $prefs = array(
		); 

	
	public function init()
	{
		$sql = e107::getDb();

		/**
		* recherche des serveur de vols
		*/
		$sql->gen("SELECT * FROM `e107_tks_serveur` WHERE '1' ORDER BY `serveur_name` ASC");
		while($row = $sql->fetch())
		{
			$server_name[$row['serveur_id']]= $row['serveur_name'];
		}
		$this->fields['server']['writeParms']['optArray']=$server_name;
		$this->fields['tx_mod']['writeParms']['optArray'] = array('tx_mod_0','tx_mod_1', 'tx_mod_2'); // Example Drop-down array. 

	}

	
		// ------- Customize Create --------
	
	public function beforeCreate($new_data,$old_data)
	{
		return $new_data;
	}

	public function afterCreate($new_data, $old_data, $id)
	{
			// do something
	}

	public function onCreateError($new_data, $old_data)
	{
			// do something		
	}		
	
	
		// ------- Customize Update --------
	
	public function beforeUpdate($new_data, $old_data, $id)
	{
		return $new_data;
	}

	public function afterUpdate($new_data, $old_data, $id)
	{
			// do something	
	}
	
	public function onUpdateError($new_data, $old_data, $id)
	{
			// do something		
	}		
	

	/*	
		// optional - a custom page.  
		public function customPage()
		{
			$text = 'Hello World!';
			$otherField  = $this->getController()->getFieldVar('other_field_name');
			return $text;
			
		}
	*/

	}
	?>
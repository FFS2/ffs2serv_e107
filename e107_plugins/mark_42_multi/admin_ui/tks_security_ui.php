<?php 

class tks_security_ui extends e_admin_ui
{

	protected $pluginTitle		= 'Multi';
	protected $pluginName		= 'mark_42_multi';
	//	protected $eventName		= 'mark_42_multi-tks_mp'; // remove comment to enable event triggers in admin. 		
	protected $table			= 'tks_security';
	protected $pid				= 'id';
	protected $perPage			= 10; 
	protected $batchDelete		= true;
	protected $batchExport     = true;
	protected $batchCopy		= true;
	//	protected $sortField		= 'somefield_order';
	//	protected $orderStep		= 10;
	//	protected $tabs				= array('Tabl 1','Tab 2'); // Use 'tab'=>0  OR 'tab'=>1 in the $fields below to enable. 

	//	protected $listQry      	= "SELECT * FROM `#tableName` WHERE field != '' "; // Example Custom Query. LEFT JOINS allowed. Should be without any Order or Limit.
	
	protected $listOrder		= 'id DESC';
	
	protected $fields 		= array (  
		'checkboxes' =>   array ( 
			'title' => '',
			'type' => null,
			'data' => null,
			'width' => '5%',
			'thclass' => 'center',
			'forced' => '1',
			'class' => 'center',
			'toggle' => 'e-multiselect', 
			),
		'id' =>   array (
			'title' => 'id',
			'data' => 'int',
			'width' => '5%',
			'help' => 'Identifiant de la sécurité',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'name' =>   array (
			'title' => 'name',
			'type' => 'text',
			'data' => 'str',
			'width' => '5%',
			'help' => 'Nom du la sécurité',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',
			),
		'value' =>   array (
			'title' =>'valeur',
			'type' => 'textarea',
			'data' => 'str',
			'width' => '80%',
			'help' => 'Donnée de sécurité',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			),
		'options' =>   array (
			'title' => LAN_OPTIONS,
			'type' => null,
			'data' => null,
			'width' => '10%',
			'thclass' => 'center last',
			'class' => 'center last',
			'forced' => '1', 
			),
		
		);		

	protected $fieldpref = array('pilot_id', 'ip', 'local_ip');


	//	protected $preftabs        = array('General', 'Other' );
	protected $prefs = array(
		); 

	
	public function init()
	{
		

	}


		// ------- Customize Create --------

	public function beforeCreate($new_data,$old_data)
	{
		return $new_data;
	}

	public function afterCreate($new_data, $old_data, $id)
	{
			// do something
	}

	public function onCreateError($new_data, $old_data)
	{
			// do something		
	}		


		// ------- Customize Update --------

	public function beforeUpdate($new_data, $old_data, $id)
	{
		return $new_data;
	}

	public function afterUpdate($new_data, $old_data, $id)
	{
			// do something	
	}

	public function onUpdateError($new_data, $old_data, $id)
	{
			// do something		
	}		


	/*	
		// optional - a custom page.  
		public function customPage()
		{
			$text = 'Hello World!';
			$otherField  = $this->getController()->getFieldVar('other_field_name');
			return $text;
			
		}
	*/

	}
	?>

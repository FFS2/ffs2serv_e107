<?php 
error_reporting(E_ALL);

//tentative de chargement des classes de E107
if (! @include_once("./../../../class2.php"))
	throw new Exception ("Votre E107 est introuvable");

//initialisation de la bd E107
$sql = e107::getDb();

$file = new SplFileObject('./temp/'.$_POST['file']);  // 996kb
$file->setFlags(SplFileObject::READ_CSV);

//$file->seek($_POST['offset_start']);
//var_dump($_POST['offset_start']);
//echo '<br>';
//var_dump($_POST['offset_end']);
//echo '<br>';
//echo "step ".$_POST['offset_start']. " to ". $_POST['offset_end']." = ".($_POST['offset_end']-$_POST['offset_start']).'<br>';
$a=$b=0;
$file->seek($_POST['offset_start']);
$last_icao='';
for ($i=$_POST['offset_start']; $i < $_POST['offset_end']+1; $i++)
{ 
	
	//echo "step ".$_POST['offset_start']. " to ". $_POST['offset_end'].'\n';
	$row=$file->current();
	if($row[0]=='A')
	{
		if($_POST['mod']=='ap')
		{
			$to_insert =array(
				'airport_id' => '',
				'icao' => $row[1],
				'name' => $row[2],
				'latitude' => $row[3],
				'longitude' => $row[4],
				'elevation' => $row[5],
				'transition_altitude' => $row[6],
				'transition_level' => $row[7],
				'max_lenght' => $row[8]
			);

			$go=$sql->insert('tks_airports', $to_insert);
		}
		else
		{
			$last_icao=$row[1];
		}
		
		
		//echo $i.'->'.$file->key();
	}
	if($row[0]=='R' and $_POST['mod']=='rw')
	{
		$to_insert =array('Runway_id' => '',
			'designator' => $row[1],
			'trueheading' => $row[2],
			'length' => $row[3],
			'width' => $row[4],
			'ILSFrequency' => $row[6],
			'ILSLocaliserCourse' => $row[7],
			'latitude' => $row[8],
			'longitude' => $row[9],
			'elevation' => $row[10],
			'ILSAngle' => $row[11],
			'icao' =>$last_icao,

		);
		
		$go=$sql->insert('tks_airports_runways', $to_insert);
		//$last_icao=$row[1];
		//echo $i.'->'.$file->key();
	}

	$file->next();
}
	//echo 'boucle'.$b++."<br>";
//0//R, piste
//1//10,runway_id
//2//106, CAP MAGNETIQUE PISTE (en degré)
//3//13500,length
//4//300,width
//5//1, PISTE EQUIPEE ILS (à confirmer: 1=ILS, 0=pas d'ILS)
//6//108.500,ILSFrequency
//7//106,ILSLocaliserCourse
//8//48.430342,lat
//9//-106.551786,lgn
//10//2757, ALTITUDE DU SEUIL DE PISTE (en pied)
//11//3.00,ILSAngle
//12//56, HAUTEUR DE REFERENCE ILS (en pied)
//13//1,
//0



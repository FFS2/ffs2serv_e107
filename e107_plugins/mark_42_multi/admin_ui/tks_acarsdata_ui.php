<?php 
class tks_acarsdata_ui extends e_admin_ui
{

	protected $pluginTitle		= 'Temps réel';
	protected $pluginName		= 'tks_acarsdata';
	//	protected $eventName		= 'tks_airports-tks_acarsdata'; // remove comment to enable event triggers in admin. 		
	protected $table			= 'tks_acarsdata';
	protected $pid				= 'map_id';
	protected $perPage			= 10; 
	protected $batchDelete		= true;
	protected $batchExport     = true;
	protected $batchCopy		= true;
	//	protected $sortField		= 'somefield_order';
	//	protected $orderStep		= 10;
	//	protected $tabs				= array('Tabl 1','Tab 2'); // Use 'tab'=>0  OR 'tab'=>1 in the $fields below to enable. 

	//	protected $listQry      	= "SELECT * FROM `#tableName` WHERE field != '' "; // Example Custom Query. LEFT JOINS allowed. Should be without any Order or Limit.
	
	protected $listOrder		= 'map_id DESC';
	
	protected $fields 		= array (
		'checkboxes' =>   array (
			'title' => '',
			'type' => null, 'data' => null, 'width' => '5%',
			'thclass' => 'center',
			'forced' => '1',
			'class' => 'center',
			'toggle' => 'e-multiselect',  
			),
		
		'ID' =>   array (
			'title' => 'map_id',
			'data' => 'int',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'pilot_id' =>   array (
			'title' => 'Identifiant du piloe',
			'type' => 'text',
			'data' => 'str',
			'width' => '5%',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'flightnum' =>   array (
			'title' => 'Indicatif du vol',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'pilotname' =>   array (
			'title' => 'Nom du pilote',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'aircraft' =>   array (
			'title' => 'Appareil utilisé',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'lat' =>   array (
			'title' => 'Lattitude',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'lng' =>   array (
			'title' => 'Longitude',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'heading' =>   array (
			'title' => 'Cap',
			'type' => 'number',
			'data' => 'int',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'alt' =>   array (
			'title' => 'Altitude',
			'type' => 'number',
			'data' => 'int',
			'width' => 'auto',
			'help' => 'Ft',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'gs' =>   array (
			'title' => 'Vitesse Sol',
			'type' => 'number',
			'data' => 'int',
			'width' => 'auto',
			'help' => 'Kts',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 

			),
		'ias' =>   array (
			'title' => 'Vitesse Air',
			'type' => 'number',
			'data' => 'int',
			'width' => 'auto',
			'help' => 'IAS',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'squawk' =>   array (
			'title' => 'Transpondeur',
			'type' => 'number',
			'data' => 'int',
			'width' => 'auto',
			'help' => '1200',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'depicao' =>   array (
			'title' => 'Aéroport de départ',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'ICAO',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'depapt' =>   array (
			'title' => 'Aéroport de départ 2',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Nom de l\'aéroport',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'arricao' =>   array (
			'title' => 'Aéroport d\'arrivé',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'ICAO',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'arrapt' =>   array (
			'title' => 'Aéroport d\'arrivé 2',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Nom de l\'aéroport',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'deptime' =>   array (
			'title' => 'Heure de départ',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'timeremaining' =>   array (
			'title' => 'Temps restant',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'arrtime' =>   array (
			'title' => 'Heure d\'arrivé',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'route' =>   array (
			'title' => 'Route',
			'type' => 'textarea',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'route_details' =>   array (
			'title' => 'Détail de la route',
			'type' => 'textarea',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'distremain' =>   array (
			'title' => 'Distance restante',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'Nmi',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'phasedetail' =>   array (
			'title' => 'Phase du vol',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'lastupdate' =>   array (
			'title' => 'Mise à jour position',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'client' =>   array (
			'title' => 'Logiciel client',
			'type' => 'text',
			'data' => 'str',
			'width' => 'auto',
			'help' => '',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',  
			),
		
		'options' =>   array (
			'title' => LAN_OPTIONS, 'type' => null, 'data' => null, 'width' => '10%',
			'thclass' => 'center last',
			'class' => 'center last',
			'forced' => '1',  
			),
		
		);		

protected $fieldpref = array();


	//	protected $preftabs        = array('General', 'Other' );
protected $prefs = array(
	); 


public function init()
{
			// Set drop-down values (if any). 
			$this->fields['pilot_id']['writeParms']['optArray'] = array('pilot_id_0','pilot_id_1', 'pilot_id_2'); // Example Drop-down array. 
			$this->fields['pilotname']['writeParms']['optArray'] = array('pilotname_0','pilotname_1', 'pilotname_2'); // Example Drop-down array. 

		}

		
		// ------- Customize Create --------
		
		public function beforeCreate($new_data,$old_data)
		{
			return $new_data;
		}

		public function afterCreate($new_data, $old_data, $id)
		{
			// do something
		}

		public function onCreateError($new_data, $old_data)
		{
			// do something		
		}		
		
		
		// ------- Customize Update --------
		
		public function beforeUpdate($new_data, $old_data, $id)
		{
			return $new_data;
		}

		public function afterUpdate($new_data, $old_data, $id)
		{
			// do something	
		}
		
		public function onUpdateError($new_data, $old_data, $id)
		{
			// do something		
		}		
		

	/*	
		// optional - a custom page.  
		public function customPage()
		{
			$text = 'Hello World!';
			$otherField  = $this->getController()->getFieldVar('other_field_name');
			return $text;
			
		}
	*/

	}
	?>
<?php 

class tks_serveur_ui extends e_admin_ui
{

	protected $pluginTitle		= 'Multi';
	protected $pluginName		= 'mark_42_multi';
	//	protected $eventName		= 'mark_42_multi-tks_mp'; // remove comment to enable event triggers in admin. 		
	protected $table			= 'tks_serveur';
	protected $pid				= 'serveur_id';
	protected $perPage			= 10; 
	protected $batchDelete		= true;
	protected $batchExport     = true;
	protected $batchCopy		= true;
	//	protected $sortField		= 'somefield_order';
	//	protected $orderStep		= 10;
	//	protected $tabs				= array('Tabl 1','Tab 2'); // Use 'tab'=>0  OR 'tab'=>1 in the $fields below to enable. 

	//	protected $listQry      	= "SELECT * FROM `#tableName` WHERE field != '' "; // Example Custom Query. LEFT JOINS allowed. Should be without any Order or Limit.
	
	protected $listOrder		= 'serveur_id DESC';
	
	protected $fields 		= array (  
		'checkboxes' =>   array ( 
			'title' => '',
			'type' => null,
			'data' => null,
			'width' => '5%',
			'thclass' => 'center',
			'forced' => '1',
			'class' => 'center',
			'toggle' => 'e-multiselect', 
			),
		'serveur_id' =>   array (
			'title' => 'serveur_id',
			'data' => 'int',
			'width' => '5%',
			'help' => 'Identifiant du serveur',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			'forced' => '1',
			),
		'serveur_name' =>   array (
			'title' => 'name',
			'type' => 'text',
			'data' => 'str',
			'width' => '5%',
			'help' => 'Nom du serveur',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',
			),
		'administrateur' =>   array (
			'title' =>'administrateur',
			'type' => 'dropdown',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'administrateurs',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left',
			'forced' => '0', 
			),
		'private' =>   array (
			'title' =>'privé',
			'type' => 'boolean',
			'data' => 'str',
			'width' => 'auto',
			'help' => 'serveur privé',
			'readParms' => '',
			'writeParms' => '',
			'class' => 'left',
			'thclass' => 'left', 
			'forced' => '0',
			),
		'options' =>   array (
			'title' => LAN_OPTIONS,
			'type' => null,
			'data' => null,
			'width' => '10%',
			'thclass' => 'center last',
			'class' => 'center last',
			'forced' => '1', 
			),
		
		);		

	//protected $fieldpref = array('pilot_id', 'ip', 'local_ip');


	//	protected $preftabs        = array('General', 'Other' );
	protected $prefs = array(
		); 

	
	public function init()
	{
		$sql = e107::getDb();
		/**
		* recherche des pilote dans la base e107_user du module forum 
		* "username"=> pseudo du pilote (Bernard_207, mirage_2000_de_paris,...)
		*/
		$pilote_username[999999]='none';
		$sql->gen("SELECT `user_name`,`user_id` FROM `e107_user` ORDER BY `e107_user`.`user_name` ASC");
		while($row = $sql->fetch())
		{
			$pilote_username[$row['user_id']]= $row['user_name'];
		}
		$this->fields['administrateur']['writeParms']['optArray']=$pilote_username;
	}


		// ------- Customize Create --------

	public function beforeCreate($new_data,$old_data)
	{
		return $new_data;
	}

	public function afterCreate($new_data, $old_data, $id)
	{

	}

	public function onCreateError($new_data, $old_data)
	{
			// do something		
	}		


		// ------- Customize Update --------

	public function beforeUpdate($new_data, $old_data, $id)
	{
		return $new_data;
	}

	public function afterUpdate($new_data, $old_data, $id)
	{
			// do something	
	}

	public function onUpdateError($new_data, $old_data, $id)
	{
			// do something		
	}		


	/*	
		// optional - a custom page.  
		public function customPage()
		{
			$text = 'Hello World!';
			$otherField  = $this->getController()->getFieldVar('other_field_name');
			return $text;
			
		}
	*/

	}
	?>
